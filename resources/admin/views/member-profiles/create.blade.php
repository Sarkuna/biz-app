@extends('layouts/fullLayoutMaster')

@section('title', 'Member New')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/forms/selects/select2.min.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/forms/validation/form-validation.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">      
@endsection

@section('content')
<div class="content-body">
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic">Member Info</h4>
                        
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">
                            

                            <form method="post" action="{{route('members.store')}}" enctype="multipart/form-data" class="form form-horizontal" novalidate>
                            {{csrf_field()}}    
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-user"></i> Login Info</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Name <span class="required">*</span></label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{old('full_name')}}" id="full_name" class="form-control" placeholder="Full Name" name="full_name" required>
                                            @error('full_name')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">E-mail <span class="required">*</span></label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{old('email')}}" id="email" class="form-control" placeholder="Email Address" name="email" required>
                                            @error('email')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput4">Mobile Number</label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{old('mobile')}}" id="mobile" class="form-control" placeholder="Mobile" name="mobile" required123>
                                            @error('mobile')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <h4 class="form-section"><i class="ft-clipboard"></i> Member Info</h4>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5">Membership No</label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{old('membership_no')}}" id="membership_no" class="form-control" placeholder="GB858525" name="membership_no" required123>
                                            @error('membership_no')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5">Company Name</label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{old('name')}}" id="company_name" class="form-control" placeholder="Member Company Name" name="name" required123>
                                            @error('name')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput6">Category</label>
                                        <div class="col-md-9 mx-auto">
                                            <select id="projectinput6" name="member_category[]" class="select2 form-control" multiple="multiple" autocomplete="off">
                                                <option value="">--Select Member Category--</option>
                                                @foreach($membercategories as $key=>$category)
                                                    <option value='{{$category->id}}'>{{$category->category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput9">Notes</label>
                                        <div class="col-md-9 mx-auto">
                                            <textarea id="summary" rows="5" class="form-control" name="summary" placeholder="Notes"></textarea>
                                            @error('summary')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <h4 class="form-section"><i class="ft-mail"></i> Contact Info</h4>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5">Address</label>
                                        <div class="col-md-4 mx-auto">
                                            <input type="text" value="{{old('address1')}}" id="projectinput5" class="form-control" placeholder="Address 1" name="address1" required123>
                                            @error('address1')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-5 mx-auto">
                                            <input type="text" value="{{old('address2')}}" id="projectinput5" class="form-control" placeholder="Address 2" name="address2" required123>
                                            @error('address2')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5"></label>
                                        <div class="col-md-4 mx-auto">
                                            <input type="text" value="{{old('city')}}" id="projectinput5" class="form-control" placeholder="City" name="city" required123>
                                            @error('city')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-5 mx-auto">
                                            <input type="text" value="{{old('state')}}" id="projectinput5" class="form-control" placeholder="State" name="state" required123>
                                            @error('state')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5"></label>
                                        <div class="col-md-4 mx-auto">
                                            <input type="text" value="{{old('post_code')}}" id="projectinput5" class="form-control" placeholder="Postcode" name="post_code" required123>
                                            @error('post_code')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-5 mx-auto">
                                            <select name="country" id="country" class="form-control" placeholder="Country" required123>
                                                <option value="">--Select--</option>
                                                <option value='malaysia'>Malaysia</option>
                                            </select>
                                            @error('country')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5">Contact Number</label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{old('office_phone')}}" id="office_phone" class="form-control" placeholder="Office Phone No" name="office_phone">
                                            @error('office_phone')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <a href="{{route('members.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>

@endsection