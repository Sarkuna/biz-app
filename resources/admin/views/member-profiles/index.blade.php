@extends('layouts/fullLayoutMaster')

@section('title', 'Member List')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">   
@endsection

@section('content')
<div class="content-body">
    <section id="pagination">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-head">
                        <div class="card-header">
                            <h4 class="card-title">Members</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            <div class="heading-elements">
                                <a href="{{ route('members.create') }}" class="btn btn-primary btn-sm" id="new-customer"><i class="ft-plus white"></i> Add a new member</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                @if(count($models)>0)
                                <table class="table table-white-space table-bordered alt-pagination">
                                    <thead>
                                        <tr>
                                            <th width="5%">#</th>
                                            <th width="60%">Member name</th>
                                            <th>Mobile No</th>
                                            <th>Date Created</th>
                                            <th>Last Login Date</th>
                                            <th width="15%">Status</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($models as $model)
                                        <tr id="client_id_{{ $model->id }}">
                                            <td>{{ $loop->index+1 }}</td>
                                            <td>{{ $model->name }}</td>
                                            <td>{{$model->member_info->mobile}}</td>
                                            <td class="text-center">
                                                @if(!empty($model->member_info->created_at))
                                                {{ $model->member_info->created_at->format('d-m-Y') }}
                                                @else
                                                N/A
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if(!empty($model->member_info->date_last_login))
                                                {{ $model->member_info->date_last_login->format('d-m-Y') }}
                                                @else
                                                N/A
                                                @endif
                                            </td>
                                            
                                            
                                            <td>
                                                @if($model->status=='active')
                                                <span class="badge badge-success">{{ucwords($model->status)}}</span>
                                                @elseif($model->status=='suspend')
                                                <span class="badge badge-danger">{{ucwords($model->status)}}</span>
                                                @else
                                                <span class="badge badge-warning">{{ucwords($model->status)}}</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{route('members.edit',$model->id)}}" id="edit-client"><i class="ft-edit"></i> </a>
                                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                                <a id="delete-category" data-id="{{ $model->id }}"><i class="ft-trash-2"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <h6 class="text-center">No email templates found!!! Please create</h6>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/tables/datatables/datatable-basic.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
@endsection