@extends('layouts/fullLayoutMaster')

@section('title', 'Events')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">   
@endsection


@section('content')
<div class="content-body">
    <section id="pagination">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-head">
                        <div class="card-header">
                            <h4 class="card-title">Venues</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            <div class="heading-elements">
                            <a href="{{ route('events.create') }}" class="btn btn-primary btn-sm" id="new-customer"><i class="ft-plus white"></i> Add a new event</a>    
                            </div>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                @if(count($models)>0)
                                <table class="table table-white-space table-bordered alt-pagination">
                                    <thead>
                                        <tr>
                                            <th width="5%">#</th>                                            
                                            <th>Event</th>
                                            <th width="10%">Events count</th>
                                            <th width="15%">Status</th>
                                            <th width="10%"><i class="la la-cog"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($models as $model)
                                        <tr id="category_id_{{ $model->id }}">
                                            <td>{{ $loop->index+1 }}</td>
                                            <td>{{ $model->name }}</td>
                                            <td>{{ $model->subject }}0</td>
                                            <td>
                                                @if($model->venue_status=='Visible')
                                                    <span class="badge badge-success">{{ucwords($model->venue_status)}}</span>
                                                @else
                                                    <span class="badge badge-danger">{{ucwords($model->venue_status)}}</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{route('events.edit',$model->id)}}" id="edit-venues" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="ft-edit"></i> </a>
                                                
                                                @if($model->venue_status == 'Hide')
                                                    <form action="{{ route('venues.status.change', $model->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to visible this record?');" style="display: inline-block;">
                                                        <input type="hidden" name="status" value="Visible">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="submit" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Visible">
                                                            <i class="ft-eye"></i>
                                                        </button>
                                                    </form>
                                                @else
                                                    <form action="{{ route('venues.status.change', $model->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to hide this record?');" style="display: inline-block;">
                                                        <input type="hidden" name="status" value="Hide">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="submit" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Hide">
                                                            <i class="ft-eye-off"></i>
                                                        </button>
                                                    </form>
                                                @endif
                                                <form action="{{ route('venues.destroy', $model->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this record?');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button type="submit" class="btn btn-sm btn-danger">
                                                        <i class="ft-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <h6 class="text-center">No email templates found!!! Please create</h6>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection
@section('page-script')

  <!-- Page js files -->

  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/tables/datatables/datatable-basic.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
@endsection