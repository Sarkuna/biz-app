@extends('layouts/fullLayoutMaster')

@section('title', ' Email Template Edit')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/forms/selects/select2.min.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/forms/validation/form-validation.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">      
@endsection

@section('content')
<div class="content-body">
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Edit</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">

                            <form method="post" action="{{route('email.update',$emailtemplate->id)}}" class="form" novalidate>
                            @csrf 
                            @method('PATCH')
                                <div class="form-body">
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="Subject">Subject <span class="text-danger">*</span></label>
                                                <input type="text" value="{{$emailtemplate->subject}}" id="Subject" class="form-control" placeholder="Email Subject" name="subject" required>
                                                @error('subject')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="form-group">
                                        <label for="projectinput8">Body <span class="text-danger">*</span></label>
                                        <textarea id="classic-editor" rows="10" cols="80" class="form-control" name="template" placeholder="Email Body">{{$emailtemplate->template}}</textarea>
                                        @error('template')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    

                                </div>

                                <div class="form-actions">
                                    <a href="{{route('email.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection
@section('page-script')

  <!-- Page js files -->

  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
  <script type="text/javascript">
    CKEDITOR.replace('template', {
        allowedContent : true,
        filebrowserBrowseUrl : '/admin/elfinder/ckeditor',
        filebrowserImageBrowseUrl : '/admin/elfinder/ckeditor',
        //uiColor : '#9AB8F3',
        height : 600
    });
</script>
@endsection