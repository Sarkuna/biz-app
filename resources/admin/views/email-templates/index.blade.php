@extends('layouts/fullLayoutMaster')

@section('title', 'Email Template')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">   
@endsection


@section('content')
<div class="content-body">
    <section id="pagination">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-head">
                        <div class="card-header">
                            <h4 class="card-title">Email Template</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            <div class="heading-elements">
                                
                            </div>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                @if(count($models)>0)
                                <table class="table table-white-space table-bordered alt-pagination">
                                    <thead>
                                        <tr>
                                            <th width="5%">#</th>                                            
                                            <th width="30%">Name</th>
                                            <th width="40%">Subject</th>
                                            <th width="15%">Status</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($models as $model)
                                        <tr id="category_id_{{ $model->id }}">
                                            <td>{{ $loop->index+1 }}</td>
                                            <td>{{ $model->name }}</td>
                                            <td>{{ $model->subject }}</td>
                                            <td>
                                                @if($model->status=='yes')
                                                    <span class="badge badge-success">{{ucwords($model->status)}}</span>
                                                @else
                                                    <span class="badge badge-danger">{{ucwords($model->status)}}</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{route('email.edit',$model->id)}}" id="edit-email-templates" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="ft-edit"></i> </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <h6 class="text-center">No email templates found!!! Please create</h6>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection
@section('page-script')

  <!-- Page js files -->

  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/tables/datatables/datatable-basic.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
@endsection