@extends('layouts/fullLayoutMaster')

@section('title', 'Email Templates')

@section('vendor-style')
  <!-- vendor css files -->
  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/forms/validation/form-validation.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">      
@endsection

@section('content')
<div class="content-body">
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Create</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">

                            <form method="post" action="{{route('global-email-templates.store')}}" class="form" novalidate>
                            {{csrf_field()}}
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="projectinput1">Code</label>
                                                <input type="text" value="{{ Helper::getToken(6) }}" id="projectinput1" class="form-control" placeholder="Template Code" name="code" readonly="readonly">
                                                @error('code')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="projectinput1">Name <span class="text-danger">*</span></label>
                                                <input type="text" value="{{old('name')}}" id="projectinput1" class="form-control" placeholder="Template Name" name="name" required>
                                                @error('name')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="Subject">Subject <span class="text-danger">*</span></label>
                                                <input type="text" value="{{old('subject')}}" id="Subject" class="form-control" placeholder="Email Subject" name="subject" required>
                                                @error('subject')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="form-group">
                                        <label for="projectinput8">Body <span class="text-danger">*</span></label>
                                        <textarea id="projectinput8" rows="5" class="form-control" name="template" placeholder="Email Body"></textarea>
                                        @error('template')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="status">Status <span class="text-danger">*</span></label>
                                        <select name="status" class="form-control">
                                            <option selected>Pick Status</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                        @error('status')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <a href="{{route('global-email-templates.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                                    
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Shortcode</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <?php
                            $path = storage_path() . "/json/shortcode.json"; // ie: /var/www/laravel/app/storage/json/filename.json

                            $shortcodes = json_decode(file_get_contents($path), true);
                            //dd($shortcodes['shortcodes']);
                            foreach($shortcodes['shortcodes'] as $shortcode) {
                                echo $shortcode;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('panels/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
  <script type="text/javascript">
    CKEDITOR.replace('template', {
        filebrowserBrowseUrl : '/elfinder/ckeditor',
        filebrowserImageBrowseUrl : '/elfinder/ckeditor',
        //uiColor : '#9AB8F3',
        height : 600
    });
</script>
@endsection