<!DOCTYPE html>

<html class="loading semi-dark-layout" lang="en" data-layout="semi-dark-layout" data-textdirection="ltr">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title') - Biz Portal</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/logo/favicon.ico')}}">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

  {{-- Include core + vendor Styles --}}
  @include('global/styles')


</head>
<?php
$user = Auth::guard(config('auth.defaults.guard'))->user();
if($user->user_type == 'members') {
    $custroute = 'member';
}else {
    $custroute = 'admin';
}

//$user->client->image
?>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    @include('global/header')
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    @include('global/verticalMenu')

    <!-- END: Main Menu-->
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            @include('global.breadcrumb')
            @yield('content')
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    @include('global/footer')
    <!-- END: Footer-->


  <!-- End: Content-->

  {{-- include default scripts --}}
  @include('global/scripts')

</body>

</html>
