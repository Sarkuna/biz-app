@extends('layouts.authLayout')
@section('title', 'Admin Recover Password')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">   
@endsection

@section('content')

<section class="row flexbox-container">
    <div class="col-12 d-flex align-items-center justify-content-center">
        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                @include('auth.partials/cardHeader')
                <div class="card-content">
                    
                    @if(session()->has('error'))
                   <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif

                    <div class="card-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.forget.password.post') }}" novalidate>
                            {{ csrf_field() }}

                            
                            <fieldset class="form-group position-relative has-icon-left {{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="controls">
                                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="user-name" placeholder="Your Email Address" required data-validation-required-message="{{ $errors->has('email') ? ' has-error' : 'This field is required' }}" >
                                    <div class="form-control-position">
                                        <i class="la la-envelope"></i>
                                    </div>
                                    @error('email')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>                                 
                            </fieldset>

                            <button type="submit" class="btn btn-outline-info btn-lg btn-block"><i class="ft-unlock"></i> Recover Password</button>
                        </form>
                    </div>
                </div>
                <div class="card-footer border-0">
                    <p class="float-sm-left text-center"><a href="/admin" class="card-link">Login</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
@endsection
