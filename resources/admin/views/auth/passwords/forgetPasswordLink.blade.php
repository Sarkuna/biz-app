@extends('layouts.authLayout')
@section('title', 'Reset Password')
@section('content')


<section class="row flexbox-container">
    <div class="col-12 d-flex align-items-center justify-content-center">
        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                @include('auth.partials/cardHeader')
                <div class="card-content">
                    
                    @if(session()->has('error'))
                   <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif

                    <div class="card-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.reset.password.post') }}" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="token" value="{{ $token }}">
                            
                            <fieldset class="form-group position-relative has-icon-left {{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="controls">
                                    <input type="password" name="password" value="{{ old('password') }}" class="form-control" id="new_password" placeholder="New Password" required>
                                    <div class="form-control-position">
                                        <i class="la la-key"></i>
                                    </div>
                                    
                                    @error('password')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                    <div id='password-strength-status'></div>
                                    <span class="text-info">Your password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.</span>
                                </div>                                
                            </fieldset>

                            <fieldset class="form-group position-relative has-icon-left {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <div class="controls">
                                    <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" class="form-control" id="user-name" placeholder="Confirm Password" required>
                                    <div class="form-control-position">
                                        <i class="la la-key"></i>
                                    </div>
                                    @error('password_confirmation')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>                                 
                            </fieldset>

                            <button type="submit" class="btn btn-outline-info btn-lg btn-block"><i class="ft-unlock"></i> Reset Password</button>
                        </form>
                    </div>
                </div>
                <div class="card-footer border-0">
                    <p class="float-sm-left text-center"><a href="/admin" class="card-link">Login</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('page-script')
<script>
    $("#new_password").keyup(function() {
        var number = /([0-9])/;
        var lowercase = /([a-z])/;
        var uppercase = /([A-Z])/;
        var special = /([!@#$%^&*])/;
        if ($('#new_password').val().length < 8) {
            $('#password-strength-status').removeClass();
            $('#password-strength-status').addClass('alert alert-danger');
            $('#password-strength-status').html("Weak (should be atleast 8 characters.)");
        } else {
            if ($('#new_password').val().match(number) && $('#new_password').val().match(lowercase) && $('#new_password').val().match(uppercase) && $('#new_password').val().match(special)) {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-success');
                $('#password-strength-status').html("Strong");
            } else {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-warning');
                $('#password-strength-status').html("Medium (should include a Uppercase, numbers, special character and Lowercase.)</>");
            }
        }
    });
    </script>
@endsection

