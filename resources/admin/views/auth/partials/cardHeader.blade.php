<div class="card-header border-0">
    <div class="card-title text-center">
        @if (!empty(Helper::client_info()['image']))
          <!--<img src="/storage/bb_logo.png" alt="branding logo" class="rounded mr-75" alt="profile image" height="64" width="64">-->
          <img src="{{ asset('/storage/images/'.Helper::client_info()['image'])}}" class="rounded" width="200">
        @else
          <img src="/storage/bb_logo.png" alt="branding logo">
        @endif
        
    </div>
    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Admin</span></h6>
</div>
