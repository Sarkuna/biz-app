@extends('layouts.authLayout')
@section('title', 'Login')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">   
@endsection

@section('content')

<section class="row flexbox-container">
    <div class="col-12 d-flex align-items-center justify-content-center">
        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                @include('auth.partials/cardHeader')
                <div class="card-content">
                    
                    @if(session()->has('error'))
                   <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif

                    <div class="card-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.login.post') }}" novalidate>
                            {{ csrf_field() }}

                            
                            <fieldset class="form-group position-relative has-icon-left {{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="controls">
                                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="user-name" placeholder="Your Username" required data-validation-required-message="{{ $errors->has('email') ? ' has-error' : 'This field is required' }}" >
                                    <div class="form-control-position">
                                        <i class="la la-user"></i>
                                    </div>
                                </div>                                 
                            </fieldset>
                       
                            <fieldset class="form-group position-relative has-icon-left">
                                <div class="controls">
                                    <input type="password" name="password" class="form-control" id="user-password" placeholder="Enter Password" required data-validation-required-message="This field is required">
                                    <div class="form-control-position">
                                        <i class="la la-key"></i>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-sm-6 col-12 text-center text-sm-left pr-0">
                                    <fieldset>
                                        <input type="checkbox" id="remember-me" class="chk-remember">
                                        <label for="remember-me"> Remember Me</label>
                                    </fieldset>
                                </div>
                                <div class="col-sm-6 col-12 float-sm-left text-center text-sm-right"><a href="{{ route('admin.forget.password.get') }}" class="card-link">Forgot Password?</a></div>
                            </div>
                            <button type="submit" class="btn btn-outline-info btn-block"><i class="ft-unlock"></i> Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
@endsection