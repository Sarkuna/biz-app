@extends('layouts/fullLayoutMaster')

@section('title', 'Venue')

@section('vendor-style')
  <!-- vendor css files -->
  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/forms/validation/form-validation.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">      
<style>
.col-form-label.required:after, label.required:after {
    content: "*";
    color: #e95b35;
}
</style>
@endsection

@section('content')
<div class="content-body">
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Add a new venue</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">

                            <form method="post" action="{{route('venues.store')}}" class="form" novalidate>
                            {{csrf_field()}}
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="venue_name" class="required">Name</label>
                                    <input type="text" value="{{old('venue_name')}}" id="projectinput1" class="form-control" placeholder="Venues Name" name="venue_name" required>
                                    @error('venue_name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="venue_street" class="required">Street address</label>
                                            <input type="text" id="venue_street" name="address1" required="required" class="form-control">
                                            @error('address1')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="venue_street2">Street address 2</label>
                                            <input type="text" id="venue_street2" name="address2" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="venue_city" class="required">City</label>
                                            <input type="text" id="venue_city" name="city" required="required" class="form-control">
                                            @error('city')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="venue_postalcode" class="required">Zip / Postal code</label>
                                            <input type="text" id="venue_postalcode" name="post_code" required="required" class="form-control">
                                            @error('post_code')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="venue_state" class="required">State</label>
                                            <input type="text" id="venue_state" name="state" required="required" class="form-control">
                                            @error('state')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                        </div>
                                        <div class="form-group">
                                            <label for="status" class="required">Status</label>
                                            <select name="venue_status" class="form-control" required>
                                                <option value="" selected>Pick Status</option>
                                                <option value="Visible">Visible</option>
                                                <option value="Hide">Hide</option>
                                            </select>
                                            @error('venue_status')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">

                                    </div>
                                </div>
                            </div>

                                <div class="form-actions">
                                    <a href="{{route('venues.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                                    
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
   
@endsection
@section('page-script')

  <!-- Page js files -->

  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>

@endsection