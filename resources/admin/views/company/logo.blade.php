@extends('layouts/fullLayoutMaster')

@section('title', 'Your Company')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/ui/prism.min.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/file-uploaders/dropzone.min.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">

@endsection

@section('page-style')
{{-- Page Css files --}}

<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/file-uploaders/dropzone.css')}}"> 
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/pages/dropzone.css')}}">
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">
<style>
    .dropzone .dz-preview .dz-image img {width: 100%;}
</style>
@endsection

@section('content')
<div class="content-body">
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-md-6">
                <div class="card">

                    <div class="card-content collpase show">
                        <div class="card-body">
                            <p class="card-text">This uploads a single file.  Now only 1 file can be selected and it will be replaced with another one instead of adding it to the preview. Max file size (1MB) Formats </p>
                            <form method="post" action="{{route('admin.company.logo.store')}}" enctype="multipart/form-data" class="dropzone dropzone-area" id="bb-accept-files">
                                @csrf
                                <input type="hidden" name="clientid" value="{{$client->id}}">
                            </form>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>



@endsection

@section('vendor-script')
  <!-- vendor files -->
    
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  <!--<script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/dropzone.js') }}"></script>-->
  <script type="text/javascript">
      var total_photos_counter = 0;
      var flagsUrl = '{{ URL::asset('storage/images/') }}';
      //Dropzone.autoDiscover = false;
    Dropzone.options.bbAcceptFiles = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 1, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        dictRemoveFile: " Trash",
        dictFileTooBig: 'Image is larger than 1MB',
        init: function () {
            this.on("removedfile", function (file) {
                $.post({
                    url: 'images-delete',
                    data: {id: file.name, _token: $('[name="_token"]').val(), clientid: $('[name="clientid"]').val()},
                    dataType: 'json',
                    success: function (data) {
                        total_photos_counter--;
                        $("#counter").text("# " + total_photos_counter);
                    }
                });
            });
            thisDropzone = this;
            $.get('images-view', function(data) {
                //console.log(data);
                var dataCount = Object.keys(data).length;
                if(dataCount == 3) {
                    var mockFile = { name: data.name, size: data.size };
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, flagsUrl+"/"+data.path);
                }
            });
        },
        success: function (file, done) {
            toastr['success']('Logo Update', 'Success!', {
                closeButton: true,
                tapToDismiss: false,
              });
            window.location.href = "/admin/company";
            total_photos_counter++;
            $("#counter").text("# " + total_photos_counter);
        }
    }
  </script>

  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
  

@endsection