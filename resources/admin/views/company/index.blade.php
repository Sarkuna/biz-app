@extends('layouts/fullLayoutMaster')

@section('title', 'Company Info')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/weather-icons/climacons.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/fonts/meteocons/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/charts/morris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/charts/chartist.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/charts/chartist-plugin-tooltip.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}

<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/pages/timeline.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/pages/dashboard-ecommerce.css') }}">
   
@endsection

@section('content')
<div class="content-body">
<section class="users-view">
                    <!-- users view media object start -->
                    <div class="row">
                        <div class="col-12 col-sm-7">
                            <div class="media mb-2">
                                <!--<a class="mr-1" href="#">
                                    <img src="{{ Storage::url('your_logo.png')}}" alt="Logo" class="users-avatar-shadow" width="120">
                                </a>-->
                                <div class="media-body pt-25">
                                    <h4 class="media-heading">{{$client->name}}</h4>
                                    <span>ID:</span>
                                    <span class="users-view-id">{{$client->subscription_code}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5 px-0 d-flex justify-content-end align-items-center px-1 mb-2">
                            <a href="{{route('admin.company.edit',$client->id)}}" class="btn btn-sm btn-primary">Edit</a> 
                            <a href="{{route('admin.company.logo')}}" class="btn btn-sm btn-info ml-1">Logo</a>
                        </div>
                    </div>
                    <!-- users view media object ends -->

                    <!-- users view card details start -->
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row bg-primary bg-lighten-5 rounded mb-2 mx-25 text-center text-lg-left">
                                    <div class="col-12 col-sm-4 p-2">
                                        <h6 class="text-primary mb-0">Admin Users: <span class="font-large-1 align-middle">1</span></h6>
                                    </div>
                                    <div class="col-12 col-sm-4 p-2">
                                        <h6 class="text-primary mb-0">Member Categories: <span class="font-large-1 align-middle">3</span></h6>
                                    </div>
                                    <div class="col-12 col-sm-4 p-2">
                                        <h6 class="text-primary mb-0">Members: <span class="font-large-1 align-middle">1</span></h6>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Registered:</td>
                                                <td class="users-view-username">{{date('d-m-Y', strtotime($client->created_at))}}</td>
                                            </tr>
                                            <tr>
                                                <td>Status:</td>
                                                <td class="users-view-name">
                                                    @if($client->status=='active')
                                                        <span class="badge badge-success">{{ucwords($client->status)}}</span>
                                                    @elseif($client->status=='inactive')
                                                        <span class="badge badge-danger">{{ucwords($client->status)}}</span>
                                                    @else
                                                        <span class="badge badge-warning">{{ucwords($client->status)}}</span>
                                                    @endif
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <h5 class="mb-1"><i class="ft-info"></i> Contact Info</h5>
                                    <table class="table table-borderless mb-0">
                                        <tbody>
                                            <tr>
                                                <td>Address:</td>
                                                <td>{{$client->address}}</td>
                                            </tr>
                                            <tr>
                                                <td>City:</td>
                                                <td>{{$client->city}}</td>
                                            </tr>
                                            <tr>
                                                <td>State:</td>
                                                <td>{{$client->state}}</td>
                                            </tr>
                                            <tr>
                                                <td>Post Code:</td>
                                                <td>{{$client->post_code}}</td>
                                            </tr>
                                            <tr>
                                                <td>Country:</td>
                                                <td>{{$client->country}}</td>
                                            </tr>
                                            <tr>
                                                <td>Contact:</td>
                                                <td>{{$client->office_phone}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- users view card details ends -->

                </section>
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/charts/chartist.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/charts/raphael-min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/charts/morris.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/timeline/horizontal-timeline.js') }}"></script>
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
  <!--<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/polyfill.min.js') }}"></script>-->
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/extensions/ext-component-toastr.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
@endsection