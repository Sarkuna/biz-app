{{-- Vendor Scripts --}}
<script src="{{ asset('themes/modern-admin/app-assets/vendors/js/vendors.min.js') }}"></script>
<script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@yield('vendor-script')


{{-- Theme Scripts --}}
<script src="{{ asset('themes/modern-admin/app-assets/js/core/app-menu.js') }}"></script>
<script src="{{ asset('themes/modern-admin/app-assets/js/core/app.js') }}"></script>
<script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
{{-- page script --}}
@yield('page-script')
@include('global/messages')

{{-- page script --}}

