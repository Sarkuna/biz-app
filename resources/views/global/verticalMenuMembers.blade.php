
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item"><a href="{{ route('member.dashboard') }}"><i class="la la-home"></i><span class="menu-title" data-i18n="Home">Dashboard</span></a></li>
            <li class="{{ (request()->is(['member/company/profile'])) ? 'active' : '' }}"><a href="{{ route('member.company.profile') }}"><i class="las la-id-badge"></i><span class="menu-title" data-i18n="Home">Company Profile</span></a></li>
            
            <!--<li class=" nav-item"><a href="#"><i class="la la-book"></i><span class="menu-title" data-i18n="Tutorial">Tutorial</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Merchant Setup">Merchant Setup</span></a>
                    </li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Receipt Processing">Receipt Processing</span></a>
                    </li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Latest Promotion">Latest Promotion</span></a>
                    </li>
                </ul>
            </li>-->
            <li class=" nav-item {{ (request()->is(['member/customer'])) ? 'active' : '' }}"><a href="{{ route('customer.index') }}"><i class="la la-users"></i><span class="menu-title" data-i18n="Customer">Customer</span></a></li>
            <li class=" nav-item"><a href="#"><i class="las la-receipt"></i><span class="menu-title" data-i18n="Receipt List">Receipt List</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->is(['member/receipt/pending', 'member/*/edit'])) ? 'active' : '' }}"><a class="menu-item" href="{{ route('receipt.index') }}"><i></i><span data-i18n="Pending">Pending</span> <span class="badge badge badge-warning badge-pill float-right mr-2">{{ $count['receipt_pending'] }}</span></a></li>
                    <li class="{{ (request()->is(['member/receipt/approve'])) ? 'active' : '' }}"><a class="menu-item" href="{{ route('receipt.approve') }}"><i></i><span data-i18n="Approve">Approve</span> <span class="badge badge badge-info badge-pill float-right mr-2">{{ $count['receipt_approve'] }}</span></a></li>
                    <li class="{{ (request()->is(['member/receipt/decline'])) ? 'active' : '' }}"><a class="menu-item" href="{{ route('receipt.decline') }}"><i></i><span data-i18n="Decline">Decline</span> <span class="badge badge badge-danger badge-pill float-right mr-2">{{ $count['receipt_decline'] }}</span></a></li>
                </ul>
            </li>
            <!--<li class=" nav-item"><a href="#"><i class="la la-bullhorn"></i><span class="menu-title" data-i18n="Latest Promotion">Latest Promotion</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Detail">Detail</span></a></li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Summary">Summary</span></a></li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Promotion History">Promotion History</span></a></li>
                </ul>
            </li>-->

            <li class=" nav-item"><a href="#"><i class="la la-calendar"></i><span class="menu-title" data-i18n="Events">Events</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Latest Event">Latest Event</span></a></li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Events History">Events History</span></a></li>
                </ul>
            </li>
            
            <!--<li class=" nav-item"><a href="#"><i class="la la-ticket"></i><span class="menu-title" data-i18n="E-Voucher">E-Voucher</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Generate">Generate</span></a></li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Pending">Pending</span></a></li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="History">History</span></a></li>
                </ul>
            </li>-->
            
            <li class=" nav-item"><a href="#"><i class="las la-coins"></i><span class="menu-title" data-i18n="Points">Points</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->is(['member/points/purchase-points','member/points/*/purchase-confirmation'])) ? 'active' : '' }}"><a class="menu-item" href="{{ route('member.purchase.points') }}"><i></i><span data-i18n="Purchase Points">Purchase Points</span></a></li>
                    <li class="{{ (request()->is(['member/points/purchase-history'])) ? 'active' : '' }}"><a class="menu-item" href="{{ route('member.purchase.history') }}"><i></i><span data-i18n="Points Purchase History">Purchase History</span></a></li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Points Issues">Points Issues</span></a></li>
                </ul>
            </li>
            
            <!--<li class=" nav-item"><a href="#"><i class="las la-file-invoice"></i><span class="menu-title" data-i18n="Report">Report</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Points Summary">Points Summary</span></a></li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Points Detailed Summary">Points Detailed Summary</span></a></li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Customer Detail">Customer Detail</span></a></li>
                </ul>
            </li>-->
            

            <!--<li class=" nav-item"><a href="#"><i class="las la-hands-helping"></i><span class="menu-title" data-i18n="Referral">Referral</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="New">New</span></a></li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="Pending Approval">Pending Approval</span></a></li>
                    <li><a class="menu-item" href="#"><i></i><span data-i18n="History">History</span></a></li>
                </ul>
            </li>
            
            <li class=" nav-item"><a href="#"><i class="las la-exchange-alt"></i><span class="menu-title" data-i18n="Activity Log">Activity Log</span></a></li>
            <li class=" nav-item"><a href="#"><i class="las la-qrcode"></i><span class="menu-title" data-i18n="QR Code Generator">QR Code Generator</span></a></li>
            <li class=" nav-item"><a href="#"><i class="las la-gifts"></i><span class="menu-title" data-i18n="Campaign">Campaign</span></a></li>
            -->
        </ul>
    </div>
</div>