<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">     
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/vendors.min.css') }}" />
{{-- Vendor Styles --}}

<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/vendors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/forms/icheck/icheck.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/forms/icheck/custom.css') }}">
@yield('vendor-style')

{{-- Theme Styles --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/bootstrap-extended.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/colors.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/components.css') }}">

{{-- Page Styles --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/core/menu/menu-types/vertical-menu-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/core/colors/palette-gradient.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/plugins/forms/validation/form-validation.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/pages/login-register.css') }}">

{{-- Page Styles --}}
@yield('page-style')


{{-- user custom styles --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/assets/css/style.css') }}">
