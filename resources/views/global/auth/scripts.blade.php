{{-- Vendor Scripts --}}
<script src="{{ asset('themes/modern-admin/app-assets/vendors/js/vendors.min.js') }}"></script>
<script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js') }}"></script>
<script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
<script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
@yield('vendor-script')


{{-- Theme Scripts --}}
<script src="{{ asset('themes/modern-admin/app-assets/js/core/app-menu.js') }}"></script>
<script src="{{ asset('themes/modern-admin/app-assets/js/core/app.js') }}"></script>

{{-- page script --}}
<script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
@yield('page-script')
{{-- page script --}}

