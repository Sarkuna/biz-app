<footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-left d-block d-md-inline-block">
                Copyright &copy; 2021 <a class="text-bold-800 grey darken-2" href="http://businessboosters.com.my" target="_blank">Business Boosters</a>
            </span>
            <span class="float-md-right d-none d-lg-block">Powerd by Business Boosters<span id="scroll-top"></span></span></p>
    </footer>
