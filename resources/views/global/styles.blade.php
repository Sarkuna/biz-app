<!-- BEGIN: Vendor CSS-->

<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/vendors.min.css') }}">
<link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
 
{{-- Vendor Styles --}}
@yield('vendor-style')
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/bootstrap-extended.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/colors.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/components.css') }}">
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/core/colors/palette-gradient.css') }}">
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">

{{-- Page Styles --}}
@yield('page-style')
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}"> 
<link rel="stylesheet" href="{{ asset('themes/modern-admin/assets/css/style.css') }}" />
<!-- END: Custom CSS-->












