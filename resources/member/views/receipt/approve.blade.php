@extends('layouts/fullLayoutMaster')

@section('title', 'Receipts')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">   
@endsection

@section('content')
<div class="content-body">
    <section id="pagination">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-head">
                        <div class="card-header">
                            <h4 class="card-title">Approved List</h4>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                @if(count($receipts)>0)
                                <table class="table table-white-space table-bordered alt-pagination">
                                    <thead>
                                        <tr>
                                            <th width="5%">#</th>
                                            <th>Order #</th>
                                            <th>Full Name</th>
                                            <th>Mobile No</th>
                                            <th>Receipt Date</th>
                                            <th>Receipts No</th>
                                            <th>Receipt RM Value</th>
                                            <th>Created Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($receipts as $model)
                                        <tr>
                                            <td>{{ $loop->index+1 }}</td>
                                            <td>{{ $model->receipt_order }}</td>
                                            <td>{{ $model->user_info->full_name }}</td>
                                            <td>{{ $model->user_info->mobile }}</td>
                                            <td>{{ $model->date_of_receipt->format('d-m-Y')}}</td>
                                            <td>{{ $model->receipt_no }}</td>
                                            <td class="text-right">{{ $model->receipt_total_amount }}</td>
                                            <td>{{ $model->created_at->format('d-m-Y')}}</td>
                                            <td>
                                                <a href="#"><i class="ft-edit-1"></i></a>
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <h6 class="text-center">No records found!!!</h6>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/tables/datatables/datatable-basic.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
@endsection