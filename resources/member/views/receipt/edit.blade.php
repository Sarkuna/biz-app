@extends('layouts/fullLayoutMaster')

@section('title', 'Receipts')

@section('vendor-style')

@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="//use.fontawesome.com/releases/v5.3.1/css/all.css">  
<link rel="stylesheet" type="text/css" href="//devadmin.businessboosters.com.my/themes/adminLTE/custom/css/photoviewer.css">  
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/pages/invoice.css') }}">
<style>
    #reason_div{display: none;}
</style>
@endsection

@section('content')
<section class="card">
    <div id="invoice-template" class="card-body p-4">
        <!-- Invoice Company Details -->
        <div id="invoice-company-details" class="row">
            <div class="col-sm-6 col-12 text-center text-sm-left">
                <h2>INVOICE</h2>
                <p class="pb-sm-3"># {{ $model->receipt_order }}</p>
                <ul class="px-0 list-unstyled">
                    <li>Balance Points</li>
                    <li class="lead text-bold-800">@convertpoint($point['points_balance'])</li>
                </ul>
            </div>
            <div class="col-sm-6 col-12 text-center text-sm-right">
                <h2>
                    @if($model->status=='approved')
                    <span class="badge badge-success">{{ucwords($model->status)}}</span>
                    @elseif($model->status=='declined')
                    <span class="badge badge-danger">{{ucwords($model->status)}}</span>
                    @else
                    <span class="badge badge-warning">{{ucwords($model->status)}}</span>
                    @endif
                </h2>
            </div>
        </div>
        <!-- Invoice Company Details -->

        <!-- Invoice Customer Details -->
        <div id="invoice-customer-details" class="row pt-1">
            <div class="col-sm-6 col-12 text-center text-sm-left">
                <ul class="px-0 list-unstyled">
                    <li class="text-bold-800">{{ $model->user_info->full_name }}</li>
                    <li>{{ $model->user_info->mobile }}</li>
                    <li>{{ $model->user_info->email }}</li>
                </ul>
            </div>
            <div class="col-sm-6 col-12 text-center text-sm-right">
                
                <p><span class="text-muted">Member Category :</span>
                    @if(!empty($model->member_category))
                        {{$model->client_category->category->name }}
                    @else
                        N/A
                    @endif    
                </p>
                <p><span class="text-muted">Receipts No :</span> {{ $model->receipt_no }}</p>
                <p><span class="text-muted">Date of Receipt :</span> 
                    @if(!empty($model->date_of_receipt))
                        {{ $model->date_of_receipt->format('d-m-Y') }}
                    @else
                        N/A
                    @endif
                </p>
                <p><span class="text-muted">RM Value :</span> <span class="lead text-bold-800">@convert($model->receipt_total_amount)</span></p>
                <p><span class="text-muted">Point Value :</span> <span class="lead text-bold-800">{{ $model->receipt_total_amount * $model->member_info->point_receipt }} pts</span></p>
            </div>
        </div>
        <!-- Invoice Customer Details -->

        <!-- Invoice Items Details -->
        <div id="invoice-items-details" class="pt-2">

            <div class="row">
                <div class="col-sm-7 col-12 text-center text-sm-left">
                    <h6 class="mb-2">Proof of Receipts:</h6>
                    @if (count($portfolios) > 0)
                    <div class="row tz-gallery">
                        @foreach ($portfolios as $key => $portfolio)

                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="thumbnail">
                                <a data-gallery="manual" href="{{ asset('/storage/receipts/'.$model->client_id.'/'.$model->ref.'/'.$key)}}">
                                    <img class="img-fluid rounded" src="{{ asset('/storage/receipts/'.$model->client_id.'/'.$model->ref.'/'.$key)}}">
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
                
                <div class="col-sm-5 col-12">
                    <form class="form" method="post" action="{{route('receipt.store', ['id' => $model->id])}}">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="accountSelect">Status</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="">Select</option>
                                        <option value="pending">Pending</option>
                                        <option value="approved">Approve</option>
                                        <option value="declined">Decline</option>
                                    </select>
                                    @error('status')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-12" id="reason_div">
                                <div class="form-group">
                                    <label for="accountSelect">Reason</label>
                                    <select class="form-control" id="reason" name="reason"> 
                                        <option value="">Select</option>
                                        @foreach($reasons as $key=>$reason)
                                            <option value='{{$reason->id}}'>{{$reason->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('reason')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>


                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Submit</button>
                                <button type="reset" class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>



    </div>
</section>
@endsection

@section('vendor-script')

@endsection

@section('page-script')
    <script src="//devadmin.businessboosters.com.my/themes/adminLTE/custom/js/photoviewer_2.0.js"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/pages/invoice-template.js') }}"></script>
    <script type="text/javascript">
$(document).ready(function() {
    $('#status').on('change', function() {
      if ( this.value == 'declined'){
        $("#reason_div").show();
      }
      else
      {
        $("#reason_div").hide();
      }
    });
    // initialize manually with a list of links
    $('[data-gallery=manual]').click(function (e) {

      e.preventDefault();

      var items = [],
        options = {
          index: $(this).index()
        };

      $('[data-gallery=manual]').each(function () {
        items.push({
          src: $(this).attr('href'),
          title: $(this).attr('data-title')
        });
      });

      new PhotoViewer(items, options);

    }); 

} );
</script>
@endsection