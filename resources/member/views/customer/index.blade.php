@extends('layouts/fullLayoutMaster')

@section('title', 'Customer')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">   
@endsection

@section('content')
<div class="content-body">
    <section id="pagination">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-head">
                        <div class="card-header">
                            <h4 class="card-title">Customer List</h4>
                            

                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                @if(count($models)>0)
                                <table class="table table-white-space table-bordered alt-pagination">
                                    <thead>
                                        <tr>
                                            <th width="5%">#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile No</th>
                                            <th>Date Joined</th>
                                            <th>Last Login Date</th>
                                            <th width="15%">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($models as $model)
                                        <tr>
                                            <td>{{ $loop->index+1 }}</td>
                                            <td>{{ $model->full_name }}</td>
                                            <td>{{ $model->email }}</td>
                                            <td>{{ $model->mobile }}</td>
                                            <td>{{ $model->created_at->format('d-m-Y')}}</td>
                                            <td>
                                                @if(!empty($model->date_last_login))
                                                    {{ $model->date_last_login->format('d-m-Y')}}
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                            <td></td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <h6 class="text-center">No records found!!!</h6>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/tables/datatables/datatable-basic.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
@endsection