@extends('layouts/fullLayoutMaster')

@section('title', 'Edit Portfolio')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/forms/selects/select2.min.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/forms/validation/form-validation.css') }}">  

@endsection

@section('page-style')
{{-- Page Css files --}}


<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .main-section{
        margin:0 auto;
        padding: 20px;
        margin-top: 100px;
        background-color: #fff;
        box-shadow: 0px 0px 20px #c1c1c1;
    }
    .fileinput-remove,
    .fileinput-upload{
        display: none;
    }
</style>   
@endsection

@section('content')
<div class="content-body">
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-content collpase show">
                        <div class="card-body">

                            <form method="post" action="{{route('member.myprofile.post.new.image')}}" enctype="multipart/form-data" class="form form-horizontal" novalidate1>
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{ $memberprofiles->id }}">
                                <div class="form-body">

                                    <h4 class="form-section"><i class="ft-image"></i> Portfolio Images <small>Upload Images on products & services</small></h4>
                                    <div class="form-group">
                                        <div class="file-loading">
                                            <input id="file-2" type="file" name="files[]" multiple class="file" data-overwrite-initial="false">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <a href="{{route('member.company.profile')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
  <script type="text/javascript">
    $("#file-2").fileinput({
          theme: 'fa',
          allowedFileExtensions: ['jpg', 'png', 'gif'],
          maxFileCount: '{{ $filecount }}',
          overwriteInitial: true,
          validateInitialCount: true,
          maxFileSize: 2000,
          slugCallback: function (filename) {
              return filename.replace('(', '_').replace(']', '_');
          }
      });
  </script>

@endsection