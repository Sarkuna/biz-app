@extends('layouts/fullLayoutMaster')

@section('title', 'Edit Company Info')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/forms/selects/select2.min.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/forms/validation/form-validation.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">      
@endsection

@section('content')
<div class="content-body">
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-content collpase show">
                        <div class="card-body">

                            <form method="post" action="{{route('member.myprofile.bank.store')}}" enctype="multipart/form-data" class="form form-horizontal" novalidate1>
                            {{csrf_field()}} 
                                <div class="form-body">

                                    <h4 class="form-section"><i class="ft-clipboard"></i> Bank Account Info</h4>

                                    
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="company_name">Bank Name</label>
                                        <div class="col-md-9 mx-auto">
                                            <select id="bank_name" name="bank_name" class="select2 form-control">
                                                <option value="">--Select Bank--</option>
                                                @foreach($banks as $key=>$bank)
                                                    <option value='{{$bank->id}}' {{$memberprofiles->bank_info->bank_id == $bank->id  ? 'selected' : ''}}>{{$bank->bank_name}}</option>
                                                @endforeach
                                            </select>
                                            @error('bank_name')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="description">Account Number</label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{$memberprofiles->bank_info->account_number}}" id="account_number" class="form-control" name="account_number" required>
                                            @error('account_number')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5">Account Holder Name</label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{$memberprofiles->bank_info->account_name}}" id="account_name" class="form-control" name="account_name" required>
                                            @error('account_name')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    
                                </div>

                                <div class="form-actions">
                                    <a href="{{route('member.company.profile')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script type="text/javascript">
    CKEDITOR.replace('description', {
        //allowedContent : true,
        toolbar: [
		//{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
		[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.																					// Line break - next group will be placed in new line.
		{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
	],
        //uiColor : '#9AB8F3',
        height : 300
    });
</script>

@endsection