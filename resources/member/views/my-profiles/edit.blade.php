@extends('layouts/fullLayoutMaster')

@section('title', 'Edit Company Info')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/forms/selects/select2.min.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/forms/validation/form-validation.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">      
@endsection

@section('content')
<div class="content-body">
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-content collpase show">
                        <div class="card-body">

                            <form method="post" action="{{route('member.myprofile.post')}}" enctype="multipart/form-data" class="form form-horizontal" novalidate1>
                            {{csrf_field()}} 
                                <div class="form-body">

                                    <h4 class="form-section"><i class="ft-clipboard"></i> Company Info</h4>

                                    
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="company_name">Company Name</label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{$memberprofiles->name}}" id="company_name" class="form-control" placeholder="Company Name" name="company_name" required123>
                                            @error('company_name')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="description">Description</label>
                                        <div class="col-md-9 mx-auto">
                                            <textarea id="description" rows="5" class="form-control" name="description" required>{{$memberprofiles->description}}</textarea>
                                            @error('description')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <h4 class="form-section"><i class="las la-coins"></i> Points</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5">Receipt</label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{$memberprofiles->point_receipt}}" id="point_receipt" class="form-control" name="point_receipt" required>
                                            @error('point_receipt')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5">Referral</label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{$memberprofiles->point_referral}}" id="point_referral" class="form-control" name="point_referral" required>
                                            @error('point_referral')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <h4 class="form-section"><i class="ft-mail"></i> Contact Info</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5">Address </label>
                                        <div class="col-md-4 mx-auto">
                                            <input type="text" value="{{$memberprofiles->address1}}" id="projectinput5" class="form-control" placeholder="Address" name="address1" required>
                                            @error('address1')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-5 mx-auto">
                                            <input type="text" value="{{$memberprofiles->address2}}" id="projectinput5" class="form-control" placeholder="Address 2" name="address2" required123>
                                            @error('address2')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5"></label>
                                        <div class="col-md-4 mx-auto">
                                            <input type="text" value="{{$memberprofiles->city}}" id="projectinput5" class="form-control" placeholder="City" name="city" required>
                                            @error('city')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-5 mx-auto">
                                            <input type="text" value="{{$memberprofiles->state}}" id="projectinput5" class="form-control" placeholder="State" name="state" required>
                                            @error('state')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5"></label>
                                        <div class="col-md-4 mx-auto">
                                            <input type="text" value="{{$memberprofiles->post_code}}" id="projectinput5" class="form-control" placeholder="Postcode" name="post_code" required>
                                            @error('post_code')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="col-md-5 mx-auto">
                                            <select name="country" id="country" class="form-control" placeholder="Country" required>
                                                <option value="">--Select--</option>
                                                <option value='malaysia' {{(($memberprofiles->country=='malaysia')? 'selected':'')}}>Malaysia</option>
                                            </select>
                                            @error('country')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput5">Contact Number</label>
                                        <div class="col-md-9 mx-auto">
                                            <input type="text" value="{{$memberprofiles->contact_num}}" id="office_phone" class="form-control" placeholder="Office Phone No" name="contact_num">
                                            @error('contact_num')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <a href="{{route('member.company.profile')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/select/form-select2.js') }}"></script>
    <script type="text/javascript">
    CKEDITOR.replace('description', {
        //allowedContent : true,
        toolbar: [
		//{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
		[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.																					// Line break - next group will be placed in new line.
		{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
	],
        //uiColor : '#9AB8F3',
        height : 300
    });
</script>

@endsection