@extends('layouts/fullLayoutMaster')

@section('title', 'Purchase Points')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/ui/prism.min.css') }}">
  
@endsection

@section('page-style')
{{-- Page Css files --}}

   
@endsection

@section('content')
<div class="content-body">
    <section id="social-cards-full-button">
        <div class="row">
            <div class="col-12 mt-3 mb-1">
                <h4 class="text-uppercase">Points Value: 1 point = RM 0.01</h4>
            </div>
        </div>
        <div class="row mt-2">
            @if(count($models)>0)
                @foreach ($models as $model)
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="card box-shadow">
                        <div class="card-body text-center rounded p-0 pt-2">
                            <div class="card-header pb-0">
                                <h2 class="my-0 font-weight-bold">RM {{number_format($model->points * $model->per_price)}}</h2>
                            </div>
                            <div class="card-body">
                                <h1 class="pricing-card-title">RM {{$model->per_price}} <small class="text-muted">/ pts</small></h1>
                                <ul class="list-unstyled mt-2 mb-2">
                                    <li>{{number_format($model->points)}} pts</li>
                                    <li>{{$model->packages}}</li>
                                </ul>
                            </div>
                            <a href="{{ route('member.purchase.confirmation',$model->id) }}" class="btn btn-primary btn-block text-uppercase p-1 square">Buy Now</a>
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
            
            
            
            
        </div>
    </section>
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
@endsection

@section('page-script')

@endsection