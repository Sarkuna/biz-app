@extends('layouts/fullLayoutMaster')

@section('title', 'My Invoices')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
  
@endsection

@section('page-style')
{{-- Page Css files --}}
 
@endsection

@section('content')
<div class="content-body">
    <section id="pagination">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-head">
                        <div class="card-header">
                            <h4 class="card-title">My Invoices <small>Your invoice history with us</small></h4>
                            

                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                @if(count($models)>0)
                                <table class="table table-white-space table-bordered alt-pagination">
                                    <thead>
                                        <tr>
                                            <th width="5%">#</th>
                                            <th>Invoice #</th>
                                            <th>Invoice Date</th>
                                            <th>Due Date</th>
                                            <th>Total RM</th>
                                            <th width="15%">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($models as $model)
                                        <tr>
                                            <td>{{ $loop->index+1 }}</td>
                                            <td>{{ $model->invoice_no }}</td>
                                            <td>{{ $model->invoice_date->format('d-m-Y')}}</td>
                                            <td>{{ $model->due_date->format('d-m-Y')}}</td>
                                            <td>{{ number_format($model->total_amount,2) }}</td>

                                            <td>
                                                @if($model->status=='paid')
                                                <span class="badge badge-success">{{ucwords($model->status)}}</span>
                                                @elseif($model->status=='dispute')
                                                <span class="badge badge-warning">{{ucwords($model->status)}}</span>
                                                @elseif($model->status=='cancel')
                                                <span class="badge badge-info">{{ucwords($model->status)}}</span>
                                                @else
                                                <span class="badge badge-danger">{{ucwords($model->status)}}</span>
                                                @endif
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <h6 class="text-center">No payment history found!!!</h6>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
        <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/tables/datatables/datatable-basic.js') }}"></script>
@endsection