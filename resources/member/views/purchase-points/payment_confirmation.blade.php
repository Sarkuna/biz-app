@extends('layouts/fullLayoutMaster')

@section('title', 'Payment Confirmation')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/ui/prism.min.css') }}">
  
@endsection

@section('page-style')
{{-- Page Css files --}}

   
@endsection

@section('content')
<div class="content-body">
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Confirmation</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">

                            <form class="form" method="post" action="{{route('member.purchase.post')}}">
                                {{csrf_field()}}
                                <input name="pricings_id" type="hidden" value="{{ $models->id}}">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="companyName">Select Payment Method</label>
                                            <fieldset class="radio">
                                                <label>
                                                    <input type="radio" name="payment_method" value="bank_transfer">
                                                     Bank Deposit or Online Transfer                                                     
                                                </label>                                                
                                            </fieldset>
                                            @error('payment_method')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                            <p><strong>Bank in Details:</strong></p>

                                            <p>Company Name: Rewards Solution Sdn Bhd<br>
                                            Bank: Public Bank<br>
                                            Bank Account Details: 32 078 786 14</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="terms">Terms</label>
                                            <fieldset class="checkboxsas">
                                                <label>
                                                    <input type="checkbox" value="yes" name="terms1">
                                                     I/We accept & acknowledge that the Rewards Solution Sdn Bhd reserves the right to review & reject (refund), suggest revisions or accept the orders without submitting a reason.
                                                </label>
                                            </fieldset>
                                            @error('terms1')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                            <fieldset class="checkboxsas">
                                                <label>
                                                    <input type="checkbox" value="yes" name="terms2">
                                                    I/We have read and agree to these <a>Terms and Conditions.</a>
                                                </label>
                                            </fieldset>
                                            @error('terms2')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                                <div class="form-actions">
                                    <a href="{{ route('member.purchase.points') }}" class="btn btn-warning mr-1">
                                        <i class="ft-x"></i> Cancel
                                    </a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Proceed to Checkout
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </section>
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
@endsection

@section('page-script')

@endsection