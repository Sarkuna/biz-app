@extends('layouts/memberSetup')

@section('title', 'Setup Account')



@section('content')
<!-- Form wzard with step validation section start -->
<section id="validation">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Welcome to the Member Setup.</h4>
                    Please fill up all the details needed and click Next.
                    If you are not sure how to proceed, please click the sample for guidance

                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <form method="post" action="{{route('member.company.setup')}}" class="steps-validation wizard-notification" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <!-- Step 1 -->
                            <h6>Step 1</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="firstName3">
                                                Company Description :
                                                <span class="danger">*</span>
                                            </label>
                                            <textarea id="company_description" rows="16" class="form-control required" name="company_description" value="{{old('company_description')}}"></textarea>
                                            @error('company_description')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="firstName3">
                                                Logo :
                                            </label>
                                            <div class="file-loading">
                                                <input id="file-1" type="file" name="file" class="file" data-overwrite-initial="false" data-min-file-count="1">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>

                            <!-- Step 2 -->
                            <h6>Step 2</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="proposalTitle3">
                                                Points for Receipt :
                                                <span class="danger">*</span>
                                            </label>
                                            <p>This page is to decide how much points you are giving the Customer for every RM spent with your company.</p>
                                            <p>i.e. RM 1 = 1 point or RM 1 = 2 points.</p>
                                            <p>1 points value to Customer is = RM0.01</p>
                                            <input type="text" class="form-control required" id="proposalTitle3" name="points_receipt" value="{{old('points_receipt')}}">
                                            @error('points_receipt')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="proposalTitle3">
                                                Points for Referral :
                                                <span class="danger">*</span>
                                            </label>
                                            <p>You need to decide how much you want to give Customers for a Qualified Lead. By definition, Qualified Lead means the Customer will get referral points when the person they referred signs up as a Customer in the portal after clicking their referral link and buys from you for the 1st time. </p>
                                            <p>i.e. 1 Referral = 500 points.</p>
                                            <p>1 point value to Customer is = RM0.01, so 500 points = RM5</p>
                                            <input type="number" class="form-control required" id="proposalTitle3" name="points_referral" value="{{old('points_referral')}}">
                                            @error('points_receipt')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <!-- Step 3 -->
                            <h6>Step 3</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="firstName3">
                                                Upload Images on products & services :
                                                <p>Please upload your products or services images here (optional)</p>
                                            </label>
                                            <div class="file-loading">
                                                <input id="file-2" type="file" name="files[]" multiple class="file" data-overwrite-initial="false" data-min-file-count="2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <!-- Step 4 -->
                            <h6>Step 4</h6>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>This info is optional.</p>
                                        <p>If you decide to publish your own e-voucher, then this info is mandatory.</p>
                                        <p>You can skip this now and fill up at the e-voucher page if needed.</p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="meetingName3">
                                                Bank Name :
                                                
                                            </label>
                                            <select id="projectinput6" name="bank_name" class="select2 form-control">
                                                <option value="">--Select Bank--</option>
                                                @foreach($banks as $key=>$bank)
                                                    <option value='{{$bank->id}}'>{{$bank->bank_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="meetingLocation3">
                                                Account Number :
                                            </label>
                                            <input type="text" class="form-control" id="meetingLocation3" name="account_number">
                                        </div>

                                        <div class="form-group">
                                            <label for="participants3">
                                                Account Holder Name :
                                            </label>
                                            <input type="text" class="form-control" id="meetingLocation3" name="account_name">
                                        </div>
                                    </div>

                                    
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Form wzard with step validation section end -->
@endsection