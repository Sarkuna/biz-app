@extends('layouts/fullLayoutMaster')

@section('title', 'My Profile')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/js/gallery/photo-swipe/photoswipe.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css') }}"> 
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/pages/gallery.css') }}">
<style>
    h5 { margin-bottom: 0px; font-size: 1rem; margin-top: 10px;}
    .text-bold {font-weight: bold;}
    .pswp__caption__center .btnSubmit {display: none;}
</style>
@endsection

@section('content')
<div class="content-body">
    <!-- account setting page start -->
    <section id="page-account-settings">
        <div class="row">
            <!-- left menu section -->
            <div class="col-md-3 mb-2 mb-md-0">
                <ul class="nav nav-pills flex-column mt-md-0 mt-1">
                    <li class="nav-item">
                        <a class="nav-link d-flex active" id="account-pill-general" data-toggle="pill" href="#account-vertical-general" aria-expanded="true">
                            <i class="ft-globe mr-50"></i>
                            Company Info
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex" id="account-pill-password" data-toggle="pill" href="#account-vertical-password" aria-expanded="false">
                            <i class="ft-image mr-50"></i>
                            Portfolio
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex" id="account-pill-info" data-toggle="pill" href="#account-vertical-info" aria-expanded="false">
                            <i class="ft-briefcase mr-50"></i>
                            Bank Account Info
                        </a>
                    </li>
                    
                </ul>
            </div>
            <!-- right content section -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="true">
                                    <div class="media">
                                        <img src="{{ asset('/storage/member_logo/'.$profiles->client_id.'/'.$profiles->photo)}}" class="rounded mr-75" alt="profile image" height="64" width="64">
                                        <div class="media-body mt-75">
                                            <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start text-bold">
                                                <p>{{$profiles->name}}<br>
                                                {{$profiles->address1}}
                                                {{$profiles->address2}}
                                                {{$profiles->city}}
                                                {{$profiles->post_code}} {{$profiles->state}}
                                                
                                                {{ucwords($profiles->country)}}
                                                </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">

                                        <div class="col-12 mb-3">
                                            <h5>Points for Receipt</h5>
                                            <span class="text-bold">{{$profiles->point_receipt}} pts</span>
                                            <h5>Points for Referral</h5>
                                            <span class="text-bold">{{$profiles->point_referral}} pts</span>
                                            <h5>Membership No</h5>
                                            <span class="text-bold">{{$profiles->membership_no}}</span>
                                            <h5>Contact No</h5>
                                            <span class="text-bold">{{$profiles->contact_num}}</span>
                                            <h6 style="margin-top: 10px;">About</h6>
                                            <div class="text-bold">{!! $profiles->description !!}</div>
                                        </div>
                                        
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <a href="{{ route('member.myprofile.edit')}}" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Edit</a>
                                        </div>
                                    </div>

                                </div>
                                
                                <div class="tab-pane fade " id="account-vertical-password" role="tabpanel" aria-labelledby="account-pill-password" aria-expanded="false">
                                    <div class="card-body" itemscope itemtype="http://schema.org/ImageGallery">

                                            @if (count($portfolios) > 0)
                                            <div class="row my-gallery">
                                                @foreach ($portfolios as $key => $portfolio)
                                                    <figure class="col-lg-3 col-md-6 col-12">
                                                        <a href="{{ asset('/storage/portfolio/'.$profiles->client_id.'/'.$profiles->ref.'/'.$key)}}" itemprop="contentUrl" data-size="480x360">
                                                            <img class="img-thumbnail img-fluid" src="{{ asset('/storage/portfolio/'.$profiles->client_id.'/'.$profiles->ref.'/'.$key)}}" itemprop="thumbnail" alt="Image description" />
                                                        </a>
                                                        <form id="form-gallery" method="POST" action="{{ route('member.myprofile.remove.image') }}">
                                                            <input type="hidden" name="ref" value="{{ $profiles->ref }}">
                                                            <input type="hidden" name="new_name" value="{{ $key }}">
                                                            <input type="hidden" name="org_name" value="{{ $portfolio }}">
                                                            {!! csrf_field() !!}
                                                            <a href="javascript:void(0)" class="btn-block btn-sm btn-danger text-center btnSubmit">Remove</a>

                                                        </form>
                                                    </figure>
                                                @endforeach
                                            </div>
                                            @endif
                                            
                                            @if (count($portfolios) < 5)
                                            <div class="row d-flex flex-sm-row flex-column justify-content-end">
                                                <a href="{{ route('member.myprofile.gallery')}}" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Add Images</a>
                                            </div>
                                            @endif
                                    </div>
                                    
                                    <!-- Root element of PhotoSwipe. Must have class pswp. -->
                                    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

                                        <!-- Background of PhotoSwipe. 
                                        It's a separate element as animating opacity is faster than rgba(). -->
                                        <div class="pswp__bg"></div>

                                        <!-- Slides wrapper with overflow:hidden. -->
                                        <div class="pswp__scroll-wrap">

                                            <!-- Container that holds slides. 
                                            PhotoSwipe keeps only 3 of them in the DOM to save memory.
                                            Don't modify these 3 pswp__item elements, data is added later on. -->
                                            <div class="pswp__container">
                                                <div class="pswp__item"></div>
                                                <div class="pswp__item"></div>
                                                <div class="pswp__item"></div>
                                            </div>

                                            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                            <div class="pswp__ui pswp__ui--hidden">

                                                <div class="pswp__top-bar">

                                                    <!--  Controls are self-explanatory. Order can be changed. -->

                                                    <div class="pswp__counter"></div>

                                                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                                    <button class="pswp__button pswp__button--share" title="Share"></button>

                                                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                                                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                                    <!-- element will get class pswp__preloader-active when preloader is running -->
                                                    <div class="pswp__preloader">
                                                        <div class="pswp__preloader__icn">
                                                            <div class="pswp__preloader__cut">
                                                                <div class="pswp__preloader__donut"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                                    <div class="pswp__share-tooltip"></div>
                                                </div>

                                                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                                                </button>

                                                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                                </button>

                                                <div class="pswp__caption">
                                                    <div class="pswp__caption__center"></div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                
                                <div class="tab-pane fade" id="account-vertical-info" role="tabpanel" aria-labelledby="account-pill-info" aria-expanded="false">
                                    
                                    @if (!empty($profiles->bank_info))
                                    
                                    <div class="row">

                                        <div class="col-12 mb-3">
                                            <h5>Bank Name</h5>
                                            <span class="text-bold">{{$profiles->bank_info->bank->bank_name}}</span>
                                            <h5>Account Name</h5>
                                            <span class="text-bold">{{$profiles->bank_info->account_name}}</span>
                                            <h5>Account Number</h5>
                                            <span class="text-bold">{{$profiles->bank_info->account_number}}</span>
                                            <h5>Account Verify</h5>
                                            @if($profiles->bank_info->account_verify == 'yes')
                                            <span class="badge badge-success">{{ucwords($profiles->bank_info->account_verify)}}</span>
                                            @else
                                            <span class="badge badge-warning">{{ucwords($profiles->bank_info->account_verify)}}</span>
                                            @endif
                                        </div>
                                        
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <a href="{{ route('member.myprofile.bank.edit') }}" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Edit</a>
                                        </div>
                                    </div>
                                    @else
                                    
                                    <div class="row">
                                        <div class="col-12 mb-3">
                                            <p>If you decide to publish your own e-voucher, then this info is mandatory.</p>
                                            <p>You can skip this now and fill up at the e-voucher page if needed.</p>
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                <a href="{{ route('member.myprofile.bank.new') }}" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Add</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- account setting page end -->
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/gallery/masonry/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/gallery/photo-swipe/photoswipe.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/gallery/photo-swipe/photoswipe-ui-default.min.js') }}"></script>
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
  <!--<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/polyfill.min.js') }}"></script>-->
@endsection
@section('page-script')

  <!-- Page js files -->
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/gallery/photo-swipe/photoswipe-script.js') }}"></script>
  @include('global/messages')
  
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/extensions/ext-component-toastr.js') }}"></script>
  <script type="text/javascript">
        $(function () {
            $(".btnSubmit").click(function (event) {
                var result = confirm("Are you sure wants to remove this ?");

                if (result == true) {
                    $('#form-gallery').submit();
                }

                else {
                    return false;
                }
            });
        });
  </script>
@endsection