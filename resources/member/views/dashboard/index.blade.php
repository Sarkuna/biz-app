@extends('layouts/fullLayoutMaster')

@section('title', 'Dashboard')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/weather-icons/climacons.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/fonts/meteocons/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/charts/morris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/charts/chartist.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/css/charts/chartist-plugin-tooltip.css') }}">  
  
@endsection

@section('page-style')
{{-- Page Css files --}}

<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/pages/timeline.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/pages/dashboard-ecommerce.css') }}">
   
@endsection

@section('content')
<div class="content-body">
    <!-- eCommerce statistic -->
    <div class="row">
        <div class="col-xl-3 col-lg-6 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-left">
                                <h3 class="info">850</h3>
                                <h6>Products Sold</h6>
                            </div>
                            <div>
                                <i class="icon-basket-loaded info font-large-2 float-right"></i>
                            </div>
                        </div>
                        <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                            <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xl-3 col-lg-6 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-left">
                                <h3 class="warning">$748</h3>
                                <h6>Net Profit</h6>
                            </div>
                            <div>
                                <i class="icon-pie-chart warning font-large-2 float-right"></i>
                            </div>
                        </div>
                        <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                            <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xl-6 col-lg-6 col-12">
            <div class="card">
                            <div class="card-header">
                                <h4 class="card-title text-center">My Points</h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body pt-0">
                                    <div class="row">
                                        <div class="col-md-6 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                                            <h4 class="font-large-2 text-bold-400">@convertpoint($point['points_balance'])</h4>
                                            <p class="blue-grey lighten-2 mb-0">Available Balance</p>
                                        </div>
                                        <div class="col-md-6 col-12 text-center">
                                            <h4 class="font-large-2 text-bold-400">@convertpoint($point['going_to_expiry'])</h4>
                                            <p class="blue-grey lighten-2 mb-0">Expiring on 31-12-2022</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div> 
        

    </div>
    <!--/ eCommerce statistic -->

    
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/charts/chartist.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/charts/raphael-min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/charts/morris.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/timeline/horizontal-timeline.js') }}"></script>
  <!--<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/polyfill.min.js') }}"></script>-->
@endsection
@section('page-script')

  <!-- Page js files -->
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
@endsection