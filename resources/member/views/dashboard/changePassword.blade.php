@extends('layouts/fullLayoutMaster')

@section('title', 'Password')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">

@endsection

@section('content')
<div class="content-body">
    
    <section id="basic-form-layouts">
        <div class="row justify-content-md-left">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-card-center">Change Password</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" method="POST" action="{{ route('member.change.password.store') }}">    
                            {{csrf_field()}}    
                                <div class="form-body">

                                    <div class="form-group">
                                        <label for="eventRegInput1">Current Password</label>
                                        <input type="password" id="current_password" class="form-control" placeholder="Current Password" name="current_password">
                                        @error('current_password')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="eventRegInput2">New Password</label>
                                        <input type="password" id="new_password" class="form-control" placeholder="New Password" name="new_password">
                                        <div id='password-strength-status'></div>
                                        @error('new_password')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                        <p id="passwordHelpBlock" class="form-text text-muted">
        Your password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.
</p>
                                    </div>

                                    <div class="form-group">
                                        <label for="eventRegInput3">New Confirm Password</label>
                                        <input type="password" id="new_confirm_password" class="form-control" placeholder="New Confirm Password" name="new_confirm_password">
                                        @error('new_confirm_password')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>

                                    
                                </div>

                                <div class="form-actions">
                                    <a href="{{ route('member.dashboard') }}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
 <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection
@section('page-script')


  <!-- Page js files -->
  @include('global/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
  <script>
    $("#new_password").keyup(function() {
        var number = /([0-9])/;
        var lowercase = /([a-z])/;
        var uppercase = /([A-Z])/;
        var special = /([!@#$%^&*])/;
        if ($('#new_password').val().length < 8) {
            $('#password-strength-status').removeClass();
            $('#password-strength-status').addClass('alert alert-danger');
            $('#password-strength-status').html("Weak (should be atleast 8 characters.)");
        } else {
            if ($('#new_password').val().match(number) && $('#new_password').val().match(lowercase) && $('#new_password').val().match(uppercase) && $('#new_password').val().match(special)) {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-success');
                $('#password-strength-status').html("Strong");
            } else {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-warning');
                $('#password-strength-status').html("Medium (should include a Uppercase, numbers, special character and Lowercase.)</>");
            }
        }
    });
    </script>
@endsection