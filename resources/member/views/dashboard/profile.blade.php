@extends('layouts/fullLayoutMaster')

@section('title', 'My Profile')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/js/gallery/photo-swipe/photoswipe.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css') }}"> 
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" type="text/css" href="{{ asset('themes/modern-admin/app-assets/css/pages/page-users.css') }}">

@endsection

@section('content')
<div class="content-body">
    <!-- account setting page start -->
    <section class="users-view">
        <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        
                        <div class="col-12">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>Name:</td>
                                        <td class="users-view-name">{{ $profile->full_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>E-mail:</td>
                                        <td class="users-view-email">{{ $profile->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Mobile:</td>
                                        <td class="users-view-email">{{ $profile->mobile }}</td>
                                    </tr>

                                    <tr>
                                        <td>Date Joined:</td>
                                        <td>{{ $profile->created_at->format('d-m-Y')}}</td>
                                    </tr>
                                    <tr>
                                        <td>Last Login Date:</td>
                                        <td>
                                            @if(!empty($profile->date_last_login))
                                                {{ $profile->date_last_login->format('d-m-Y')}}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- account setting page end -->
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
  <!--<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/polyfill.min.js') }}"></script>-->
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('global/messages')

  
@endsection