@extends('layouts/EcommerceLayout')

@section('title', 'Mebers Lists')
@section('vendor-style')
  <!-- vendor css files -->

@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/app-ecommerce.css') }}">
@endsection

@section('content')
<form action="{{ route('user.member.list.filter') }}" method="POST">
		@csrf
<div class="content-detached content-right">
    <div class="content-body">
        <!-- E-commerce Content Section Starts -->
        <section id="ecommerce-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ecommerce-header-items">
                        <div class="result-toggler">
                            <button class="navbar-toggler shop-sidebar-toggler" type="button" data-toggle="collapse">
                                <span class="navbar-toggler-icon d-block d-lg-none"><i data-feather="menu"></i></span>
                            </button>
   
                        </div>
                        <div class="view-options d-flex">
                            
                            <div class="single-shorter">
                                <label>Sort By :</label>
                                <select class='sortBy' name='sortBy' onchange="this.form.submit();">
                                    <option value="">Default</option>
                                    <option value="asc" @if(!empty($_GET['sortBy']) && $_GET['sortBy']=='asc') selected @endif>Name ASC</option>
                                    <option value="desc" @if(!empty($_GET['sortBy']) && $_GET['sortBy']=='desc') selected @endif>Name DESC</option>
                                </select>
                            </div>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-icon btn-outline-primary view-btn grid-view-btn">
                                    <input type="radio" name="radio_options" id="radio_option1" checked />
                                    <i data-feather="grid" class="font-medium-3"></i>
                                </label>
                                <label class="btn btn-icon btn-outline-primary view-btn list-view-btn">
                                    <input type="radio" name="radio_options" id="radio_option2" />
                                    <i data-feather="list" class="font-medium-3"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- E-commerce Content Section Starts -->

        <!-- background Overlay when sidebar is shown  starts-->
        <div class="body-content-overlay"></div>
        <!-- background Overlay when sidebar is shown  ends-->

        <!-- E-commerce Search Bar Starts -->
        <section id="ecommerce-searchbar" class="ecommerce-searchbar">
            <div class="row mt-1">
                <div class="col-sm-12">
                    <div class="input-group input-group-merge">
                        <input type="text" class="form-control search-product" id="shop-search" placeholder="Search" aria-label="Search..." aria-describedby="shop-search" name="companyname" />
                        <div class="input-group-append">
                            <span class="input-group-text"><i data-feather="search" class="text-muted"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- E-commerce Search Bar Ends -->

        <!-- E-commerce Products Starts -->
        <section id="ecommerce-products" class="grid-view">
            {{-- {{$members}} --}}
            @if(count($members)>0)
                @foreach($members as $member)
                    <div class="card ecommerce-card">
                        <div class="item-img text-center">
                            <a href="{{ route('user.member.list.view', $member->id)}}">
                                @if (!empty($member->photo))
                                <img class="img-fluid card-img-top" src="{{ asset('/storage/member_logo/'.$member->client_id.'/'.$member->photo)}}" alt="img-placeholder" />
                                @else
                                <img class="img-fluid card-img-top" src="{{ asset('themes/vuexy-admin-v6/images/no_image.png') }}" alt="No Image">
                                
                                @endif
                                
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="item-wrapper">
                                <div class="item-rating">
                                    Receipt pts: RM1 = {{ $member->point_receipt }}pts
                                </div>
                            </div>
                            <h6 class="item-name">
                                <a class="text-body" href="{{ route('user.member.list.view', $member->id)}}">{{ $member->name }}</a>
                                
                            </h6>
                            <p class="card-text item-description">
                                
                                {{ strip_tags($member->description) }}
                            </p>
                        </div>
                        <div class="item-options text-center">
                            
                            <a href="{{ route('user.member.list.view', $member->id)}}" class="btn btn-primary btn-block">
                                <i data-feather="file-text"></i>
                                <span class="add-to-cart23">Upload Receipt</span>
                            </a>
                        </div>
                    </div>
                @endforeach
            @else
                <h4 class="text-warning" style="margin:100px auto;">There are no products.</h4>
            @endif
        </section>
        <!-- E-commerce Products Ends -->

        <!-- E-commerce Pagination Starts -->
        <section id="ecommerce-pagination">
            <div class="row">
                <div class="col-sm-12" id="page-meb">
                    {{$members->appends($_GET)->links()}}
                    <!--{!! $members->appends(['sort' => 'department'])->links() !!}-->
                    
                </div>
            </div>
        </section>
        <!-- E-commerce Pagination Ends -->

    </div>
</div>
    
<div class="sidebar-detached sidebar-left">
    <div class="sidebar">
        <!-- Ecommerce Sidebar Starts -->
        <div class="sidebar-shop">
            <div class="row">
                <div class="col-sm-12">
                    <h6 class="filter-heading d-none d-lg-block">Filters</h6>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <!-- Categories Starts -->
                    <div id="product-categories">
                        <h6 class="filter-title mt-0">Categories of Members</h6>
                        
                        @if(count($client_categories)>0)                
                            <ul class="list-unstyled categories-list">
                                <li>
                                    <div class="custom-control custom-radio">
                                        <input onChange="this.form.submit();" type="radio" value="" id="category0" name="category" class="custom-control-input"  @if(empty($_GET['category'])) checked @endif />
                                        <label class="custom-control-label" for="category0">All</label>
                                    </div>
                                </li>
                                @foreach($client_categories as $client_category)
                                <li>
                                    <div class="custom-control custom-radio">
                                        <input onChange="this.form.submit();" type="radio" value="{{$client_category->category->id}}" id="category{{$client_category->category->id}}" name="category" class="custom-control-input" @if(!empty($_GET['category']) && $_GET['category']==$client_category->category->id) checked @endif />
                                        <label class="custom-control-label" for="category{{$client_category->category->id}}">{{$client_category->category->name}}</label>
                                    </div>
                                </li>
                                
                                @endforeach
                            </ul>                        
                            @else
                                <h4 class="text-warning" style="margin:100px auto;">There are no products.</h4>
                            @endif
                    </div>
                    <!-- Categories Ends -->

                   
                </div>
            </div>
        </div>
        <!-- Ecommerce Sidebar Ends -->

    </div>
</div>
</form>
@endsection

@section('vendor-script')
  <!-- vendor files -->

@endsection

@section('page-script')

  <!-- Page js files -->
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/pages/app-ecommerce.js') }}"></script>
  <script>
      $(function() {
        $('#page-meb ul').addClass('justify-content-center mt-2');
      });
      
      $(document).ready(function() {
        $(window).keydown(function(event){
          if(event.keyCode == 13) {
            this.form.submit();
          }
        });
      });
  </script>
  
@endsection

