@extends('layouts/fullLayoutMaster')

@section('title', 'Member Detail')
@section('vendor-style')
  <!-- vendor css files -->

@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/app-invoice-list.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/app-user.css') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
@endsection

@section('content')
<section class="app-user-view">
                    <!-- User Card & Plan Starts -->
                    <div class="row">
                        <!-- User Card starts-->
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <div class="card user-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                            <div class="user-avatar-section">
                                                <div class="d-flex justify-content-start">
                                                    @if (!empty($member_detail->photo))
                                                        <img class="img-fluid rounded" src="{{ asset('/storage/member_logo/'.$member_detail->client_id.'/'.$member_detail->photo)}}" height="100%" width="104" alt="{{$member_detail->name}}" />
                                                    @else
                                                        <img class="img-fluid rounded" src="{{ asset('themes/vuexy-admin-v6/images/noimage.jpg') }}" height="104" width="104" alt="No Image">
                                                    @endif
                                                    
                                                    <div class="d-flex flex-column ml-1">
                                                        <div class="user-info mb-1">
                                                            <h4 class="mb-0">{{ $member_detail->name }}</h4>
                                                            <span class="card-text">{{$member_detail->address1}} {{$member_detail->address2}} {{$member_detail->city}} {{$member_detail->post_code}} {{$member_detail->state}}
                                                        {{ucwords($member_detail->country)}}</span>
                                                        </div>
                                                        <div class="d-flex flex-wrap">
                                                            <form action="{{route('user.receipt.list.filter')}}" method="POST">
                                                                @csrf
                                                                <input type="hidden" name="category">
                                                                <input type="hidden" name="name" value="{{ $member_detail->id }}">
                                                            <button type="submit" class="btn btn-primary btn-xs-block">Upload Receipt</button>
                                                            <a href="" class="btn btn-outline-info ml-1 btn-xs-block">Refer A Friend</a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                                            <div class="user-info-wrapper">
                                                <div class="d-flex flex-wrap">
                                                    <div class="user-info-title">
                                                        <i data-feather="user" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">PIC Name</span>
                                                    </div>
                                                    <p class="card-text mb-0">{{ $member_detail->member_info->full_name }}</p>
                                                </div>
                                                <div class="d-flex flex-wrap my-50">
                                                    <div class="user-info-title">
                                                        <i data-feather="mail" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">Email</span>
                                                    </div>
                                                    <p class="card-text mb-0">{{ $member_detail->member_info->email }}</p>
                                                </div>
                                                
                                                <div class="d-flex flex-wrap">
                                                    <div class="user-info-title">
                                                        <i data-feather="phone" class="mr-1"></i>
                                                        <span class="card-text user-info-title font-weight-bold mb-0">Contact</span>
                                                    </div>
                                                    <p class="card-text mb-0">{{ $member_detail->member_info->mobile }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /User Card Ends-->
                    </div>
                    <!-- User Card & Plan Ends -->

                    <!-- User Timeline & Permissions Starts -->
                    <div class="row">
                        <!-- information starts -->
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title mb-2">About Company</h4>
                                </div>
                                <div class="card-body">
                                    {!! $member_detail->description !!}
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title mb-2">Company Portfolio</h4>
                                </div>
                                
                                <div class="card-body">
                                    @if (!empty($portfolios))
                                    <div class="row tz-gallery">
                                        @foreach ($portfolios as $key => $portfolio)
                                        <figure class="col-lg-3 col-md-6 col-12" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                            <a href="{{ asset('/storage/portfolio/'.$member_detail->client_id.'/'.$member_detail->ref.'/'.$key)}}" itemprop="contentUrl" data-size="480x360" class="lightbox">
                                                <img class="img-thumbnail img-fluid" src="{{ asset('/storage/portfolio/'.$member_detail->client_id.'/'.$member_detail->ref.'/'.$key)}}" itemprop="thumbnail" alt="Image description" />
                                            </a>
                                        </figure>
                                        @endforeach
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- information Ends -->
                    </div>
                    <!-- User Timeline & Permissions Ends -->

                    
                </section>
@endsection

@section('vendor-script')
  <!-- vendor files -->

@endsection

@section('page-script')

  <!-- Page js files -->
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/pages/app-user-view.js') }}"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
@endsection

