@extends('layouts/EcommerceLayout')

@section('title', 'Rewards')
@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/extensions/swiper.min.css') }}" />

@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/app-ecommerce-details.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/form-number-input.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/css/style.css') }}" />

@endsection

@section('content')
<!-- app e-commerce details start -->

<section class="app-ecommerce-details">
    <div class="card">
        <!-- Product Details starts -->
        <div class="card-body">
            <div class="row my-2">
                <div class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0" id="component-swiper-gallery">
                    <div class="gallery">
                        <div id="photo-viewer"></div>
                        <div id="thumbnails">
                            @php ($images = json_decode($product_detail->gallery_image->new_name,true)) @endphp
                             
                            @foreach($images as $image)
                                   <a href="{{config('app.siteoptions.reward_image_url')}}/storage/products/{{$product_detail->ref}}/{{$image}}" class="thumb active" title="{{$product_detail->title}}"><img src="{{config('app.siteoptions.reward_image_url')}}/storage/products/{{$product_detail->ref}}/thumbnail/{{$image}}"  class="img-fluid rounded" /></a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-7">
                    <form action="{{route('single-add-to-cart')}}" method="POST">
                        @csrf
                    <h4>{{config('app.siteoptions.point_shop')}} {{$product_detail->title}}</h4>
                    <span class="card-text item-company">By <a href="javascript:void(0)" class="company-name">{{$product_detail->brand_info['title']}}</a></span>
                    <div class="ecommerce-details-price d-flex flex-wrap mt-1">
                        <h4 class="item-price mr-1">
                            
                                @if($product_detail->discount > 0) 
                                    @php
                                        $price = $product_detail->sale_price-($product_detail->sale_price*$product_detail->discount)/100;                                        
                                        $discount = '<del style="padding-left:4%; position: absolute;">'.number_format($product_detail->sale_price / config('app.siteoptions.point_shop')).' '.config('app.siteoptions.point_prefix').'</del>';
                                    @endphp

                                @else
                                    @php
                                        $price = $product_detail->sale_price;
                                        $discount = "";
                                    @endphp
                                @endif
                            
                            {{number_format($price / config('app.siteoptions.point_shop'))}} {{config('app.siteoptions.point_prefix')}} 
                            {!! $discount !!}
                        </h4>

                    </div>
                    <p class="card-text">Stock - <span class="badge badge-success">{{$product_detail->stock}}</span></p>
                    <p class="card-text">Condition - <span class="text-success">{{$product_detail->condition}}</span></p>


                    
                    <div class="cart-item-qty">                        
                        <div class="input-group">
                            <span class="quantity-title">Qty:</span>
                            <input class="touchspin-cart" name="quant[1]" type="number" value="1" data-min="1" data-max="100">
                        </div>
                    </div>
                    <ul class="product-features list-unstyled">
                        <li><i data-feather="shopping-cart"></i> <span>Shipping</span></li>
                        <select class="form-control" id="add-type" name="shipping">
                            <option value="em">East Malaysia - {{number_format($product_detail->shipping_price_em / config('app.siteoptions.point_shop'))}} {{config('app.siteoptions.point_prefix')}}</option>
                            <option value="wm">West Malaysia - {{number_format($product_detail->shipping_price_wm / config('app.siteoptions.point_shop'))}} {{config('app.siteoptions.point_prefix')}}</option>
                        </select>

                        
                    </ul>
                    <hr />
                    <div class="product-color-options">
                        <p class="card-text">{!!($product_detail->summary)!!}</p>
                    </div>
                    <hr />
                    <div class="d-flex flex-column flex-sm-row pt-1">
                             
                        <input type="hidden" name="slug" value="{{$product_detail->slug}}">
                        <button type="submit" class="btn btn-primary mr-0 mr-sm-1 mb-1 mb-sm-0"><span class="add-to-cart"><i data-feather="shopping-cart" class="mr-50"></i>Add to cart</span></button>
                        
                    </div>
                    </form> 
                </div>
            </div>
            
            <div class="col-12 mb-md-0 mt-3">
                {!!($product_detail->description)!!}
            </div>
        </div>
        <!-- Product Details ends -->



        <!-- Related Products starts -->
        <div class="card-body">
            <div class="mt-4 mb-2 text-center">
                <h4>Related Products</h4>
                <p class="card-text">People also search for this items</p>
            </div>
            <div class="swiper-responsive-breakpoints swiper-container px-4 py-2">
                <div class="swiper-wrapper">
                    {{-- {{$product_detail->rel_prods}} --}}
                    @foreach($product_detail->rel_prods as $data)
                        @if($data->id !==$product_detail->id)
                        <!-- Start Single Product -->
                            <div class="swiper-slide">
                                <a href="{{route('product-detail',$data->slug)}}">
                                    <div class="item-heading">
                                        <h5 class="text-truncate mb-0">{{$data->title}}</h5>
                                        <small class="text-body">by {{$data->brand_info['title']}}</small>
                                    </div>
                                    <div class="img-container w-50 mx-auto py-75">
                                        <img src="{{config('app.siteoptions.reward_image_url')}}/storage/products/{{$data->photo}}" class="img-fluid" alt="image" />
                                    </div>
                                    <div class="item-meta">
                                        <p class="card-text text-primary mb-0">{{number_format($product_detail->sale_price / config('app.siteoptions.point_shop'))}} {{config('app.siteoptions.point_prefix')}}</p>
                                    </div>
                                </a>
                            </div>
                        <!-- End Single Product -->
                        @endif
                    @endforeach
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
        <!-- Related Products ends -->
    </div>
</section>
<!-- app e-commerce details end -->
@endsection

@section('vendor-script')
  <!-- vendor files -->
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js') }}"></script>
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/swiper.min.js') }}"></script>
@endsection

@section('page-script')
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/pages/app-ecommerce-details.js') }}"></script>
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/forms/form-number-input.js') }}"></script>
<script src="{{ asset('themes/vuexy-admin-v6/js/scripts/viewer.js') }}"></script>


@endsection

