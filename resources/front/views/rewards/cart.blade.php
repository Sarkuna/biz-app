@extends('layouts/EcommerceLayout')

@section('title', 'Cart')
@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/forms/wizard/bs-stepper.min.css') }}" />
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/app-ecommerce.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/form-wizard.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/form-number-input.css') }}" />

@endsection

@section('content')
<div class="content-wrapper">
    @php
        $cartlists = Helper::getAllProductFromCart();
    @endphp
    
    @if(count(Helper::getAllProductFromCart()) > 0)   
    <div id="place-order" class="list-view product-checkout">
        <!-- Checkout Place Order Left starts -->
        <div class="checkout-items">
            <form action="{{route('cart.update')}}" method="POST">
                @csrf
                
                @foreach($cartlists as $key=>$cart)
                <div class="card ecommerce-card">
                    <input type="hidden" name="qty_id[]" value="{{$cart->id}}">
                    <div class="item-img">
                        <a href="app-ecommerce-details.html">
                            <img src="{{config('app.siteoptions.reward_image_url')}}/storage/products/{{$cart->product['photo']}}" alt="img-placeholder" />
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="item-name">
                            <h6 class="mb-0"><a href="{{route('product-detail',$cart->product['slug'])}}" class="text-body">{{$cart->product['title']}}</a></h6>
                            <span class="item-company">By <a href="javascript:void(0)" class="company-name">{{$cart->product->brand_info['title']}}</a></span>

                        </div>
                        <span class="text-success mb-1">Unit Point  <span class="text-muted">{{$cart->quantity}} X {{$cart->point}}</span></span>

                        <div class="item-quantity">
                            <span class="quantity-title">Qty:</span>
                            <div class="input-group quantity-counter-wrapper">
                                <input type="text" name="quant[{{$key}}]" class="quantity-counter" data-min="1" data-max="100" value="{{$cart->quantity}}" />

                            </div>
                        </div>
                    </div>
                    <div class="item-options text-center">
                        <div class="item-wrapper">
                            <div class="item-cost">
                                <h4 class="item-price">{{number_format($cart->total_point)}} {{config('app.siteoptions.point_prefix')}}</h4>
                                <p class="shipping">
                                    <i class="feather icon-truck"></i>
                                    @if($cart->shipping_point_em > 0)
                                        {{number_format($cart->shipping_point_em)}} {{config('app.siteoptions.point_prefix')}}
                                    @elseif ($cart->shipping_point_wm > 0)
                                        {{number_format($cart->shipping_point_wm)}} {{config('app.siteoptions.point_prefix')}}
                                    @else
                                        Free
                                    @endif
                                </p>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-light btn-cart">
                            <i data-feather="edit" class="align-middle mr-25"></i>
                            <span class="text-truncate">Update</span>
                        </button>
                        <a href="{{route('cart-delete',$cart->id)}}" class="btn btn-danger mt-1 remove-wishlist"><i data-feather="x" class="align-middle mr-25"></i>
                            <span>Remove</span></a>
                    </div>
                </div>
                @endforeach
            </form>
        </div>
        <!-- Checkout Place Order Left ends -->

        <!-- Checkout Place Order Right starts -->
        <div class="checkout-options">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Shipping</h4>
                </div>
                <div class="card-body">
                    <p class="card-text mb-0"><b>{{$default_address->full_name}} <a class="float-right" href="/addressbook">Edit</a></b></p>                    
                    <p class="mb-0">{{$default_address->phone}}</p>
                    <p class="mb-0">{{$default_address->address}}, {{$default_address->area}}, {{$default_address->post_code}}, {{$default_address->state}}</p>

                    
                    <hr />
                    <div class="price-details">
                        <h6 class="price-title">Order Summary</h6>
                        <ul class="list-unstyled">
                            <li class="price-detail">
                                <div class="detail-title">Your Current Balance</div>
                                <div class="detail-amt text-info text-bold-600">@convertpoint($userpoint['user_points_balance']) {{config('app.siteoptions.point_prefix')}}</div>
                            </li>
                            <li class="price-detail">
                                <div class="detail-title">Sub Total</div>
                                <div class="detail-amt discount-amt text-success">{{number_format($cartlists->sum('total_point'))}} {{config('app.siteoptions.point_prefix')}}</div>
                            </li>
                            
                            <li class="price-detail">
                                <div class="detail-title">Delivery Charges</div>
                                <div class="detail-amt discount-amt text-success">{{number_format($cartlists->sum('shipping_point_em') + $cartlists->sum('shipping_point_wm'))}} {{config('app.siteoptions.point_prefix')}}</div>
                            </li>
                        </ul>
                        <hr />
                        <ul class="list-unstyled">
                            <li class="price-detail">
                                <div class="detail-title detail-total">Total</div>
                                <div class="detail-amt font-weight-bolder">{{number_format($cartlists->sum('total_point') + $cartlists->sum('shipping_point_em') + $cartlists->sum('shipping_point_wm'))}} {{config('app.siteoptions.point_prefix')}}</div>
                            </li>
                            <li class="price-detail">
                                <div class="detail-title detail-total">New Balance</div>
                                <div class="detail-amt font-weight-bolder text-primary">
                                    @php
                                        $cart_total = $cartlists->sum('total_point') + $cartlists->sum('shipping_point_em') + $cartlists->sum('shipping_point_wm');
                                        $newbalance = $userpoint['user_points_balance'] - $cart_total;
                                    @endphp
                                    
                                    {{number_format($newbalance) }} {{config('app.siteoptions.point_prefix')}}
                                </div>
                            </li>
                        </ul>
                        
                        @if($userpoint['user_points_balance'] > $cart_total)
                            <a href="{{route('checkout')}}" class="btn btn-primary btn-block btn-next place-order">Checkout</a>
                        @else
                            <div class="demo-spacing-0">
                                <div class="alert alert-danger mt-1 alert-validation-msg" role="alert">
                                    <div class="alert-body">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info mr-50 align-middle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                        <span><strong>Sorry!</strong>. Your point balance is insufficient to make this order. Plese remove if u add more products.</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- Checkout Place Order Right ends -->
        </div>
    </div>
    <!-- Checkout Place order Ends -->
    @else
        <p class="text-center">Your shopping cart is empty. <a href="{{route('product-grids')}}" style="color:blue;">Continue shopping</a></p>
    @endif
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/forms/wizard/bs-stepper.min.js') }}"></script>
@endsection

@section('page-script')
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/pages/app-ecommerce-checkout.js') }}"></script>
  <!-- Page js files -->
  
@endsection

