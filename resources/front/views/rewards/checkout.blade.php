@extends('layouts/EcommerceLayout')

@section('title', 'Checkout')
@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/forms/wizard/bs-stepper.min.css') }}" />
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/app-ecommerce.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/form-wizard.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/form-number-input.css') }}" />

@endsection

@section('content')
<div class="content-wrapper">
    @php
        $cartlists = Helper::getAllProductFromCart();
        $totalitems = count(Helper::getAllProductFromCart());
    @endphp
    
    @if(count(Helper::getAllProductFromCart()) > 0)
    <form id="checkout-payment" class="list-view product-checkout" method="POST" action="{{route('cart.order')}}">

        <div class="amount-payable checkout-options">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Order Summary</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled price-details">
                        <li class="price-detail">
                            <div class="details-title">Point of {{$totalitems}} items without delivery charges</div>
                            <div class="detail-amt">
                                <strong>$699.30</strong>
                            </div>
                        </li>
                        <li class="price-detail">
                            <div class="details-title">Delivery Charges</div>
                            <div class="detail-amt discount-amt text-success">Free</div>
                        </li>
                    </ul>
                    <hr>
                    <ul class="list-unstyled price-details">
                        <li class="price-detail">
                            <div class="details-title">Point Payable</div>
                            <div class="detail-amt font-weight-bolder">$699.30</div>
                        </li>
                    </ul>
                    <hr>
                    <div class="form-group field-agreeform-agree required has-error">
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="validationCheckBootstrap" required="">
                                <label class="custom-control-label" for="validationCheckBootstrap">I have read and agree to the <a data-toggle="modal" href="#myModal">Terms &amp; Conditions</a></label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary place-order" type="submit">CONFIRM ORDER</button>
                </div>
            </div>
        </div>
    </form>
    

    @else
        <p class="text-center">Your shopping cart is empty. <a href="{{route('product-grids')}}" style="color:blue;">Continue shopping</a></p>
    @endif
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/forms/wizard/bs-stepper.min.js') }}"></script>
@endsection

@section('page-script')
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/pages/app-ecommerce-checkout.js') }}"></script>
  <!-- Page js files -->
  
@endsection

