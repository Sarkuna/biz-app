@extends('layouts/EcommerceLayout')

@section('title', 'Events')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/assets/css/event.css') }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/app-ecommerce.css') }}">
@endsection


@section('content')
<div class="content-body">
    <section id="pagination">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="card card-event">
                    <div class="img-wrap img-lazy-load b-loaded" style="background-image: url(https://eventic.mtrsolution.com/uploads/events/5f9ea98f0b308065460986.jpg);">
                        <a href="/en/event/camping-trip"></a>
                    </div>

                    <div class="info-wrap">

                        <div class="event-info">
                            <h5>
                                <a href="/en/event/camping-trip" class="text-dark">Sample Event</a>
                            </h5>

                            <div class="text-black-50 small">
                                ATLANTIQUE PARC: Les Mathes, France
                                &nbsp;
                            </div>
                            <div class="text-black-50 small">
                                Mon 28 Mar 2022, 1:00 PM EDT
                                &nbsp;
                            </div>
                        </div>
                        <div class="price-wrap">
                            <span class="price-new">110$</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <span class="event-category">
                        Trip / Camp
                    </span>

                    <div class="event-date text-center">
                        <div class="event-month bg-primary text-light">MAR</div>
                        <div class="event-day bg-white">28</div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-4 col-md-6 col-12">
                <div class="card card-event">
                    <div class="img-wrap img-lazy-load b-loaded" style="background-image: url(https://eventic.mtrsolution.com/uploads/events/5f9ea98f0b308065460986.jpg);">
                        <a href="/en/event/camping-trip"></a>
                    </div>

                    <div class="info-wrap">

                        <div class="event-info">
                            <h5>
                                <a href="/en/event/camping-trip" class="text-dark">Sample Event 2</a>
                            </h5>

                            <div class="text-black-50 small">
                                ATLANTIQUE PARC: Les Mathes, France
                                &nbsp;
                            </div>
                            <div class="text-black-50 small">
                                Mon 28 Mar 2022, 1:00 PM EDT
                                &nbsp;
                            </div>
                        </div>
                        <div class="price-wrap">
                            <span class="price-new">110$</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <span class="event-category">
                        Trip / Camp
                    </span>

                    <div class="event-date text-center">
                        <div class="event-month bg-primary text-light">MAR</div>
                        <div class="event-day bg-white">28</div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-4 col-md-6 col-12">
                <div class="card card-event">
                    <div class="img-wrap img-lazy-load b-loaded" style="background-image: url(https://eventic.mtrsolution.com/uploads/events/5f9ea98f0b308065460986.jpg);">
                        <a href="/en/event/camping-trip"></a>
                    </div>

                    <div class="info-wrap">

                        <div class="event-info">
                            <h5>
                                <a href="/en/event/camping-trip" class="text-dark">Sample Event 3</a>
                            </h5>

                            <div class="text-black-50 small">
                                ATLANTIQUE PARC: Les Mathes, France
                                &nbsp;
                            </div>
                            <div class="text-black-50 small">
                                Mon 28 Mar 2022, 1:00 PM EDT
                                &nbsp;
                            </div>
                        </div>
                        <div class="price-wrap">
                            <span class="price-new">110$</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <span class="event-category">
                        Trip / Camp
                    </span>

                    <div class="event-date text-center">
                        <div class="event-month bg-primary text-light">MAR</div>
                        <div class="event-day bg-white">28</div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->

@endsection

@section('page-script')

  <!-- Page js files -->
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/pages/app-ecommerce.js') }}"></script>
  
  
@endsection

