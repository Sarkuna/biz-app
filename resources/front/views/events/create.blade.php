@extends('layouts/fullLayoutMaster')

@section('title', 'Events')

@section('vendor-style')
  <!-- vendor css files -->
  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/forms/validation/form-validation.css') }}">  
<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">
  
@endsection

@section('page-style')
{{-- Page Css files --}}


<link rel="stylesheet" href="{{ asset('themes/modern-admin/app-assets/css/plugins/extensions/toastr.css')}}">      
<style>
.col-form-label.required:after, label.required:after {
    content: "*";
    color: #e95b35;
}
</style>
@endsection

@section('content')
<div class="content-body">
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Add a new event</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">

                            <form method="post" action="{{route('events.store')}}" class="form form-horizontal" novalidate>
                            {{csrf_field()}}
                            <div class="form-body">
                                <h4 class="form-section"><i class="ft-calendar"></i> Event Info</h4>
                                <div class="form-group row">
                                    <label for="audiences" class="col-md-4 label-control required">Audiences</label>
                                    <div class="col-md-8 mx-auto">
                                        <select name="audiences" class="form-control" required>
                                            <option value="" selected>Pick Audiences</option>
                                            <option value="All">All</option>
                                            <option value="Customers">Customers</option>
                                            <option value="Members">Members</option>
                                        </select>
                                        @error('audiences')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 label-control required">Event Name</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('name')}}" id="projectinput1" class="form-control" placeholder="Event Name" name="name" required>
                                        @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 label-control required">Event Date & Time</label>
                                    
                                    <div class="col-md-4 mx-auto">
                                        <input type="datetime-local" class="form-control" name="starts_on" id="starts_on" value="2022-01-26T09:00:00">
                                        @error('starts_on')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-4 mx-auto">
                                        <input type="datetime-local" class="form-control" name="ends_on" id="ends_on" value="2022-01-26T17:59:00">
                                        @error('ends_on')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 label-control">Artists <small class="block">Enter the list of artists that will perform in your event (press Enter after each entry)</small></label>
                                    
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('artists')}}" id="projectinput1" class="form-control" placeholder="Event Artists Name" name="artists">
                                        @error('artists')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 label-control required">Is this event date online ?</label>
                                    
                                    <div class="col-md-8 mx-auto">
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input" name="event_online" value="No" id="radio1" checked>
                                            <label class="custom-control-label" for="radio1">No</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input" name="event_online" value="Yes" id="radio2">
                                            <label class="custom-control-label" for="radio2">Yes</label>
                                        </div>
                                    </div>                                    
                                </div>
                                
                                <div class="form-group row">                                        
                                    <label for="venue_id" class="col-md-4 label-control">Venue</label>
                                    <div class="col-md-8 mx-auto">
                                        <select name="venue_id" class="form-control" required>
                                            <option value="" selected>Pick Venue</option>
                                            @foreach($venueslist as $key=>$venue)
                                                <option value='{{$venue->id}}'>{{$venue->venue_name}}</option>
                                            @endforeach
                                        </select>
                                        @error('venue_id')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="online_link" class="col-md-4 label-control">Online Link</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('online_link')}}" id="online_link" class="form-control" placeholder="Event online link" name="online_link">
                                        @error('online_link')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="description" class="col-md-4 label-control">Description</label>
                                    <div class="col-md-8 mx-auto">
                                        <textarea id="classic-editor" rows="10" cols="80" class="form-control" name="description" placeholder="About Event"></textarea>
                                        @error('description')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="description" class="col-md-4 label-control">Main event image <small class="block">Choose the right image to represent your event (We recommend using at least a 1200x600px (2:1 ratio) image )</small></label>
                                    <div class="col-md-8 mx-auto">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile02">
                                            <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFile02">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <h4 class="form-section"><i class="ft-archive"></i> Event tickets</h4>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 label-control required">Enable sales for this ticket?</label>
                                    
                                    <div class="col-md-8 mx-auto">
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input" name="ticket" value="No" checked id="ticket1">
                                            <label class="custom-control-label" for="ticket1">No</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input" name="ticket" value="Yes" id="ticket2">
                                            <label class="custom-control-label" for="ticket2">Yes</label>
                                        </div>
                                    </div>                                    
                                </div>
                                
                                <div class="form-group row">
                                    <label for="price" class="col-md-4 label-control required">Price RM</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('price')}}" id="price" class="form-control" placeholder="Price" name="price">
                                        @error('price')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="promotional_price" class="col-md-4 label-control required">Promotional Price RM</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('promotional_price')}}" id="promotional_price" class="form-control" placeholder="Promotional Price" name="promotional_price">
                                        @error('promotional_price')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="quantity" class="col-md-4 label-control required">Quantity</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('quantity')}}" id="quantity" class="form-control" placeholder="Quantity" name="quantity">
                                        @error('quantity')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="per_attendee" class="col-md-4 label-control">Per Attendee</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('per_attendee')}}" id="per_attendee" class="form-control" placeholder="Per Attendee" name="per_attendee">
                                        @error('per_attendee')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="voting_duration" class="col-md-4 label-control">Voting Duration</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('voting_duration')}}" id="voting_duration" class="form-control" placeholder="Voting Duration Day" name="voting_duration">
                                        @error('voting_duration')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="points" class="col-md-4 label-control">Points</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('points')}}" id="points" class="form-control" placeholder="Points" name="points">
                                        @error('points')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <h4 class="form-section"><i class="ft-globe"></i> Social Media Link</h4>
                                <div class="form-group row">
                                    <label for="youtube_video_url" class="col-md-4 label-control">Youtube</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('youtube_video_url')}}" id="youtube_video_url" class="form-control" placeholder="Event youtube link" name="youtube_video_url">
                                        @error('youtube_video_url')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="external_link" class="col-md-4 label-control">External <small class="block">If your event has a dedicated website, enter its url here</small></label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('external_link')}}" id="external_link" class="form-control" placeholder="Event external link" name="external_link">
                                        @error('external_link')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="instagram" class="col-md-4 label-control">Instagram</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('instagram')}}" id="instagram" class="form-control" placeholder="Event instagram link" name="instagram">
                                        @error('instagram')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="facebook" class="col-md-4 label-control">Facebook</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('facebook')}}" id="youtube_video_url" class="form-control" placeholder="Event facebook link" name="facebook">
                                        @error('facebook')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="linkedin" class="col-md-4 label-control">Linkedin</label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('linkedin')}}" id="linkedin" class="form-control" placeholder="Event linkedin link" name="linkedin">
                                        @error('linkedin')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <h4 class="form-section"><i class="ft-user"></i> Contact Info</h4>
                                <div class="form-group row">
                                    <label for="contact_phone_number" class="col-md-4 label-control">Phone Number <small class="block">Enter the phone number to be called for inquiries</small></label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('contact_phone_number')}}" id="contact_phone_number" class="form-control" placeholder="Contact Phone Number" name="contact_phone_number">
                                        @error('contact_phone_number')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_email_address" class="col-md-4 label-control">Email Address <small class="block">Enter the email address to be reached for inquiries</small></label>
                                    <div class="col-md-8 mx-auto">
                                        <input type="text" value="{{old('contact_email_address')}}" id="contact_email_address" class="form-control" placeholder="Contact Email Address" name="contact_email_address">
                                        @error('contact_email_address')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <h4 class="form-section"><i class="ft-settings"></i> Notification</h4>
                                <div class="form-group row">
                                    <label for="notification" class="col-md-4 label-control required">Enable email notification?</label>
                                    
                                    <div class="col-md-8 mx-auto">
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input" name="notification" value="No" checked id="notification1">
                                            <label class="custom-control-label" for="notification1">No</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input" name="notification" value="Yes" id="notification2">
                                            <label class="custom-control-label" for="notification2">Yes</label>
                                        </div>
                                    </div>                                    
                                </div>
                                
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 label-control required">Enable review?</label>
                                    
                                    <div class="col-md-8 mx-auto">
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input" name="reviews" value="No" checked id="reviews1">
                                            <label class="custom-control-label" for="reviews1">No</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input" name="reviews" value="Yes" id="reviews2">
                                            <label class="custom-control-label" for="reviews2">Yes</label>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>

                                <div class="form-actions">
                                    <a href="{{route('events.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                                    
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </section>
</div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection
@section('page-script')

  <!-- Page js files -->

  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/extensions/toastr.js') }}"></script>
  <script type="text/javascript">
    CKEDITOR.replace('description', {
        allowedContent : true,
        filebrowserBrowseUrl : '/admin/elfinder/ckeditor',
        filebrowserImageBrowseUrl : '/admin/elfinder/ckeditor',
        //uiColor : '#9AB8F3',
        height : 600
    });
</script>
 <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/custom-file-input.js') }}"></script>
@endsection