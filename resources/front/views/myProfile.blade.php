@extends('layouts/fullLayoutMaster')

@section('title', 'My Profile')

@section('vendor-style')
  <!-- vendor css files -->

@endsection

@section('page-style')
{{-- Page Css files --}}

@endsection

@section('content')
<div class="content-body">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-transaction">
                <div class="card-body">                    
                    
                    <div class="transaction-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="transaction-title">Full Name</h6>
                            </div>
                        </div>
                        <div class="font-weight-bolder">
                            {{ Auth::user()->full_name }}

                        </div>
                    </div>
                    
                    <div class="transaction-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="transaction-title">Mobile</h6>
                            </div>
                        </div>
                        <div class="font-weight-bolder">{{ Auth::user()->mobile }}</div>
                    </div>
                    
                    <div class="transaction-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="transaction-title">Date of Birth</h6>
                            </div>
                        </div>
                        <div class="font-weight-bolder">
                            @if(!empty(Auth::user()->dob))
                                {{ Auth::user()->dob->format('d M Y') }}
                            @else
                                N/A
                            @endif
                        </div>
                    </div>
                    
                    <div class="transaction-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="transaction-title">Gender</h6>
                            </div>
                        </div>
                        <div class="font-weight-bolder">
                            @if(!empty(Auth::user()->gender))
                                {{ ucwords(Auth::user()->gender) }}
                            @else
                                N/A
                            @endif
                            	
                        </div>
                    </div>
                    
                    <a href="{{ route('user.my.profile.edit') }}" class="btn btn-primary mr-1 mt-1 waves-effect waves-float waves-light">Edit</a>
                    <a href="{{ route('user.change.password') }}" class="btn btn-info mt-1 waves-effect">Change Password</a>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="card card-transaction">
                <div class="card-body">
                    <div class="transaction-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="transaction-title">Code</h6>
                            </div>
                        </div>
                        <div class="font-weight-bolder">{{ Auth::user()->customer_code }}</div>
                    </div>
                    
                    <div class="transaction-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="transaction-title">Email</h6>
                            </div>
                        </div>
                        <div class="font-weight-bolder">{{ Auth::user()->email }}</div>
                    </div>
                    
                    <div class="transaction-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="transaction-title">Date Last Login</h6>
                            </div>
                        </div>
                        <div class="font-weight-bolder">
                            @if(!empty(Auth::user()->date_last_login))
                            {{ Auth::user()->date_last_login->format('d M Y g:i a') }}
                            @else
                            N/A
                            @endif
                        </div>
                    </div>
                    
                    
                    <div class="transaction-item">
                        <div class="media">

                            <div class="media-body">
                                <h6 class="transaction-title">Status</h6>

                            </div>
                        </div>
                        <div class="font-weight-bolder">
                            @if(Auth::user()->status=='active')
                            <span class="badge badge-success">{{ucwords(Auth::user()->status)}}</span>
                            @elseif(Auth::user()->status=='declined')
                            <span class="badge badge-danger">{{ucwords(Auth::user()->status)}}</span>
                            @else
                            <span class="badge badge-warning">{{ucwords(Auth::user()->status)}}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection

@section('vendor-script')

@endsection

@section('page-script')

        
@endsection