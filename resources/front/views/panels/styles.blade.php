     
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/vendors.min.css') }}" />
{{-- Vendor Styles --}}
@yield('vendor-style')
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/extensions/toastr.min.css') }}">

{{-- Theme Styles --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/bootstrap-extended.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/colors.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/components.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/themes/dark-layout.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/themes/bordered-layout.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/themes/semi-dark-layout.css') }}">


{{-- Page Styles --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/core/menu/menu-types/vertical-menu.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/form-validation.css') }}" >
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/extensions/ext-component-toastr.css')}}">  

{{-- Page Styles --}}
@yield('page-style')


{{-- user custom styles --}}
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/assets/css/style.css') }}" />
