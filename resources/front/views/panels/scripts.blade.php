    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/vendors.min.js') }}"></script>
    @yield('vendor-script')
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    @yield('page-vendor')
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/core/app.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    
    @include('panels/messages')
    <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/extensions/ext-component-toastr.js') }}"></script>
    @yield('page-script')
    <!-- END: Page JS-->

