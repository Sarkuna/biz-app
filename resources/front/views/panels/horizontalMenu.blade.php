<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ route('user.home') }}">
                        <span class="brand-logo">
                            @if (!empty(Helper::client_info()['image']))
                                <!--<img src="/storage/bb_logo.png" alt="branding logo" class="rounded mr-75" alt="profile image" height="64" width="64">-->
                                <img src="{{ asset('/storage/images/'.Helper::client_info()['image'])}}" class="rounded" width="200">
                              @else
                                <img src="/storage/bb_logo.png" alt="branding logo">
                              @endif
                        </span>
                        <h2 class="brand-text">{{ Helper::client_info()['name'] }}</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li>
                    <a class="d-flex align-items-center" href="{{ route('user.home') }}"><i data-feather="home"></i><span class="menu-item text-truncate" data-i18n="Analytics">Home</span></a>
                </li>
                
                <li class=" navigation-header">
                    <span data-i18n="Apps &amp; Pages">Apps &amp; Pages</span><i data-feather="more-horizontal"></i>
                </li>
                
                <li class=" nav-item {{ (request()->is(['user/member-list*'])) ? 'active' : '' }}">
                    <a class="d-flex align-items-center" href="{{ route('user.member.list') }}"><i data-feather="users"></i><span class="menu-title text-truncate" data-i18n="Member List">Member List</span></a>
                </li>
                <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Invoice">Receipt</span></a>
                    <ul class="menu-content">
                        <li class="{{ (request()->is(['user/receipts','user/receipts/*/view'])) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('receipts.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">List</span></a></li>
                        <li class="{{ (request()->is(['user/receipts/create'])) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('receipts.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Upload">Upload Receipt</span></a></li>
                    </ul>
                </li>
                <li class=" nav-item {{ (request()->is(['user/rewards*'])) ? 'active' : '' }}">
                    <a class="d-flex align-items-center" href="{{ route('product-grids') }}"><i data-feather="gift"></i><span class="menu-title text-truncate" data-i18n="Member List">Rewards</span></a>
                </li>
                <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="calendar"></i><span class="menu-title text-truncate" data-i18n="Invoice">Events</span></a>
                    <ul class="menu-content">
                        <li class="{{ (request()->is(['user/events/upcoming-events','user/events*/view'])) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('upcoming.events.list') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Upcoming events</span></a></li>
                        <li class="{{ (request()->is(['user/events/past-events'])) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('past.events.list') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Upload">Past events</span></a></li>
                    </ul>
                </li>
                <li class=" navigation-header"><span data-i18n="Manage My Account">Manage My Account</span><i data-feather="more-horizontal"></i>
                    <li class=" nav-item {{ (request()->is(['user/profile*'])) ? 'active' : '' }}">
                        <a class="d-flex align-items-center" href=""><i data-feather="user"></i>
                            <span class="menu-title text-truncate" data-i18n="My Profile">My Profile</span></a>
                    </li>        
                    
                    <li class=" nav-item {{ (request()->is(['user/shipping*'])) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('shipping.index') }}"><i data-feather="book"></i><span class="menu-title text-truncate" data-i18n="Address Book">Address Book</span></a></li>
                
                
                
            </ul>
        </div>
    </div>