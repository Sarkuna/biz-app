<!-- BEGIN: Header-->
    <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow">
        <div class="navbar-container d-flex content">
            <div class="bookmark-wrapper d-flex align-items-center">
                <ul class="nav navbar-nav d-xl-none">
                    <li class="nav-item">
                        <a class="nav-link menu-toggle" href="javascript:void(0);">
                            <i class="ficon" data-feather="menu"></i>
                        </a>
                    </li>
                </ul>
                
            </div>
            <ul class="nav navbar-nav align-items-center ml-auto">

                <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a></li>
                
                <li class="nav-item dropdown dropdown-cart mr-25">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="shopping-cart"></i><span class="badge badge-pill badge-primary badge-up cart-item-count">{{count(Helper::getAllProductFromCart())}}</span></a>
                    <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                        <li class="dropdown-menu-header">
                            <div class="dropdown-header d-flex">
                                <h4 class="notification-title mb-0 mr-auto">My Cart</h4>
                                <div class="badge badge-pill badge-light-primary">{{count(Helper::getAllProductFromCart())}} Items</div>
                            </div>
                        </li>
                        <li class="scrollable-container media-list">
                            
                            @if(count(Helper::getAllProductFromCart()) > 0)
                                @foreach(Helper::getAllProductFromCart() as $data)
                                <div class="media align-items-center"><img class="d-block rounded mr-1" src="{{config('app.siteoptions.reward_image_url')}}/storage/products/{{$data->product['photo']}}" alt="donuts" width="62">
                                    <div class="media-body"><a href="{{route('cart-delete',$data->id)}}"><i class="ficon cart-item-remove" data-feather="x"></i></a>
                                        <div class="media-heading">
                                            <h6 class="cart-item-title"><a class="text-body" href="{{route('product-detail',$data->product['slug'])}}"> {{$data->product['title']}}</a></h6><small class="cart-item-by">{{$data->quantity}} X {{$data->point}} {{config('app.siteoptions.point_prefix')}}</small>
                                        </div>

                                        <h5 class="cart-item-price">{{number_format($data->total_point)}} {{config('app.siteoptions.point_prefix')}}</h5>
                                    </div>
                                </div>
                                @endforeach
                            @else
                                <p class="text-center mt-75">There are no items in this cart</p>
                            @endif
                        </li>
                        <li class="dropdown-menu-footer">
                            <div class="d-flex justify-content-between mb-1">
                                <h6 class="font-weight-bolder mb-0">Total:</h6>
                                <h6 class="text-primary font-weight-bolder mb-0">{{number_format(Helper::totalCartPrice())}} {{config('app.siteoptions.point_prefix')}}</h6>
                            </div><a class="btn btn-primary btn-block" href="{{route('cart')}}">View Cart</a>
                        </li>
                    </ul>
                </li>
                
                <li class="nav-item dropdown dropdown-notification mr-25"><a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="bell"></i><!--<span class="badge badge-pill badge-danger badge-up">5</span>--></a>
                    <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                        <li class="dropdown-menu-header">
                            <div class="dropdown-header d-flex">
                                <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                                <!--<div class="badge badge-pill badge-light-primary">6 New</div>-->
                            </div>
                        </li>
                        <li class="scrollable-container media-list">
                            <p class="text-center mt-75">There are no notifications</p>
                            <!--<div class="media d-flex align-items-center">
                                
                                <h6 class="font-weight-bolder mr-auto mb-0 text-center">No Notifications</h6>
                                <div class="custom-control custom-control-primary custom-switch">
                                    <input class="custom-control-input" id="systemNotification" type="checkbox" checked="">
                                    <label class="custom-control-label" for="systemNotification"></label>
                                </div>
                            </div>-->
                            
                        </li>
                        <!--<li class="dropdown-menu-footer"><a class="btn btn-primary btn-block" href="javascript:void(0)">Read all notifications</a></li>-->
                    </ul>
                </li>
                <li class="nav-item dropdown dropdown-user">
                    <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="user-nav d-sm-flex d-none">
                            <span class="user-name font-weight-bolder">{{ Auth::user()->full_name }}</span>
                            <span class="user-status">Online</span>
                        </div>
                        <span class="avatar">
                            <img class="round" src="{{ Helper::user_avatar() }}" alt="{{ Auth::user()->full_name }}" height="40" width="40">
                            <span class="avatar-status-online"></span>                                
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                        <a class="dropdown-item" href="{{ route('user.my.profile') }}"><i class="mr-50" data-feather="user"></i> Profile</a>
                        <a class="dropdown-item" href="{{ route('user.change.password') }}"><i class="mr-50" data-feather="lock"></i> Change Password </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void" onclick="$('#logout-form-user').submit();"><i class="mr-50" data-feather="power"></i> Logout</a>
                        <form id="logout-form-user" action="{{ route('user.logout.submit') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <!-- END: Header-->