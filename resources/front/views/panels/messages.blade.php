@if ($message = Session::get('success'))
{{ $message }}
<script>
    toastr['success']('{{ $message }}', 'Success!', {
      closeButton: true,
      tapToDismiss: false,
    });
</script>
@endif


@if ($message = Session::get('error'))
<script>
      toastr['error']('{{ $message }}', 'Error!', {
      closeButton: true,
      tapToDismiss: false,
    });
    </script>
@endif


@if ($message = Session::get('warning'))
<script>
    toastr['warning']('{{ $message }}', 'Warning!', {
      closeButton: true,
      tapToDismiss: false,
    });
</script>
@endif


@if ($message = Session::get('info'))
<script>
    toastr['info']('{{ $message }}', 'Info!', {
      closeButton: true,
      tapToDismiss: false,
    });
</script>
@endif


@if ($errors->any())
<script>
    toastr['warning']('Please check the form below for errors', 'Warning!', {
      closeButton: true,
      tapToDismiss: false,
    });
</script>
@endif