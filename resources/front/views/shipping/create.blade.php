@extends('layouts/fullLayoutMaster')

@section('title', 'New Address')
@section('vendor-style')
  <!-- vendor css files -->

@endsection

@section('page-style')
{{-- Page Css files --}}

@endsection

@section('content')
<!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
    <div class="row">
        <div class="col-md-7 col-12">
            <div class="card">

                <div class="card-body">
                    <form class="form form-horizontal" method="post" action="{{route('shipping.store')}}">
                    {{csrf_field()}}    
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="first-name">Full Name</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="text" id="first-name" class="form-control" name="full_name" placeholder="Full Name">
                                        @error('full_name')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="contact-info">Phone Number</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="number" id="phone" class="form-control" name="phone" placeholder="Phone Number">
                                        @error('phone')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="address">Address</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <textarea class="form-control" placeholder="House Number, Building, Street Name" maxlength="128" name="address"></textarea>
                                        @error('address')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="first-name">State</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control" id="basicSelect" name="state">
                                            <option value="">--Select--</option>
                                            <option value="Johor">Johor</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Kelantan">Kelantan</option>
                                            <option value="Kuala Lumpur">Kuala Lumpur</option>
                                            <option value="Labuan">Labuan</option>
                                            <option value="Melaka">Melaka</option>
                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                            <option value="Pahang">Pahang</option>
                                            <option value="Penang">Penang</option>
                                            <option value="Perak">Perak</option>
                                            <option value="Perlis">Perlis</option>
                                            <option value="Putrajaya">Putrajaya</option>
                                            <option value="Sabah">Sabah</option>
                                            <option value="Sarawak">Sarawak</option>
                                            <option value="Selangor">Selangor</option>
                                            <option value="Terengganu">Terengganu</option>                                            
                                        </select>
                                        @error('state')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="area">Area</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="text" id="area" class="form-control" name="area" placeholder="Area">
                                        @error('area')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="post_code">Postal Code</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="text" id="post_code" class="form-control" name="post_code" placeholder="Postal Code">
                                        @error('post_code')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="post_code">Label As</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control" id="basicSelect" name="label_as">
                                            <option value="">--Select--</option>
                                            <option value="Home">Home</option>
                                            <option value="Work">Work</option>                                            
                                        </select>
                                        @error('label_as')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-7 offset-sm-5">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck3" name="default_as">
                                        <label class="custom-control-label" for="customCheck3">Set as Default Address</label>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="col-sm-7 offset-sm-5">
                                <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary waves-effect">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
</section>

<!-- Basic Horizontal form layout section end -->

@endsection

@section('vendor-script')
  <!-- vendor files -->
    
@endsection
@section('page-script')

  <!-- Page js files -->

@endsection

