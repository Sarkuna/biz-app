@extends('layouts/fullLayoutMaster')

@section('title', 'Addresses List')
@section('vendor-style')
  <!-- vendor css files -->
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/app-invoice-list.css') }}" />
@endsection

@section('content')
<!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">My Addresses</h4>
            <a class="dt-button btn btn-primary btn-add-record ml-2" href="{{ route('shipping.create') }}"><span>Add New Address</span></a>
        </div>
    </div>
    
    <div class="row">
        @foreach ($models as $model)
        <div class="col-md-6 col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title" style="margin-bottom: 0;">{{$model->full_name }}</h4>
                    <h4 class="card-title">{{$model->phone }}</h4>
                    <div class="my-1 py-25">
                        @if($model->default_as=='Yes')
                            <span class="badge rounded-pill badge-light-success">Default</span>
                        @endif
                    </div>
                    <p class="card-text blog-content-truncate">
                        {{$model->address }}
                        {{$model->area }}
                        {{$model->post_code }}
                        {{$model->state }}
                    </p>
                    <hr>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="{{ route('shipping.edit', $model->id) }}" class="font-weight-bold" data-toggle="tooltip" data-placement="bottom" title="Edit">Edit</a>
                            @if($model->default_as=='Yes')
                            <button type="reset" class="btn btn-outline-secondary waves-effect" disabled="disabled">Set as default</button>
                            @else
                            <a href="{{route('shipping.delete',[$model->id])}}" class="font-weight-bold text-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="return confirm('Are you sure to delete this item?')">Delete</a>
                            <a href="{{route('shipping.default',[$model->id])}}" class="font-weight-bold" data-toggle="tooltip" data-placement="bottom" title="Set as default">Set as default</a>
                            @endif
                        </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
                <!-- Basic Horizontal form layout section end -->

@endsection

@section('vendor-script')
  <!-- vendor files -->
@endsection

@section('page-script')

  <!-- Page js files -->
  
@endsection

