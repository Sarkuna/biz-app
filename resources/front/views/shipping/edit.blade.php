@extends('layouts/fullLayoutMaster')

@section('title', 'Edit Address')
@section('vendor-style')
  <!-- vendor css files -->

@endsection

@section('page-style')
{{-- Page Css files --}}

@endsection

@section('content')
<!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
    <div class="row">
        <div class="col-md-7 col-12">
            <div class="card">

                <div class="card-body">
                    <form class="form form-horizontal" method="post" action="{{route('shipping.update',$model->id)}}">
                    @csrf 
                    @method('PATCH')
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="first-name">Full Name</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="text" id="first-name" class="form-control" name="full_name" placeholder="Full Name" value="{{ $model->full_name }}">
                                        @error('full_name')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="contact-info">Phone Number</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="number" id="phone" class="form-control" name="phone" placeholder="Phone Number" value="{{ $model->phone }}">
                                        @error('phone')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="address">Address</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <textarea class="form-control" placeholder="House Number, Building, Street Name" maxlength="128" name="address">{{ $model->address }}</textarea>
                                        @error('address')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="first-name">State</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control" id="basicSelect" name="state">
                                            <option value="">--Select--</option>
                                            <option value="Johor" {{ ($model->state) == 'Johor' ? 'selected' : '' }}>Johor</option>
                                            <option value="Kedah" {{ ($model->state) == 'Kedah' ? 'selected' : '' }}>Kedah</option>
                                            <option value="Kelantan" {{ ($model->state) == 'Kelantan' ? 'selected' : '' }}>Kelantan</option>
                                            <option value="Kuala Lumpur" {{ ($model->state) == 'Kuala Lumpur' ? 'selected' : '' }}>Kuala Lumpur</option>
                                            <option value="Labuan" {{ ($model->state) == 'Labuan' ? 'selected' : '' }}>Labuan</option>
                                            <option value="Melaka" {{ ($model->state) == 'Melaka' ? 'selected' : '' }}>Melaka</option>
                                            <option value="Negeri Sembilan" {{ ($model->state) == 'Negeri Sembilan' ? 'selected' : '' }}>Negeri Sembilan</option>
                                            <option value="Pahang" {{ ($model->state) == 'Pahang' ? 'selected' : '' }}>Pahang</option>
                                            <option value="Penang" {{ ($model->state) == 'Penang' ? 'selected' : '' }}>Penang</option>
                                            <option value="Perak" {{ ($model->state) == 'Perak' ? 'selected' : '' }}>Perak</option>
                                            <option value="Perlis" {{ ($model->state) == 'Perlis' ? 'selected' : '' }}>Perlis</option>
                                            <option value="Putrajaya" {{ ($model->state) == 'Putrajaya' ? 'selected' : '' }}>Putrajaya</option>
                                            <option value="Sabah" {{ ($model->state) == 'Sabah' ? 'selected' : '' }}>Sabah</option>
                                            <option value="Sarawak" {{ ($model->state) == 'Sarawak' ? 'selected' : '' }}>Sarawak</option>
                                            <option value="Selangor" {{ ($model->state) == 'Selangor' ? 'selected' : '' }}>Selangor</option>
                                            <option value="Terengganu" {{ ($model->state) == 'Terengganu' ? 'selected' : '' }}>Terengganu</option>                                            
                                        </select>
                                        @error('state')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="area">Area</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="text" id="area" class="form-control" name="area" placeholder="Area" value="{{ $model->area }}">
                                        @error('area')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="post_code">Postal Code</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="text" id="post_code" class="form-control" name="post_code" placeholder="Postal Code" value="{{ $model->post_code }}">
                                        @error('post_code')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-sm-5 col-form-label">
                                        <label for="post_code">Label As</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control" id="basicSelect" name="label_as">
                                            <option value="">--Select--</option>
                                            <option value="Home" {{ ($model->label_as) == 'Home' ? 'selected' : '' }}>Home</option>
                                            <option value="Work" {{ ($model->label_as) == 'Work' ? 'selected' : '' }}>Work</option>                                            
                                        </select>
                                        @error('label_as')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-7 offset-sm-5">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        @if($model->default_as=='No')
                                            <input type="checkbox" class="custom-control-input" id="customCheck3" name="default_as" {{  ($model->default_as == 'Yes' ? ' checked' : '') }}>
                                            <label class="custom-control-label" for="customCheck3">Set as Default Address</label>
                                        @elseif($model->default_as=='Yes')
                                        <input type="checkbox" class="custom-control-input" id="customCheck3" disabled="disabled" {{  ($model->default_as == 'Yes' ? ' checked' : '') }}>
                                            <label class="custom-control-label" for="customCheck3">Set as Default Address</label>    
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="col-sm-7 offset-sm-5">
                                <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary waves-effect">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
</section>

<!-- Basic Horizontal form layout section end -->

@endsection

@section('vendor-script')
  <!-- vendor files -->
    
@endsection
@section('page-script')

  <!-- Page js files -->

@endsection

