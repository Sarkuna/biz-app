@extends('layouts/fullLayoutMaster')

@section('title', 'Edit Profile')

@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}" />
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}" />
<style>
    .flatpickr-current-month .flatpickr-monthDropdown-months span, .flatpickr-current-month .numInputWrapper span {
    display: block;
}
</style>
@endsection

@section('content')
<div class="content-body">
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <!-- form -->
                            <form class="validate-form" method="POST" action="{{ route('user.my.profile.store') }}" novalidate="novalidate">    
                            {{csrf_field()}}     
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="account-full-name">Full Name</label>
                                            <div class="input-group form-password-toggle input-group-merge">
                                                <input type="text" class="form-control" id="account-full-name" name="full_name" placeholder="Your Full Name" value="{{ Auth::user()->full_name }}">                                                  
                                            </div>
                                            @error('full_name')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror 
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="account-mobile">Mobile</label>
                                            <div class="input-group form-password-toggle input-group-merge">
                                                <input type="text" class="form-control" id="account-mobile" name="mobile" placeholder="Your Mobile" value="{{ Auth::user()->mobile }}">                                                  
                                            </div>
                                            @error('mobile')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="gender">Gender</label>
                                            <select class="form-control" id="accountSelect" name="gender">
                                                <option value=""></option>
                                                <option value="male" {{ (Auth::user()->gender) == 'male' ? 'selected' : '' }} >Male</option>
                                                <option value="female" {{ (Auth::user()->gender) == 'female' ? 'selected' : '' }}>Female</option>
                                            </select>
                                            @error('gender')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="account-retype-new-password">Birth date</label>
                                            <input type="text" class="form-control flatpickr" placeholder="Birth date" id="account-birth-date" name="dob" value="{{ Auth::user()->dob }}"/>
                                            @error('dob')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary mr-1 mt-1 waves-effect waves-float waves-light">Save changes</button>
                                        <a href="{{ route('user.my.profile') }}" class="btn btn-outline-secondary mt-1 waves-effect">Cancel</a>
                                    </div>
                                </div>
                            </form>
                            <!--/ form -->
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('page-vendor')
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
@endsection

@section('page-script')
<script>
    $(function () {
        'use strict';
        
        // variables
        var flat_picker = $('.flatpickr');
        var maxBirthdayDate = new Date();
        maxBirthdayDate.setFullYear( maxBirthdayDate.getFullYear() - 18 );
        // flatpickr init
        if (flat_picker.length) {
            flat_picker.flatpickr({
                altInput: true,
                dateFormat: "Y-m-d",
                altFormat: "d-m-Y",
                maxDate: maxBirthdayDate,
                onReady: function (selectedDates, dateStr, instance) {
                  if (instance.isMobile) {
                    $(instance.mobileInput).attr('step', null);
                  }
                }
            });
        }
    });
</script>
@endsection