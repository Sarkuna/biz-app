@extends('layouts/AuthLayout')

@section('title', 'Register Page')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/animate/animate.min.css') }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/page-auth.css') }}">
  
@endsection


@section('content')
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <!-- Brand logo--><a class="brand-logo" href="javascript:void(0);">
            
            <h2 class="brand-text text-primary ml-1">{{ Helper::client_info()['name'] }}</h2>
        </a>
        <!-- /Brand logo-->
        <!-- Left Text-->
        <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                <img class="img-fluid" src="{{ asset('themes/vuexy-admin-v6/app-assets/images/pages/register-v2.svg') }}" alt="Register V2" /></div>
        </div>
        <!-- /Left Text-->
        <!-- Register-->
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <p class="text-center">
                    @if (!empty(Helper::client_info()['image']))
                      <!--<img src="/storage/bb_logo.png" alt="branding logo" class="rounded mr-75" alt="profile image" height="64" width="64">-->
                    <img src="{{ asset('/storage/images/'.Helper::client_info()['image'])}}" class="rounded" width="200">
                    @else
                    <img src="/storage/bb_logo.png" alt="branding logo" width="200">
                    @endif
                </p>
                <!--<h2 class="card-title font-weight-bold mb-1">Adventure starts here</h2>-->
                <form  class="auth-register-form mt-2" action="{{ route('user.registration.post') }}" method="POST">
                {{ csrf_field() }}   
                    <div class="form-group">
                        <label class="form-label" for="register-username">Full Name (as per IC)</label>
                        <input class="form-control" id="register-full-name" type="text" value="{{old('full_name')}}" name="full_name" placeholder="johndoe" aria-describedby="register-username" autofocus="" tabindex="1"/>
                        @error('full_name')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-mobile">Mobile No.</label>
                        <input class="form-control" id="register-mobile" type="text" value="{{old('mobile')}}" name="mobile" placeholder="60XX-XXXXXXX" aria-describedby="register-mobile" tabindex="2" />
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-email">Email</label>
                        <input class="form-control" id="register-email" type="text" value="{{old('email')}}" name="email" placeholder="john@example.com" aria-describedby="register-email" tabindex="3"/>
                        @error('email')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-member-name">Member Name</label>
                        <select class="select2 form-control" name="member_name" autofocus="" tabindex="4">
                            <option value="">--- Select Your Member</option>
                            
                            @foreach($members as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-password">Password</label>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input class="form-control form-control-merge" id="register-password" type="password" value="{{old('password')}}" name="password" placeholder="············" aria-describedby="register-password" tabindex="5"/>
                            <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                        </div>
                        @error('password')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-confirm-password">Confirm Password</label>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input class="form-control form-control-merge" id="register-confirm-password" type="password" value="{{old('confirm_password')}}" name="confirm_password" placeholder="············" aria-describedby="register-confirm-password" tabindex="5"/>
                            <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                        </div>
                        @error('confirm_password')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" id="register-privacy-policy" type="checkbox" id="validationCheck" name="terms" tabindex="4" />
                            <label class="custom-control-label" for="register-privacy-policy">I agree to<a href="javascript:void(0);" data-toggle="modal" data-target="#default">&nbsp;privacy policy & terms</a></label>
                        </div>
                        @error('terms')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <button class="btn btn-primary btn-block" tabindex="5">Sign up</button>
                </form>
                <p class="text-center mt-2"><span>Already have an account?</span><a href="{{ route('login') }}"><span>&nbsp;Sign in instead</span></a></p>

                
            </div>
        </div>
        <!-- /Register-->
    </div>
</div>

<!-- Basic trigger modal -->
<div class="basic-modal">
    <!-- Modal -->
    <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Terms</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Check First Paragraph</h5>
                    <p>
                        Oat cake ice cream candy chocolate cake chocolate cake cotton candy dragée apple pie. Brownie
                        carrot cake candy canes bonbon fruitcake topping halvah. Cake sweet roll cake cheesecake cookie
                        chocolate cake liquorice.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Accept</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic trigger modal end -->
@endsection
@section('vendor-script')
  <!-- vendor files -->
  
  <!--<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/polyfill.min.js') }}"></script>-->
@endsection
@section('page-script')
  <!-- Page js files -->
  
@endsection

