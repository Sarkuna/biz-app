@extends('layouts/AuthLayout')

@section('title', 'Reset Password')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/animate/animate.min.css') }}">
  <link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/extensions/toastr.min.css') }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/page-auth.css') }}">
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/extensions/ext-component-toastr.css')}}">    
@endsection

@section('content')

<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <!-- Brand logo--><a class="brand-logo" href="javascript:void(0);">

            <h2 class="brand-text text-primary ml-1">Biz App</h2>
        </a>
        <!-- /Brand logo-->
        <!-- Left Text-->
        <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                <img class="img-fluid" src="{{ asset('themes/vuexy-admin-v6/app-assets/images/pages/reset-password-v2.svg') }}" alt="Login V2" /></div>
        </div>
        <!-- /Left Text-->
        <!-- Login-->
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">

                <h2 class="card-title font-weight-bold mb-1">Forgot Password? 🔒</h2>
                <p class="card-text mb-2">Enter your email and we'll send you instructions to reset your password</p>

                <form class="auth-login-form mt-2" action="{{ route('user.reset.password.post') }}" method="POST">    
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <div class="d-flex justify-content-between">
                            <label for="reset-password-new">New Password</label>
                        </div>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input class="form-control form-control-merge @error('password') is-invalid @enderror" id="reset-password-new" type="password" name="password" value="{{ old('password') }}" placeholder="············" aria-describedby="reset-password-new" autofocus="" tabindex="1" />
                            <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                        </div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div id='password-strength-status'></div>
                        <span class="text-info">Your password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.</span>
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-between">
                            <label for="reset-password-confirm">Confirm Password</label>
                        </div>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input class="form-control form-control-merge" id="reset-password-confirm" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="············" aria-describedby="reset-password-confirm" tabindex="2" />
                            <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                        </div>
                        @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <button class="btn btn-primary btn-block" tabindex="3">Set New Password</button>
                </form>
                <p class="text-center mt-2"><a href="{{ route('login') }}"><i data-feather="chevron-left"></i> Back to login</a></p>




            </div>
        </div>
        <!-- /Login-->
    </div>
</div>
@endsection


@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
  <!--<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/polyfill.min.js') }}"></script>-->
@endsection
@section('page-script')
  <!-- Page js files -->
  @include('panels/messages')
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/extensions/ext-component-toastr.js') }}"></script>
  <script>
    $("#reset-password-new").keyup(function() {
        var number = /([0-9])/;
        var lowercase = /([a-z])/;
        var uppercase = /([A-Z])/;
        var special = /([!@#$%^&*])/;
        if ($('#reset-password-new').val().length < 8) {
            $('#password-strength-status').removeClass();
            $('#password-strength-status').addClass('alert alert-danger');
            $('#password-strength-status').html("Weak (should be atleast 8 characters.)");
        } else {
            if ($('#reset-password-new').val().match(number) && $('#reset-password-new').val().match(lowercase) && $('#reset-password-new').val().match(uppercase) && $('#reset-password-new').val().match(special)) {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-success');
                $('#password-strength-status').html("Strong");
            } else {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-warning');
                $('#password-strength-status').html("Medium (should include a Uppercase, numbers, special character and Lowercase.)</>");
            }
        }
    });
    </script>
@endsection