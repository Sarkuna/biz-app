@extends('layouts/AuthLayout')

@section('title', 'Forgot Password')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/animate/animate.min.css') }}">
  <link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/extensions/toastr.min.css') }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/page-auth.css') }}">
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/extensions/ext-component-toastr.css')}}">    
@endsection

@section('content')
<div class="auth-wrapper auth-v2">
                    <div class="auth-inner row m-0">
                        <!-- Brand logo--><a class="brand-logo" href="javascript:void(0);">
                            
                            <h2 class="brand-text text-primary ml-1">{{ Helper::client_info()['name'] }}</h2>
                        </a>
                        <!-- /Brand logo-->
                        <!-- Left Text-->
                        <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
                            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                                <img class="img-fluid" src="{{ asset('themes/vuexy-admin-v6/app-assets/images/pages/forgot-password-v2.svg') }}" alt="Login V2" /></div>
                        </div>
                        <!-- /Left Text-->
                        <!-- Login-->
                        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                                <p class="text-center">
                                  @if (!empty(Helper::client_info()['image']))
                                    <!--<img src="/storage/bb_logo.png" alt="branding logo" class="rounded mr-75" alt="profile image" height="64" width="64">-->
                                    <img src="{{ asset('/storage/images/'.Helper::client_info()['image'])}}" class="rounded" width="200">
                                  @else
                                    <img src="/storage/bb_logo.png" alt="branding logo" width="200">
                                  @endif
                                </p>
                                <h2 class="card-title font-weight-bold mb-1">Forgot Password? 🔒</h2>
                                <p class="card-text mb-2">Enter your email and we'll send you instructions to reset your password</p>

                                <form class="auth-login-form mt-2" action="{{ route('user.forget.password.post') }}" method="POST">    
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="form-label" for="forgot-password-email">Email</label>
                                        <input class="form-control @error('email') is-invalid @enderror" id="forgot-password-email" type="text" name="email" placeholder="john@example.com" aria-describedby="forgot-password-email" autofocus="" value="{{ old('email') }}" tabindex="1" />
                                        @error('email')
                                          <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                          </span>
                                        @enderror
                                    </div>
                                    <button class="btn btn-primary btn-block" tabindex="2">Send reset link</button>
                                </form>
                                <p class="text-center mt-2"><a href="{{ route('login') }}"><i data-feather="chevron-left"></i> Back to login</a></p>
                                

                                
                            </div>
                        </div>
                        <!-- /Login-->
                    </div>
                </div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
  <!--<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/polyfill.min.js') }}"></script>-->
@endsection
@section('page-script')
  <!-- Page js files -->
  @include('panels/messages')
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/extensions/ext-component-toastr.js') }}"></script>
@endsection