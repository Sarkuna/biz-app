@extends('layouts/AuthLayout')

@section('title', 'Login Page')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/animate/animate.min.css') }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/page-auth.css') }}">
  
@endsection

@section('content')
<div class="auth-wrapper auth-v2">
                    <div class="auth-inner row m-0">
                        <!-- Brand logo--><a class="brand-logo" href="javascript:void(0);">
                            
                            <h2 class="brand-text text-primary ml-1">{{ Helper::client_info()['name'] }}</h2>
                        </a>
                        <!-- /Brand logo-->
                        <!-- Left Text-->
                        <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
                            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                                <img class="img-fluid" src="{{ asset('themes/vuexy-admin-v6/app-assets/images/pages/login-v2.svg') }}" alt="Login V2" /></div>
                        </div>
                        <!-- /Left Text-->
                        <!-- Login-->
                        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                                <p class="text-center">
                                  @if (!empty(Helper::client_info()['image']))
                                    <!--<img src="/storage/bb_logo.png" alt="branding logo" class="rounded mr-75" alt="profile image" height="64" width="64">-->
                                    <img src="{{ asset('/storage/images/'.Helper::client_info()['image'])}}" class="rounded" width="200">
                                  @else
                                    <img src="/storage/bb_logo.png" alt="branding logo" width="200">
                                  @endif
                                </p>
                                <h2 class="card-title font-weight-bold mb-1">Welcome to Biz Portal!</h2>
                                <p class="card-text mb-2">Please sign-in to your account and start the adventure</p>

                                <form class="auth-login-form mt-2" action="{{ route('user.login.post') }}" method="POST">    
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="login-email" class="form-label">Email</label>
                                        <input type="text" class="form-control @error('email') is-invalid @enderror" id="login-email" name="email" placeholder="john@example.com" aria-describedby="login-email" tabindex="1" autofocus value="{{ old('email') }}" />
                                        @error('email')
                                          <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                          </span>
                                        @enderror
                                      </div>
                                    
                                    
                                    <div class="form-group">
                                        <div class="d-flex justify-content-between">
                                          <label for="login-password">Password</label>
                                          @if (Route::has('password.request'))
                                          <a href="{{ route('user.forget.password.get') }}">
                                            <small>Forgot Password?</small>
                                          </a>
                                          @endif
                                        </div>
                                        <div class="input-group input-group-merge form-password-toggle">
                                          <input type="password" class="form-control form-control-merge" id="login-password" name="password" tabindex="2" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="login-password" />
                                          <div class="input-group-append">
                                            <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                          </div>
                                        </div>
                                      </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                          <input class="custom-control-input" type="checkbox" id="remember-me" name="remember-me" tabindex="3" {{ old('remember-me') ? 'checked' : '' }} />
                                          <label class="custom-control-label" for="remember-me"> Remember Me </label>
                                        </div>
                                      </div>
                                    <button class="btn btn-primary btn-block" tabindex="4">Sign in</button>
                                </form>
                                <p class="text-center mt-2">
                                    <span>New on our platform?</span>
                                    @if (Route::has('register'))
                                    <a href="{{ route('user.registration') }}">
                                      <span>Create an account</span>
                                    </a>
                                    @endif
                                  </p>

                                
                            </div>
                        </div>
                        <!-- /Login-->
                    </div>
                </div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
  
  <!--<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/polyfill.min.js') }}"></script>-->
@endsection
@section('page-script')
  <!-- Page js files -->
  
@endsection