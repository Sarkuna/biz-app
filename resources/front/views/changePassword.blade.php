@extends('layouts/fullLayoutMaster')

@section('title', 'Change Password')

@section('vendor-style')
  <!-- vendor css files -->

@endsection

@section('page-style')
{{-- Page Css files --}}

@endsection

@section('content')
<div class="content-body">
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <!-- form -->
                            <form class="validate-form" method="POST" action="{{ route('user.change.password.store') }}" novalidate="novalidate">    
                            {{csrf_field()}}     
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="account-old-password">Old Password</label>
                                            <div class="input-group form-password-toggle input-group-merge">
                                                <input type="password" class="form-control" id="account-old-password" name="password" placeholder="Old Password">
                                                <div class="input-group-append">
                                                    <div class="input-group-text cursor-pointer">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                    </div>
                                                </div>                                                    
                                            </div>
                                            @error('password')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="account-new-password">New Password</label>
                                            <div class="input-group form-password-toggle input-group-merge">
                                                <input type="password" id="new_password" name="new_password" class="form-control" placeholder="New Password">
                                                <div class="input-group-append">
                                                    <div class="input-group-text cursor-pointer">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                            @error('new_password')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                            <div id="password-strength-status"></div>
                                            <p id="passwordHelpBlock" class="form-text text-muted">
                                                Your password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="account-retype-new-password">Retype New Password</label>
                                            <div class="input-group form-password-toggle input-group-merge">
                                                <input type="password" class="form-control" id="account-retype-new-password" name="confirm_password" placeholder="New Password">
                                                <div class="input-group-append">
                                                    <div class="input-group-text cursor-pointer"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></div>
                                                </div>
                                            </div>
                                            @error('confirm_password')
                                                <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary mr-1 mt-1 waves-effect waves-float waves-light">Save changes</button>
                                        <a href="{{ route('user.home') }}" class="btn btn-outline-secondary mt-1 waves-effect">Cancel</a>
                                    </div>
                                </div>
                            </form>
                            <!--/ form -->
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('vendor-script')
@endsection

@section('page-script')
    <script>
        $("#new_password").keyup(function() {
            var number = /([0-9])/;
            var lowercase = /([a-z])/;
            var uppercase = /([A-Z])/;
            var special = /([!@#$%^&*])/;
            if ($('#new_password').val().length < 8) {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-danger');
                $('#password-strength-status').html("Weak (should be atleast 8 characters.)");
            } else {
                if ($('#new_password').val().match(number) && $('#new_password').val().match(lowercase) && $('#new_password').val().match(uppercase) && $('#new_password').val().match(special)) {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('alert alert-success');
                    $('#password-strength-status').html("Strong");
                } else {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('alert alert-warning');
                    $('#password-strength-status').html("Medium (should include a Uppercase, numbers, special character and Lowercase.)</>");
                }
            }
        });
    </script>
@endsection