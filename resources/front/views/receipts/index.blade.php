@extends('layouts/fullLayoutMaster')

@section('title', 'Receipt List')
@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/tables/datatable/datatables.min.css') }}" />  
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}" />  
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/tables/datatable/responsive.bootstrap.min.css') }}" />  
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/app-invoice-list.css') }}" />
@endsection

@section('content')
<!-- Basic Horizontal form layout section start -->
<section class="invoice-list-wrapper">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">All Receipts</h4>
        </div>
        <div class="card-datatable table-responsive">
            @if(count($models)>0)
            <table class="invoice-list-table table" id="receipts-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Order Num</th>
                        <th>Receipts No</th>                                
                        <th>Date of Receipt</th>
                        <th>Amount RM</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($models as $model)
                    <tr id="client_id_{{ $model->id }}">
                        <td>{{ $loop->index+1 }}</td>
                        <td><a class="fw-bold" href="{{ route('receipts.show', $model->id) }}"> {{ $model->receipt_order }}</a></td>
                        <td>{{ $model->receipt_no }}</td>
                        <td class="text-center">
                            @if(!empty($model->date_of_receipt))
                            {{ $model->date_of_receipt->format('d-m-Y') }}
                            @else
                            N/A
                            @endif
                        </td>
                        <td class="text-right">{{$model->receipt_total_amount}}</td>
                        <td class="text-center">
                            @if(!empty($model->created_at))
                            {{ $model->created_at->format('d-m-Y') }}
                            @else
                            N/A
                            @endif
                        </td>

                        <td>
                            @if($model->status=='approved')
                            <span class="badge rounded-pill badge-light-success">{{ucwords($model->status)}}</span>
                            @elseif($model->status=='declined')
                            <span class="badge rounded-pill badge-light-danger">{{ucwords($model->status)}}</span>
                            @else
                            <span class="badge rounded-pill badge-light-warning">{{ucwords($model->status)}}</span>
                            @endif
                        </td>
                        <td>
                            <a class="me-1" href="{{ route('user.member.receipt.view', $model->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i data-feather="eye" class="mx-1 feather feather-eye"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <h6 class="text-center">No items found!!! Please create</h6>
            @endif
        </div>
    </div>
</section>

                <!-- Basic Horizontal form layout section end -->

@endsection

@section('vendor-script')
  <!-- vendor files -->
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/extensions/moment.min.js') }}"></script>
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/tables/datatable/responsive.bootstrap.min.js') }}"></script>
@endsection

@section('page-script')

  <!-- Page js files -->
  <script type="text/javascript">
$(document).ready(function() {
    $('#receipts-list').DataTable({
        order: [[1, 'desc']],
      dom:
        '<"row d-flex justify-content-between align-items-center m-1"' +
        '<"col-lg-6 d-flex align-items-center"l<"dt-action-buttons text-xl-right text-lg-left text-lg-right text-left "B>>' +
        '<"col-lg-6 d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap pr-lg-1 p-0"f<"invoice_status ml-sm-2">>' +
        '>t' +
        '<"d-flex justify-content-between mx-2 row"' +
        '<"col-sm-12 col-md-6"i>' +
        '<"col-sm-12 col-md-6"p>' +
        '>',
      language: {
        sLengthMenu: 'Show _MENU_',
        search: 'Search',
        searchPlaceholder: 'Search',
        paginate: {
          // remove previous & next text from pagination
          previous: '&nbsp;',
          next: '&nbsp;'
        }
      },
      // Buttons with Dropdown
      buttons: [
        /*{
          text: 'Add Record',
          className: 'btn btn-primary btn-add-record ml-2',
          action: function (e, dt, button, config) {
            window.location = invoiceAdd;
          }
        }*/
      ],
      // For responsive popup
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Details of ' + data['client_name'];
            }
          }),
          type: 'column',
          renderer: $.fn.dataTable.Responsive.renderer.tableAll({
            tableClass: 'table',
            columnDefs: [
              {
                targets: 2,
                visible: false
              },
              {
                targets: 3,
                visible: false
              }
            ]
          })
        }
      },
    });
    
} );
$(document).find('[data-bs-toggle="tooltip"]').tooltip();
</script>
@endsection

