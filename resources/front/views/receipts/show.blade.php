@extends('layouts/fullLayoutMaster')

@section('title', 'Receipt List')
@section('vendor-style')
  <!-- vendor css files -->
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/pages/app-invoice.css') }}" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
@endsection

@section('content')
<!-- Basic Horizontal form layout section start -->
<section class="invoice-preview-wrapper">
    <div class="row invoice-preview">
        <!-- Invoice -->
        <div class="col-xl-12 col-md-12 col-12">
            <div class="card invoice-preview-card">
                <div class="card-body invoice-padding pb-0">
                    <!-- Header starts -->
                    <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                        <div>
                            <div class="logo-wrapper">
                                <img class="img-fluid rounded" src="{{ asset('/storage/member_logo/'.$model->member_info->client_id.'/'.$model->member_info->photo)}}" height="104" width="104" alt="User avatar" />
                                <h3 class="text-primary invoice-logo">{{ $model->member_info->name }}</h3>
                            </div>
                            <p class="card-text mb-25">{{$model->member_info->address1}} {{$model->member_info->address2}}</p>
                            <p class="card-text mb-25">{{$model->member_info->city}} {{$model->member_info->post_code}} {{$model->member_info->state}}</p>
                            <p class="card-text mb-0">{{ucwords($model->member_info->country)}}</p>
                        </div>
                        <div class="mt-md-0 mt-2">
                            <h4 class="invoice-title">
                                @if($model->status=='approved')
                                <span class="badge rounded-pill badge-light-success">{{ucwords($model->status)}}</span>
                                @elseif($model->status=='declined')
                                <span class="badge rounded-pill badge-light-danger">{{ucwords($model->status)}}</span>
                                @else
                                <span class="badge rounded-pill badge-light-warning">{{ucwords($model->status)}}</span>
                                @endif
                            </h4>
                            <div class="invoice-date-wrapper">
                                <p class="invoice-date-title">Invoice #:</p>
                                <p class="invoice-date">{{ $model->receipt_order }}</p>
                            </div>
                            <div class="invoice-date-wrapper">
                                <p class="invoice-date-title">Create Date:</p>
                                <p class="invoice-date">
                                    @if(!empty($model->created_at))
                                    {{ $model->created_at->format('d-m-Y') }}
                                    @else
                                    N/A
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Header ends -->
                </div>

                <hr class="invoice-spacing" />

                <!-- Address and Contact starts -->
                <div class="card-body invoice-padding pt-0">
                    <div class="row invoice-spacing">
                        <div class="col-xl-5 p-0">
                            <h6 class="mb-2">Receipt Details:</h6>
                            <table>
                                <tbody>
                                    
                                    <tr>
                                        <td class="pr-1">Receipts No:</td>
                                        <td>{{ $model->receipt_no }}</td>
                                    </tr>
                                    <tr>
                                        <td class="pr-1">Date of Receipt:</td>
                                        <td>
                                            @if(!empty($model->date_of_receipt))
                                            {{ $model->date_of_receipt->format('d-m-Y') }}
                                            @else
                                            N/A
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="pr-1">Amount:</td>
                                        <td>
                                            <span class="font-weight-bold">
                                            @convert($model->receipt_total_amount)
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xl-7 p-0 mt-xl-0 mt-2">
                            <h6 class="mb-2">Proof of Receipts:</h6>
                            @if (count($portfolios) > 0)
                            <div class="row tz-gallery">
                                @foreach ($portfolios as $key => $portfolio)
                                <figure class="col-lg-3 col-md-6 col-12">
                                    <a href="{{ asset('/storage/receipts/'.$model->client_id.'/'.$model->ref.'/'.$key)}}" itemprop="contentUrl" data-size="480x360" class="lightbox">
                                        <img class="img-fluid rounded" src="{{ asset('/storage/receipts/'.$model->client_id.'/'.$model->ref.'/'.$key)}}" itemprop="thumbnail" alt="Image description" />
                                    </a>
                                </figure>
                                @endforeach
                            </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
                <!-- Address and Contact ends -->

            </div>
        </div>
        <!-- /Invoice -->

    </div>
</section>
@endsection

@section('vendor-script')
  <!-- vendor files -->
@endsection

@section('page-script')
    <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/extensions/toastr.js') }}"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
  <!-- Page js files -->
  <script type="text/javascript">
$(document).ready(function() {
    $('#receipts-list').DataTable();
} );
baguetteBox.run('.tz-gallery');
</script>
  
@endsection

