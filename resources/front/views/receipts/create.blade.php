@extends('layouts/fullLayoutMaster')

@section('title', 'Upload Receipt')
@section('vendor-style')
  <!-- vendor css files -->
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}" />  
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}" />  
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/css/forms/select/select2.min.css') }}" />
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/pickers/form-pickadate.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('themes/vuexy-admin-v6/app-assets/css/plugins/forms/form-file-uploader.css') }}" />
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<!-- Basic Horizontal form layout section start -->
<section id="basic-vertical-layouts">
    <div class="row">
        <div class="col-md-9 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Upload Receipt</h4>
                </div>
                <div class="card-body">
                    <form class="form form-vertical" method="post" action="{{route('receipts.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="member-category-vertical">Member Category</label>
                                    <select class="select2 form-control" name="member_category">
                                        <option value="">Select Member Category</option>
                                        @foreach($categories as $key=>$category)
                                            <option value='{{$category->id}}'>{{$category->category->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('member_category')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="company-name-vertical">Member Company Name</label>
                                    <select class="select2 form-control" name="company_name">
                                        <option value="">Select Company Name</option>
                                        @foreach($members as $key=>$member)
                                            <option value='{{$member->id}}' @if(!empty($_GET['name']) && $_GET['name']==$member->id) selected @endif>{{$member->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('company_name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="receipt-no-vertical">Receipt No</label>
                                    <input type="text" id="contact-info-vertical" class="form-control" name="receipt_no" placeholder="ER586952" />
                                    @error('receipt_no')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="contact-info-vertical">Date of Receipt</label>
                                    <input type="text" id="fp-default" class="form-control flatpickr-basic" name="receipt_date" placeholder="YYYY-MM-DD" />
                                    @error('receipt_date')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="contact-info-vertical">Receipt Total Amount</label>
                                    <input type="number" id="contact-info-vertical" class="form-control" name="receipt_amount" placeholder="0.00" />
                                    @error('receipt_amount')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="password-vertical">Attach Receipt</label>
                                    <input id="input-707" name="files[]" type="file" accept="image/*" multiple>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        

    </div>
</section>
                <!-- Basic Horizontal form layout section end -->

@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>    
    <script src="{{ asset('themes/vuexy-admin-v6/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/forms/pickers/form-pickers.js') }}"></script>
  <script src="{{ asset('themes/vuexy-admin-v6/app-assets/js/scripts/forms/form-select2.js') }}"></script>
  
  
  <script type="text/javascript">
    $(document).ready(function () {
        var $el3 = $("#input-707");
        $el3.fileinput({
            theme: 'fa',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            uploadUrl: "/user/receipts/create",
            //showUpload: false, // hide upload button
            shoaCancel: false,
            overwriteInitial: true, // append files to initial preview
            maxFileCount: 4,
            maxFileSize: 2000,
            browseOnZoneClick: true,
            initialPreviewAsData: true,
        });
    });
  </script>
@endsection

