@extends('layouts/fullLayoutMaster')
@section('title', 'Home')
@section('page-style')
<style> .content-header {display: none;} </style>
@endsection
@section('content')
<section id="dashboard-ecommerce">
    <div class="row match-height">
        <!-- Medal Card -->
        <div class="col-xl-5 col-md-6 col-12">
            <div class="card card-transaction">
                <div class="card-header">
                    <h4 class="card-title">{{ Auth::user()->full_name }}</h4>
                </div>
                <div class="card-body">
                    <div class="transaction-item">
                        <div class="media">

                            <div class="media-body">
                                <h6 class="transaction-title">Code</h6>

                            </div>
                        </div>
                        <div class="font-weight-bolder">{{ Auth::user()->customer_code }}</div>
                    </div>
                    
                    <div class="transaction-item">
                        <div class="media">

                            <div class="media-body">
                                <h6 class="transaction-title">Member Name</h6>

                            </div>
                        </div>
                        <div class="font-weight-bolder">
                            @if(Auth::user()->member_id > 0) 
                                {{ Auth::user()->member_info->name }}
                            @else
                             N/A
                            @endif
                        </div>
                    </div>
                    <div class="transaction-item">
                        <div class="media">

                            <div class="media-body">
                                <h6 class="transaction-title">Status</h6>

                            </div>
                        </div>
                        <div class="font-weight-bolder">
                            test{{Auth::user('api')->token()}}
                            @if(Auth::user()->status=='active')
                            <span class="badge badge-success">{{ucwords(Auth::user()->status)}}</span>
                            @elseif(Auth::user()->status=='declined')
                            <span class="badge badge-danger">{{ucwords(Auth::user()->status)}}</span>
                            @else
                            <span class="badge badge-warning">{{ucwords(Auth::user()->status)}}</span>
                            @endif
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!--/ Medal Card -->
        
        <div class="col-xl-7 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="mb-0">My Points</h4>                    
                </div>
                <div class="card-content">
                    <div class="card-body px-0 pb-0" style="padding: 0px;">
                        <div id="goal-overview-chart" class="mt-75"></div>
                        <div class="row text-center mx-0">
                            <div class="col-6 border-top border-right d-flex align-items-between flex-column py-1">
                                <p class="mb-50">Available Balance</p>
                                <p class="font-large-1 text-bold-700">@convertpoint($userpoint['user_points_balance'])</p>
                            </div>
                            <div class="col-6 border-top d-flex align-items-between flex-column py-1">
                                <p class="mb-50">Expiring on 31-12-2021</p>
                                <p class="font-large-1 text-bold-700">0</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    
    <div class="row match-height">
    <div class="col-12 mb-2">
        <div id="carousel-interval" class="carousel slide" data-ride="carousel" data-interval="5000">
            <ol class="carousel-indicators">
                <li data-target="#carousel-interval" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-interval" data-slide-to="1"></li>
                <li data-target="#carousel-interval" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img class="img-fluid" src="{{ asset('themes/vuexy-admin-v6/app-assets/images/banner/banner-1.jpg') }}" alt="First slide" />
                </div>
                <div class="carousel-item">
                    <img class="img-fluid" src="{{ asset('themes/vuexy-admin-v6/app-assets/images/banner/banner-2.jpg') }}" alt="Second slide" />
                </div>
                <div class="carousel-item">
                    <img class="img-fluid" src="{{ asset('themes/vuexy-admin-v6/app-assets/images/banner/banner-3.jpg') }}" alt="Third slide" />
                </div>
            </div>
            <a class="carousel-control-prev" href="#carousel-interval" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-interval" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    </div>
    <div class="row match-height">
        <div class="col-lg-12 col-12">
            <div class="row match-height">                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="{{ route('user.member.list') }}">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="users"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Member List</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="{{ route('receipts.create') }}">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="file-text"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Upload Receipt</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="/rewards">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="gift"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Reward Gallery</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="/rewards">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="thumbs-up"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Refer A Friend</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row match-height">
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="/rewards">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="calendar"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Events</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="/rewards">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="archive"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Latest Promotion</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="/rewards">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="file-text"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Receipt History</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="/rewards">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="list"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Referral History</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row match-height">
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="/rewards">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="list"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Points History</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="/rewards">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="info"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Customer Profile</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="/rewards">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="briefcase"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Terms & Condition</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-md-3 col-sm-6">
                    <div class="card text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="p-50 m-0 mb-1">
                                    <a href="/rewards">
                                        <div class="avatar-content">
                                            <i class="feather-3" data-feather="trello"></i>
                                        </div>
                                    </a>
                                </div>
                                <h4 class="text-bold-700">Latest Info</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    
</section>
@endsection 

