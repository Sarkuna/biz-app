<?php

namespace App\Http\View\Composers;

//use App\Repositories\UserRepository;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

use App\Models\Receipts;

use App\Helpers\Helper;

class ProfileComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        //dd('here1');
        //$orderToken = session('o_token');
        //$this->cartDetails = Order::getOrderDetails($orderToken);
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Auth::guard('member')->check()) {
            $client_id = Helper::my_domain();
            $member_id = Helper::member_profile();

            $receipt_pending = Receipts::where(['client_id' => $client_id, 'member_id' => $member_id, 'status' => 'pending'])->count();
            $receipt_approve = Receipts::where(['client_id' => $client_id, 'member_id' => $member_id, 'status' => 'approved'])->count();
            $receipt_decline = Receipts::where(['client_id' => $client_id, 'member_id' => $member_id, 'status' => 'declined'])->count();

            $view->with('count', [
                'receipt_pending' => $receipt_pending,
                'receipt_approve' => $receipt_approve,
                'receipt_decline' => $receipt_decline,            
            ]);
        }
    }
}