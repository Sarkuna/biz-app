<?php

namespace App\Http\View\Composers;

//use App\Repositories\UserRepository;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

///use App\Models\MemberPoints;
use App\Models\UserPoints;

use App\Helpers\Helper;

class UserComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        //dd('here1');
        //$orderToken = session('o_token');
        //$this->cartDetails = Order::getOrderDetails($orderToken);
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $client_id = Helper::my_domain();
        //$user_profiles_id = Helper::user_profile();
        $user_id = Auth::id();
        
        $user_points_balance = UserPoints::where(['client_id' => $client_id, 'user_id' => $user_id])->sum('num_points');
        $goingtoexpiry = 0;
        $view->with('userpoint', [
            'user_points_balance' => $user_points_balance
            //'receipt_decline' => $receipt_decline,            
        ]);
    }
}