<?php

namespace App\Http\View\Composers;

//use App\Repositories\UserRepository;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

use App\Models\MemberPoints;
//use App\Models\UserPoints;

use App\Helpers\Helper;

class MemberComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        //dd('here1');
        //$orderToken = session('o_token');
        //$this->cartDetails = Order::getOrderDetails($orderToken);
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $client_id = Helper::my_domain();
        $member_profiles_id = Helper::member_profile();
        
        $points_balance = MemberPoints::where(['client_id' => $client_id, 'member_profiles_id' => $member_profiles_id])->sum('num_points');
        $goingtoexpiry = 0;
        $view->with('point', [
            'points_balance' => $points_balance,
            'going_to_expiry' => $goingtoexpiry,          
        ]);
    }
}