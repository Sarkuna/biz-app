<?php

namespace App\Http\View\Composers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //dd('123dfgdgfdg');
        // Using class based composers...
        View::composer(
            '*', 'App\Http\View\Composers\ProfileComposer'
        );
        
        View::composer(
            '*', 'App\Http\View\Composers\MemberComposer'
        );
        
        View::composer(
            '*', 'App\Http\View\Composers\UserComposer'
        );
    }
}