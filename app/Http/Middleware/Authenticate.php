<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        if ($request->is('admin') || $request->is('admin/*')) {
            return route('admin.login');
        }

        if ($request->is('member') || $request->is('member/*')) {
            return route('member.login');
            //return redirect()->intended('/login');
        }

        if (! $request->expectsJson()) {
            return route('login');
        }
        /*dd($request);
        die;
        if (Auth::guard('admin')) {
            if (!$request->expectsJson()) {
                return route('admin.loginasdsadsada');
            }
        }else if (Auth::guard('client')) {
            if (!$request->expectsJson()) {
                return route('client.loginasdsadsada');
            }
        }  else {
            if (!$request->expectsJson()) {
                return route('login122');
            }
        }*/
    }
}
