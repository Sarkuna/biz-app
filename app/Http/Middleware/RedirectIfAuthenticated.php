<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use Request;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(Auth::guard('admin')->check()){
            return redirect()->route('admin.dashboard');
            //return redirect(RouteServiceProvider::ADMIN_DASHBOARD);
        }
        
        if(Auth::guard('member')->check()){
            //return redirect('/member/dashboard');
            return redirect()->route('member.dashboard');
            //return redirect(RouteServiceProvider::ADMIN_DASHBOARD);
        }

        if (Auth::guard($guard)->check()) {
            //return redirect(RouteServiceProvider::HOME);
            return redirect()->route('user.home');
        }
        
        return $next($request);
    }
}
