<?php

namespace App\Http\Middleware;

use Closure;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        dd($request->user());
        die;
        if($request->user()->user_type=='admin'){
            return $next($request);
        }
        else{
            request()->session()->flash('error','You do not have any permission to access this page');
            //return redirect()->route($request->user()->role);
        }
    }
}
