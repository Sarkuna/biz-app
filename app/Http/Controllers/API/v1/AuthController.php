<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use Carbon\Carbon;
use App\User;
use App\Models\UserPoints;
use App\Helpers\Helper;
use App\Rules\MatchOldPassword;
use App\Rules\IsValidPassword;

use DB;
use Mail; 
use Hash;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] full_name
     * @param  [string] email
     * @param  [string] mobile
     * @param  [string] password  
     * @param  [string] confirm_password 
     * @param  [boolen] terms
     * @response status=404 scenario="user not found" {
     *  "message": "Account successfully created"
     * }
     */
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|max:50',
            'email' => 'required|max:100|email|unique:users',
            'password' => ['required', 'string', new isValidPassword()],
            'confirm_password' => ['same:password'],
            'terms' => 'required'
        ]);
        
        if ($validator->fails()) {
            $message = $validator->errors()->first();
            return response()->json(['statusCode' => 500, 'success' => false, 'message' => $message], 500);
        }
        
        $user = new User();
        $user->full_name = $request->full_name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->password = Hash::make($request->confirm_password);
        //$user->member_id = $request->member_name;
        $user->client_id = 18;
        if($user->save()) {
            $return = 'Account successfully created';
            return response()->json(['statusCode' => 200, 'success' => true, 'message' => $return], 200);
        }else {
            $return = 'DB Error';
            return response()->json(['statusCode' => 500, 'success' => false, 'message' => $message], 500);
        }
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized / Access Token Expired'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
    
    /**
    * @response scenario=success {
    *   "code": "C18082100001",
    *   "name": "Busines Booster Sdn Bhd",
    *   "user_points_balance": "1010",
    *   "expiring_on": 0,
    *   "status": "active"
    * }
    * @response status=404 scenario="user not found" {
    *  "message": "User not found"
    * } 
    */
    public function Dashboard(Request $request)
    {
        //$client_id = Helper::my_domain();
        //$user_profiles_id = Helper::user_profile();
        $user_id = $request->user()->id;
        $client_id  = $request->user()->client_id;
        $code  = $request->user()->customer_code;
        $name  = $request->user()->full_name;
        if(!empty($request->user()->member_id)) {
           $member_name = $request->user()->member_info->name; 
        }else {
            $member_name = 'N/A';
        }
        
        $status = $request->user()->status;
        $token_id = $request->user()->token();
        
        $user_points_balance = UserPoints::where(['client_id' => $client_id, 'user_id' => $user_id])->sum('num_points');
        //$goingtoexpiry = 0;
        
        return response()->json([
            'code' => $code,
            'name' => $name,
            'member_name' => $member_name,
            'user_points_balance' => $user_points_balance,
            'expiring_on' => 0,
            'status' => $status,
            'token_id' => $token_id->id
        ]);
    }
    
    /**
     * Forgot Password
     *
     * @param  [string] email
     * @response status=404 scenario="user not found" {
     *  "message": "We have e-mailed your password reset link!"
     * }
     */
    public function ForgotPassword(Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->first();
            return response()->json(['statusCode' => 500, 'success' => false, 'message' => $message], 500);
        }

        $token = Str::random(64);

        $reset_token = $request->getSchemeAndHttpHost() . '/reset-password/' . $token;

        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        /* Mail::send('email.forgetPassword', ['token' => $token], function($message) use($request){
          $message->to($request->email);
          $message->subject('Reset Password');
          }); */

        $data = ['email' => $request->email, 'reset_token' => $reset_token];
        $subject = '';
        $return = Helper::sendEmailUser($request->email, $subject, 'forgot_password', $data);

        if ($return > 0) {
            $return = 'We have e-mailed your password reset link!';
        } else {
            $return = 'Please try again!!';
        }

        return response()->json(['statusCode' => 200, 'success' => true, 'message' => $return], 200);
    }
    
    /**
     * Change Password
     *
     * @param  [string] password
     * @param  [string] new_password
     * @param  [string] confirm_password  
     * @response status=404 scenario="user not found" {
     *  "message": "Password successfully changed"
     * }
     */
    public function changPassword(Request $request)
    {
        //return 'test post here';
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'password' => ['required', new MatchOldPassword],
            'new_password' => ['required', 'string', new isValidPassword()],
            'confirm_password' => ['same:new_password'],
        ]);
        
        if ($validator->fails()) {
            $message = $validator->errors()->first();
            return response()->json(['statusCode' => 500, 'success' => false, 'message' => $message], 500);
        }

        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->confirm_password)]);
   
        $return = 'Password successfully changed';
        return response()->json(['statusCode' => 200, 'success' => true, 'message' => $return], 200);
    }
    
    /**
     * Check Externel Login
     *
     * @response status=404 scenario="user not found" {
     *  "message": "0"
     * }
     */
    public function checkExternallogin(Request $request)
    {
        $user_id = $request->user()->id;
        $models = \App\Models\AppPageLogins::where([['user_id', '=', $user_id]])->count();

        if ($models > 0) {
            $return = 1;
            return response()->json(['statusCode' => 200, 'success' => true, 'message' => $return], 200);
        }else {
            $return = 0;
            return response()->json(['statusCode' => 200, 'success' => true, 'message' => $return], 200);
        }
    }
}
