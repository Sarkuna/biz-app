<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        //$this->middleware('guest:admin');
        $this->middleware('guest')->except('logout');
    }
    
    public function index()
    {
        //return view('home');
      return view('auth.login'); 
    }
    
    public function registration()
    {
        return view('auth.register');
    }
    
    public function postLogin(Request $request)
    {
        /*request()->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
 
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
        return Redirect::to("/admin/login")->withError('Oppes! You have entered invalid credentials');*/
        
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            //'email' => 'required|string|email|max:255',
            //'password' => 'required|string|min:6|confirmed',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('home');
        }

        return redirect('admin/login')->with('error', 'Oppes! You have entered invalid credentials');
    }
    
    public function postRegistration(Request $request)
    {  
        request()->validate([
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        ]);
         
        $data = $request->all();
 
        $check = $this->create($data);
       
        return Redirect::to("dashboard")->withSuccess('Great! You have Successfully loggedin');
    }
     
    public function dashboard()
    {
      return view('home');
    }
 
    public function create(array $data)
    {
      return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
    }
     
    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('login');
    }
    
}
