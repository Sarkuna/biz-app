<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Admin;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ADMIN_DASHBOARD;
    protected $guard = 'admin';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    protected function guard()
    {
        return Auth::guard('admin');
    }
    
    public function index()
    {
        
        //return view('home');
      return view('auth.login'); 
    }
    /**
     * show login form for admin guard
     *
     * @return void
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }


    /**
     * login admin
     *
     * @param Request $request
     * @return void
     */
    public function postLogin(Request $request)
    {
        //dd($request->all());die;
        // Validate Login Data
        $request->validate([
            'email' => 'required|max:50',
            'password' => 'required',
        ]);
	$credentials = $request->only('email', 'password');
        
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password, 'user_type' => 'admin'], $request->remember)) {
            session()->flash('success', 'Successully Logged in !');
            return redirect()->intended('/admin/dashboard');
            //return redirect('/admin');
        }
        // error
        session()->flash('error', 'Invalid email and password');
        return back();
       
    }

    /**
     * logout admin guard
     *
     * @return void
     */
    /*public function logout()
    {
        Auth::guard('admin')->logout();
        //return redirect()->route('admin.login');
        return redirect('/admin');
    }*/
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/admin');
    }
}
