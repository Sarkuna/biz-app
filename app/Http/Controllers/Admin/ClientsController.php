<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;



//Models
use App\Models\Client;
//use App\Models\ClientCategories;
//use App\Models\MemberCategories;
//use App\Models\Admin;
use Helper;
use DB;

class ClientsController extends Controller
{
    private $photos_path;

    public function __construct()
    {
        //$this->photos_path = public_path('/images');
        $this->photos_path = public_path('/storage/images/');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
        //superadmin
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;        
        $client = Client::findOrFail($id);
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('admin.roles.index'), 'name' => "All Roles"], ['name' => "Create Role"]];
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => $client->name]];
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');*/
        
        return view('company.index', compact('breadcrumbs','client'));
    }
    
    public function companylogo()
    {
        //dd($path = public_path('/storage/images/'));
        $id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;        
        $client = Client::findOrFail($id);
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('admin.roles.index'), 'name' => "All Roles"], ['name' => "Create Role"]];
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => $client->name]];
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('admin.company.index'), 'name' => "Company"], ['name' => $client->name]];
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');*/
        
        return view('company.logo', compact('breadcrumbs','client'));
    }
    
    public function fileStore(Request $request)
    {
        $id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;
        $uploaded_image = Client::find($id);

        if (!empty($uploaded_image->image)) {
            $file_path = $this->photos_path.$uploaded_image->image;
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            $uploaded_image->image = '';
            $uploaded_image->save();
        }
        
        $image = $request->file('file');
        //$imageName = $image->getClientOriginalName();
        $name = date('YmdHis');
        $save_name = $name . '.' . $image->getClientOriginalExtension();
        
        
        $image->move($this->photos_path,$save_name);
        
        $imageUpload = Client::find($id);
        if($imageUpload) {
            $imageUpload->image = $save_name;
            $imageUpload->save();
        }
        //return response()->json(['success'=>$save_name]);
        //return response()->json(['url'=>url('/admin/company')]);
    }
    
    public function destroy(Request $request)
    {
        //dd($request);
        $filename = $request->id;
        $id = $request->clientid;

        $uploaded_image = Client::find($id);

        if (empty($uploaded_image)) {
            return Response::json(['message' => 'Sorry file does not exist'], 400);
        }
 
        $file_path = $this->photos_path.$filename;
        //$resized_file = $this->photos_path . '/' . $uploaded_image->resized_name;
 
        if (file_exists($file_path)) {
            unlink($file_path);
        }

        if (!empty($uploaded_image)) {
            $uploaded_image->image = '';
            $uploaded_image->save();
        }
 
        return Response::json(['message' => 'File successfully delete'], 200);
    }
    
    public function viewimage()
    {
        //dd($request);
        $id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;
        //$id = 1;
        $uploaded_image = Client::findOrFail($id);
        $file = $uploaded_image->image;
        if(!empty($file)) {
            $file_path = $this->photos_path.$file;
            //$resized_file = $this->photos_path . '/' . $uploaded_image->resized_name;
            if (file_exists($file_path)) {
                $size = filesize($file_path);
                $return = array('name'=>$file,'size'=>$size,'path'=> $file);
            }
        }else {
            $return = false;
        }
        return Response::json($return);
        //return json_encode($file_list);
        //return Response::json(array(
        
        //echo json_encode($file_list);
        //exit;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models  $models
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$membercategories = MemberCategories::where('status',"approved")->get();
        $clients = Client::findOrFail($id);
        //$clientcategorie =  ClientCategories::where('client_id',$id)->get();
        //$clientcategories = $clientcategorie->pluck('member_cat_id');

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('admin.company.index'), 'name' => "Company"], ['name' => "Update"]];
        return view('company.edit',compact('breadcrumbs','clients'));
        //return view('client.edit', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories)->with('clients',$clients)->with('clientcategories',$clientcategories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models  $models
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post=Client::findOrFail($id);
        $this->validate($request,[
            'name'=>'required|max:200',
            'address'=>'string|required',
            'city'=>'string|required',
            'state'=>'string|required',
            'post_code'=>'string|required',
            'country'=>'string|required'
        ]);

        $data=$request->all();

        $status=$post->fill($data)->save();
        if($status){
            //request()->session()->flash('success','Post Successfully added');
            $msg = 'Company Profile Successfully update';
        }
        else{
            //request()->session()->flash('error','Please try again!!');
            $msg = 'Please try again!!';
        }
        return redirect()->route('admin.company.index')->with('success', $msg);
    }

}
