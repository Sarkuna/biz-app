<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;



//Models
use App\Models\MemberProfiles;
use App\Models\ClientCategories;
use App\Models\AssignCategories;
//use App\Models\ClientCategories;
//use App\Models\MemberCategories;
use App\Models\Admin;
use Helper;
use DB;

class MemberProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;

        $models = MemberProfiles::where('client_id', '=', $id)->get();

        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('admin.roles.index'), 'name' => "All Roles"], ['name' => "Create Role"]];
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => 'Mebers List']];
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');*/
        
        return view('member-profiles.index', compact('breadcrumbs','models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;
        $membercategories=ClientCategories::where('deleted_at', '=', null)->where('client_id', '=', $id)->get();

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('members.index'), 'name' => "Members List"], ['name' => "Create"]];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('member-profiles.create',compact('breadcrumbs','membercategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required|max:100',
            //'email' => 'required|max:100|email',
            'email' => 'required|max:100|email|unique:admins,email,NULL,id,user_type,members',
            'mobile' => 'required|max:15',
            'name' => 'required|max:200',
            'membership_no' => 'required|max:100',
            'name' => 'required|max:200'
        ]);
        
        $member_categorys = $request->input('member_category');
        

        DB::beginTransaction();
        try {// all good
            $admin = Admin::create([
                'full_name' => $request->full_name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'password' => Hash::make('Password'),
                'user_type' => 'members',
                'client_id' => Helper::my_domain(),
            ]);
            
            $member_profile = MemberProfiles::create([
                'name' => $request->name,
                'membership_no' => $request->membership_no,
                'contact_num' => $request->office_phone,
                'address1' => $request->address1,
                'address2' => $request->address2,
                'city' => $request->city,                
                'state' => $request->state,
                'post_code' => $request->post_code,
                'country' => $request->country,
                'summary' => $request->summary,
                'status' => 'active',                
                'user_id' => $admin->id,
                'client_id' => Helper::my_domain(),  

            ]);
            
            
            
            if(!empty($member_categorys)) {
                $member_categorys_data = array();
                foreach($member_categorys as $member_category) {
                    $member_categorys_data[] = [
                        'client_categories_id'=> $member_category,
                        'member_profiles_id' => $member_profile->id,
                    ];
                }
                $client_categories = AssignCategories::insert($member_categorys_data);
            }
            
            if($member_profile && $admin) {
                DB::commit();
                $msg = 'Member Successfully added';
            }else {
                DB::rollback();
                $msg = 'Please try again!!';
            }
            return redirect()->back()->with('success', $msg);
            
        } catch (\Exception $e) {// something went wrong
            DB::rollback();
            //dd($e);
            return redirect()->back()->with('danger', 'something went wrong!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MemberProfiles  $memberProfiles
     * @return \Illuminate\Http\Response
     */
    public function show(MemberProfiles $memberProfiles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MemberProfiles  $memberProfiles
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($id);
        $client_id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;
        $membercategories=ClientCategories::where('deleted_at', '=', null)->where('client_id', '=', $client_id)->get();
        $memberprofiles = MemberProfiles::where('client_id', '=', $client_id)->findOrFail($id);

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('members.index'), 'name' => "Members List"], ['name' => $memberprofiles->name]];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('member-profiles.edit',compact('breadcrumbs','membercategories','memberprofiles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MemberProfiles  $memberProfiles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MemberProfiles $memberProfiles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MemberProfiles  $memberProfiles
     * @return \Illuminate\Http\Response
     */
    public function destroy(MemberProfiles $memberProfiles)
    {
        //
    }
}
