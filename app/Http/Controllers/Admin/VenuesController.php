<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Helper;

use App\Models\Venues;

class VenuesController extends Controller
{
    private $clientID;
    public function __construct()
    {
        $this->clientID = Helper::my_domain();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "All Venues"]];
        $models = Venues::where([['client_id', '=', $this->clientID],['venue_status', '!=' , 'Delete']])->latest()->get();
        return view('venues.index', ['breadcrumbs' => $breadcrumbs], compact('models'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;
        //$membercategories=ClientCategories::where('deleted_at', '=', null)->where('client_id', '=', $id)->get();

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('venues.index'), 'name' => "Venues List"], ['name' => "Create"]];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('venues.create',compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'venue_name' => ['required'],
            'address1' => ['required'],
            'city' => ['required'],
            'post_code' => ['required'],
            'state' => ['required'],
            //'country' => ['required'],
            'venue_status' => ['required'],
        ]);

        $data=$request->all();

        $data['client_id']=$this->clientID;
        $data['country']="Malaysia";
        // return $data;

        $status=Venues::create($data);


        if($status){
            request()->session()->flash('success','Venue Successfully added');
            //$msg = ['success', 'Venue Successfully added'];
        }
        else{
            request()->session()->flash('error','Please try again!!');
            //$msg = ['error', 'Please try again!!'];
        }
        return redirect()->route('venues.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Venues  $venues
     * @return \Illuminate\Http\Response
     */
    public function show(Venues $venues)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Venues  $venues
     * @return \Illuminate\Http\Response
     */
    public function edit(Venues $venues, $id)
    {
        $model=  Venues::findOrFail($id);
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('venues.index'), 'name' => "Venues List"], ['name' => "Edit"]];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('venues.edit',compact('breadcrumbs','model'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Venues  $venues
     * @return \Illuminate\Http\Response
     */
    public function status(Venues $venues)
    {
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Venues  $venues
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Venues $venues, $id)
    {
        $post = Venues::findOrFail($id);
        //$user_id = Auth::id();

        $this->validate($request,[
            'venue_name' => ['required'],
            'address1' => ['required'],
            'city' => ['required'],
            'post_code' => ['required'],
            'state' => ['required'],
            //'country' => ['required'],
            'venue_status' => ['required'],
        ]);

        $data=$request->all();
        $status=$post->fill($data)->save();
        //$data['client_id']=$client_id;
        //$data['country']="Malaysia";
        // return $data;

        if($status){
            request()->session()->flash('success','Venue Successfully update');
            //$msg = ['success', 'Venue Successfully added'];
        }
        else{
            request()->session()->flash('error','Please try again!!');
            //$msg = ['error', 'Please try again!!'];
        }
        return redirect()->route('venues.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Venues  $venues
     * @return \Illuminate\Http\Response
     */
    public function destroy(Venues $venues, $id)
    {
        $venues=Venues::find($id);
        if($venues){
            $venues->venue_status = 'Delete';
            $status=$venues->save();
            if($status){
                request()->session()->flash('success','Venue successfully deleted');
            }
            else{
                request()->session()->flash('error','Error, Please try again');
            }
            return redirect()->route('venues.index');
        }
        else{
            request()->session()->flash('error','Venue not found');
            return redirect()->back();
        }
    }
    
    public function destroystatus(Request $request, $id)
    {
        $venues=Venues::find($id);
        if($venues){
            $venues->venue_status = $request->status;
            $status=$venues->save();
            if($status){
                request()->session()->flash('success','Venue successfully changed');
            }
            else{
                request()->session()->flash('error','Error, Please try again');
            }
            return redirect()->route('venues.index');
        }
        else{
            request()->session()->flash('error','Venue not found');
            return redirect()->back();
        }
    }
}
