<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Helper;

use App\Models\Client;

class DashboardController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }


    public function index()
    {
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/
        return view('dashboard.index');
        /*$total_roles = count(Role::select('id')->get());
        $total_admins = count(Admin::select('id')->get());
        $total_permissions = count(Permission::select('id')->get());
        return view('admin.pages.dashboard.index', compact('total_admins', 'total_roles', 'total_permissions'));*/
    }
    
    public function companyprofile()
    {
        $id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;        
        $client = Client::findOrFail($id);
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('admin.roles.index'), 'name' => "All Roles"], ['name' => "Create Role"]];
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => $client->name]];
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/
        
        return view('dashboard.companyprofile', compact('breadcrumbs','client'));
    }
    
    public function companyprofileedit()
    {
        $id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;        
        $client = Client::findOrFail($id);
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('admin.roles.index'), 'name' => "All Roles"], ['name' => "Create Role"]];
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => $client->name]];
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/
        
        return view('dashboard.companyprofile', compact('breadcrumbs','client'));
    }
}
