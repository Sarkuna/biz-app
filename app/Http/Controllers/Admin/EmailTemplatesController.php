<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmailTemplate;
use Illuminate\Http\Request;

class EmailTemplatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "All Email Template"]];
        $models = EmailTemplate::latest()->paginate(5);
        return view('email-templates.index', ['breadcrumbs' => $breadcrumbs], compact('models'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmailTemplates  $emailTemplates
     * @return \Illuminate\Http\Response
     */
    public function show(EmailTemplates $emailTemplates)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmailTemplates  $emailTemplates
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emailtemplate=EmailTemplate::findOrFail($id);
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('email.index'), 'name' => "All Email Template"], ['name' => "Edit"]];
        return view('email-templates.edit', ['breadcrumbs' => $breadcrumbs])->with('emailtemplate',$emailtemplate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmailTemplates  $emailTemplates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $post = EmailTemplate::findOrFail($id);

        $this->validate($request,[
            //'name'=>'string|required',
            'subject'=>'string|required',
            'template'=>'string|nullable'
        ]);

        $data=$request->all();

        $status=$post->fill($data)->save();
        
        if($status){
            //request()->session()->flash('success','Post Successfully added');
            $msg = 'Email Template Successfully update';
        }
        else{
            //request()->session()->flash('error','Please try again!!');
            $msg = 'Please try again!!';
        }
        return redirect()->route('email.index')->with('success', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmailTemplates  $emailTemplates
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailTemplates $emailTemplates)
    {
        //
    }
}
