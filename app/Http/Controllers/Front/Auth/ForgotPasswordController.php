<?php

namespace App\Http\Controllers\Front\Auth;

//use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\User;
use App\Rules\IsValidPassword;

use DB;
use Mail; 
use Hash;
use Helper;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    //use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function showForgetPasswordForm()
    {
       return view('auth.passwords.forgetPassword');
    }
    
    public function submitForgetPasswordForm(Request $request)
      {
          $request->validate([
              'email' => 'required|email|exists:users',
          ]);
          
          $token = Str::random(64);

          $reset_token = $request->getSchemeAndHttpHost().'/reset-password/'.$token;
   
          DB::table('password_resets')->insert([
              'email' => $request->email, 
              'token' => $token, 
              'created_at' => Carbon::now()
            ]);
  
          /*Mail::send('email.forgetPassword', ['token' => $token], function($message) use($request){
              $message->to($request->email);
              $message->subject('Reset Password');
          });*/
          
          $data = ['email' => $request->email, 'reset_token' => $reset_token];    
          $subject = '';
          $return = Helper::sendEmailUser($request->email, $subject, 'forgot_password', $data);
          if($return > 0) {
              return back()->with('success', 'We have e-mailed your password reset link!');
          }else {
              return back()->with('error', 'Please try again!!');
          }

    }

    public function showResetPasswordForm($token) {
        return view('auth.passwords.forgetPasswordLink', ['token' => $token]);
    }
    
    public function submitResetPasswordForm(Request $request)
      {
          $request->validate([
                'password' => ['required', 'string', new isValidPassword()],
                'password_confirmation' => ['same:password'],
          ]);
  
          $updatePassword = DB::table('password_resets')
                              ->where([
                                //'email' => $request->email, 
                                'token' => $request->token
                              ])
                              ->first();

          if(!$updatePassword){
              return back()->withInput()->with('error', 'Invalid token!');
          }
  
          $user = User::where(['email'=> $updatePassword->email])
                      ->update(['password' => Hash::make($request->password_confirmation)]);
          DB::table('password_resets')->where(['email'=> $updatePassword->email])->delete();
          //dd($user);
          return redirect('/login')->with('success', 'Your password has been changed!');
      }
      
    public function showLinkRequestForm(){
      $pageConfigs = [
        'bodyClass' => "bg-full-screen-image",
        'blankPage' => true
      ];

      return view('/auth/passwords/email', [
        'pageConfigs' => $pageConfigs
      ]);
    }
}
