<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;

use App\User;
use App\Models\AppPageLogins;
use Auth;
use Hash;
use Helper;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // Login
    public function showLoginForm(){
        /*$data = json_encode(array(
            'name' => 'Mohamed Shihan',
            'password' => 'Absd12345678ERTY',
        ));
        $rt = Helper::sendEmailAdmin(1, 'welcome', $data);
        echo $rt;
        die;*/
      $pageConfigs = [
          'bodyClass' => "bg-full-screen-image",
          'blankPage' => true
      ];

      return view('/auth/login', [
          'pageConfigs' => $pageConfigs
      ]);
    }
    
    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            //'email' => 'required|string|email|max:255',
            //'password' => 'required|string|min:6|confirmed',
        ]);

        $credentials = $request->only('email', 'password');
        $credentials['client_id'] = Helper::my_domain();
        //dd($credentials);
        if (Auth::attempt($credentials)) {
            Auth::user()->date_last_login = Carbon::now()->format('Y-m-d H:i:s');
            Auth::user()->save();

            /*$pagelogs = new AppPageLogins();
            $pagelogs->user_id = Auth::user()->id;
            $pagelogs->save();*/
            //return redirect()->intended('home');
            return redirect()->route('user.home');
        }

        return redirect('/')->with('error', 'Oppes! You have entered invalid credentials');
    }
    
    public function loginUserbyid(Request $request, $token = null, $url= null) {
        $link = str_replace('_','/',$url);
        //dd($link);
        $user_id = DB::table('oauth_access_tokens')->where('id', $token)->where('revoked', '=', 0)->first();
        if (!empty($user_id)) {
            Auth::loginUsingId($user_id->user_id, true);
            $pagelogs = new AppPageLogins();
            $pagelogs->user_id = $user_id->user_id;
            $pagelogs->save();
            return redirect('/user/'.$link);
        } else {
            return redirect('/')->with('error', 'Oppes! You have entered invalid token');
        }
    }
    
    public function logoutapi(Request $request) {
        dd('test login');
        
        auth()->logout();
        Session()->flush();
        redirect('/login');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/login');
    }
}
