<?php

namespace App\Http\Controllers\Front\Auth;

use App\User;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

use App\Rules\MatchOldPassword;
use App\Rules\IsValidPassword;
use App\Models\MemberProfiles;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /*protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }*/

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    
    /*protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }*/

    // Register
    public function showRegistrationForm(){
      $client_id = Helper::my_domain();
      $pageConfigs = [
          'bodyClass' => "bg-full-screen-image",
          'blankPage' => true
      ];
      
      //client_id
      $members = MemberProfiles::where('status', 'active')->where('client_id', '=', $client_id)->orderBy('name')->pluck('name', 'id');
      //dd($members);
      return view('/auth/register', [
          'pageConfigs' => $pageConfigs,
          'members' => $members,
      ]);
    }
    
    public function postRegistration(Request $request)
    {
        // Validation Data
        //dd($request->all());
        $request->validate([
            'full_name' => 'required|max:50',
            //'email' => 'required|max:100|email|unique:users',
            'email' => 'required|max:100|email|unique:users,email,NULL,id,client_id,'.Helper::my_domain(),
            'password' => ['required', 'string', new isValidPassword()],
            'confirm_password' => ['same:password'],
            'terms' => 'required'
        ]);

        
        // Create New User
        $user = new User();
        $user->full_name = $request->full_name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->password = Hash::make($request->confirm_password);
        $user->member_id = $request->member_name;
        $user->client_id = Helper::my_domain();
        if($user->save()) {
            session()->flash('success', 'User has been created !!');
            return redirect()->route('login');
        }else {
            dd($user->error());
        }
        
    }
}
