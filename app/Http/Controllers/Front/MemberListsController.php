<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;
use File;
use DB;
use Auth;
use App\Helpers\Helper;

use App\Models\ClientCategories;
use App\Models\AssignCategories;
use App\Models\MemberProfiles;
use App\Models\Documents;

class MemberListsController extends Controller
{
    public function index()
    {
        $userId = Auth::id();
        $client_id = Helper::my_domain();
        //$client_id = 10;

        $client_categories = ClientCategories::where('client_id', '=', $client_id)->get();
        
        $members =DB::table('client_categories')
                ->join('assign_categories', 'client_categories.id', '=', 'assign_categories.client_categories_id')
                ->join('member_profiles', 'member_profiles.id', '=', 'assign_categories.member_profiles_id')
                ->select('member_profiles.id','member_profiles.client_id','member_profiles.name','member_profiles.photo','member_profiles.point_receipt', 'member_profiles.ref', 'member_profiles.description')               
                ->where('client_categories.client_id', '=', $client_id)
                ->groupBy('assign_categories.member_profiles_id');
        
        if(!empty($_GET['category'])){
            //$sort = $_GET['sortBy'];
            $members = $members->where('client_categories.member_cat_id', '=', $_GET['category']);
        }
        
        if(!empty($_GET['sortBy'])){
            $sort = $_GET['sortBy'];
            $members = $members->orderBy('name',$sort);
        }
        
        if(!empty($_GET['companyName'])){
            $members = $members->where('member_profiles.name','LIKE','%'.urldecode($_GET['companyName']).'%');
        }
        
        $members = $members->paginate(12);
        
        
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('admin.roles.index'), 'name' => "All Roles"], ['name' => "Create Role"]];
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => 'Mebers List']];       
        return view('member-lists.index', compact('breadcrumbs','members', 'client_categories'));
    }
    
    public function memberFilter(Request $request){
            $data= $request->all();
            //dd($data);
            // return $data;
            $catURL="";
            if(!empty($data['category'])){
                $catURL .='&category='.$data['category'];
                
            }

            $sortByURL='';
            if(!empty($data['sortBy'])){
                $sortByURL .='&sortBy='.$data['sortBy'];
            }
            
            $companyName='';
            if(!empty($data['companyname'])){
                $companyName .='&companyName='.$data['companyname'];
            }
            
            
            
            return redirect()->route('user.member.list',$catURL.$sortByURL.$companyName);
    }
    
    public function receiptFilter(Request $request){
            $data= $request->all();
            //dd($data);
            // return $data;
            $memberCategory="";
            if(!empty($data['category'])){
                $memberCategory .='&category='.$data['category'];
                
            }

            $memberName='';
            if(!empty($data['name'])){
                $memberName .='&name='.$data['name'];
            }
            
            
            
            return redirect()->route('receipts.create',$memberCategory.$memberName);
    }
    
    public function memberDetail($id){
        $client_id = Helper::my_domain();
        $member_detail = MemberProfiles::where('client_id',$client_id)->find($id);
        if(!empty($member_detail->ref)) {
            $photos = Documents::where('ref', '=', $member_detail->ref)->first();
            $portfolios1 = json_decode($photos->new_name, true);
            $portfolios2 = json_decode($photos->org_name, true);
            $portfolios = array_combine($portfolios1,$portfolios2);
            //$resJson = json_encode($portfolios);
            //$portfolios = json_decode($c, true);
        }else {
            $portfolios = '';
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('user.member.list'), 'name' => 'Mebers List'], ['name' => $member_detail->name]];
        return view('member-lists.view', compact('breadcrumbs','member_detail', 'portfolios'));
    }
}
