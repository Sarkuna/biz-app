<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helper;

use Auth;
use App\Models\ShippingAddresses;

class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::id();
        $client_id = Helper::my_domain();

        $models = ShippingAddresses::where([['user_id', '=', $userId]])->orderBy('default_as', 'asc')->get();

        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('admin.roles.index'), 'name' => "All Roles"], ['name' => "Create Role"]];
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => 'Shipping Address']];
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');*/
        
        return view('shipping.index', compact('breadcrumbs','models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client_id = Helper::my_domain();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('shipping.index'),'name' => "My Addresses"], ['name' => "New Address"]];
        //$warranties = PurchaseDetails::where('user_id',Auth::user()->id)->latest()->paginate(5);
        //return view('home', ['breadcrumbs' => $breadcrumbs], compact('warranties'));
        return view('shipping.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'string|required|max:255',
            'phone' => 'string|required|max:20',
            'address' => 'string|required|max:100',
            'state' => 'string|required',
            'area' => 'string|required|max:30',
            'post_code' => 'integer|required',
            'label_as' => 'required|in:Home,Work'
            //'default_as' => ['required'],
        ]);
        
        $count = ShippingAddresses::where(['user_id' => Auth::id()])->get()->count();
        if($count > 0) {
           $default_as = isset($request->default_as) ? "Yes" : "No"; 
        }else {
           $default_as = "Yes"; 
        }
        
        
        $shipping = new ShippingAddresses();
        $shipping->user_id = Auth::id();
        //$receipts->reasons_id = 'superadmin';
        $shipping->full_name = $request->full_name;
        $shipping->phone = $request->phone;
        $shipping->address = $request->address;
        $shipping->state = $request->state;
        $shipping->area = $request->area;
        $shipping->post_code = $request->post_code;
        $shipping->country = 'Malaysia';
        $shipping->label_as  = $request->label_as;
        $shipping->default_as  = $default_as;
        if($shipping->save()){
            //request()->session()->flash('success','Post Successfully added');
            $msg = 'Address Successfully added';
        }
        else{
            //request()->session()->flash('error','Please try again!!');
            $msg = 'Please try again!!';
        }
        //dd($shipping);
        return redirect()->route('shipping.index')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShippingAddress  $shippingAddress
     * @return \Illuminate\Http\Response
     */
    public function show(ShippingAddress $shippingAddress)
    {
        //
    }
    
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShippingAddress  $shippingAddress
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model=ShippingAddresses::findOrFail($id);
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('shipping.index'),'name' => "My Addresses"], ['name' => "Edit Address"]];
        return view('shipping.edit', compact('breadcrumbs','model'));
        //
    }
    
    public function addressDefault($id)
    {
        $model=ShippingAddresses::findOrFail($id);
        ShippingAddresses::where(['user_id' => Auth::id()])->update(['default_as' => 'No']);
        $model->default_as = 'Yes';
        $status=$model->save();
        if($status){
            //request()->session()->flash('success','Post Successfully added');
            $msg = 'Successfully update';
        }
        else{
            //request()->session()->flash('error','Please try again!!');
            $msg = 'Please try again!!';
        }
        return redirect()->route('shipping.index')->with('success', $msg);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShippingAddress  $shippingAddress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = ShippingAddresses::findOrFail($id);
        $this->validate($request,[
            'full_name' => 'string|required|max:255',
            'phone' => 'string|required|max:20',
            'address' => 'string|required|max:100',
            'state' => 'string|required',
            'area' => 'string|required|max:30',
            'post_code' => 'integer|required',
            'label_as' => 'required|in:Home,Work'
        ]);
        
        $default_as = isset($request->default_as) ? "Yes" : "No";

        $data=$request->all();


        if($post->default_as == 'No') {
            $default_as = isset($request->default_as) ? "Yes" : "No";        
            if($default_as == 'Yes') {
                ShippingAddresses::where(['user_id' => Auth::id()])->update(['default_as' => 'No']);
            }
            $data['default_as']=$default_as;
        }
        // return $data;

        $status=$post->fill($data)->save();
        if($status){
            //request()->session()->flash('success','Post Successfully added');
            $msg = 'Shipping Successfully update';
        }
        else{
            //request()->session()->flash('error','Please try again!!');
            $msg = 'Please try again!!';
        }
        return redirect()->route('shipping.index')->with('success', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShippingAddress  $shippingAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shipping=ShippingAddresses::find($id);
        if($shipping){
            $status=$shipping->delete();
            if($status){
                $msg = 'Shipping address successfully deleted';
            }
            else{
                $msg = 'Oops, Please try again';
            }
        }
        else{
            $msg = 'Oops, Shipping address not found';
        }
        
        return redirect()->route('shipping.index')->with('success', $msg);
    }
}
