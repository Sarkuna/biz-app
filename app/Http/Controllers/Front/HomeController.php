<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use File;
use DB;
use App\Rules\MatchOldPassword;
use App\Rules\IsValidPassword;

use App\Models\Uploads;
use App\User;
use App\Models\ClientCategories;
use App\Models\MemberProfiles;

use Auth;

class HomeController extends Controller
{
    private $photos_path;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->photos_path = public_path('/upload/');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('home', compact('breadcrumbs'));
        return view('home');
    }
    
    
    public function home2()
    {
        $msg = ['success' => 'Thank you your form was successfully submitted'];
        //dd($msg);
        return redirect()->route('user.home')->with($msg);
       // return view('home2');
    }

    public function changePassword(){
        $breadcrumbs = [['link' => "/", 'name' => "Home"]];
        return view('changePassword', compact('breadcrumbs'));
    }
    
    public function changPasswordStore(Request $request)
    {
        $request->validate([
            'password' => ['required', new MatchOldPassword],
            //'new_password' => ['required'],
            'new_password' => ['required', 'string', new isValidPassword()],
            'confirm_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->confirm_password)]);
   
        return redirect()->route('user.home')->with('success','Password successfully changed');
        //return redirect()->route('dashboard');
    }
    
    public function viewProfile(){
        $breadcrumbs = [['link' => "/", 'name' => "Home"]];
        return view('myProfile', compact('breadcrumbs'));
    }
    
    public function editProfile(){
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('user.my.profile'), 'name' => "My Profile"]];
        return view('editProfile', compact('breadcrumbs'));
    }
    
    public function editProfileStore(Request $request)
    {
        $request->validate([
            'full_name' => ['required'],
        ]);

        User::find(auth()->user()->id)->update([
            'full_name'=> $request->full_name,
            'mobile'=> $request->mobile,
            'gender'=> $request->gender,
            'dob'=> $request->dob,
        ]);
   
        return redirect()->route('user.my.profile')->with('success','Profile successfully update');
        //return redirect()->route('dashboard');
    }
    
    public function loginUserbyid(Request $request, $token = null, $url= null) {
        auth()->logout();
        Session()->flush();
        
        $link = str_replace('_','/',$url);
        //dd($link);
        $user_id = DB::table('oauth_access_tokens')->where('id', $token)->where('revoked', '=', 0)->first();
        if (!empty($user_id)) {
            Auth::loginUsingId($user_id->user_id, true);
            /*return response()->json([
                'message' => 'Successfully logged in'
            ]);*/
            //return true;
            //return redirect()->route('product-grids');
            return redirect('/user/'.$link);
        } else {
            return redirect('/')->with('error', 'Oppes! You have entered invalid token');
        }
    }

}
