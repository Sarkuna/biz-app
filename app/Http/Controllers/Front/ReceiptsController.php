<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

use Carbon\Carbon;
use File;
use DB;
use App\Helpers\Helper;

use Auth;
use App\Models\Receipts;
use App\Models\Uploads;
use App\User;
use App\Models\ClientCategories;
use App\Models\MemberProfiles;
use App\Models\Documents;

class ReceiptsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::id();
        $client_id = Helper::my_domain();

        $models = Receipts::where([['client_id', '=', $client_id],['user_id', '=', $userId]])->get();

        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('admin.roles.index'), 'name' => "All Roles"], ['name' => "Create Role"]];
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => 'Receipts List']];
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');*/
        
        return view('receipts.index', compact('breadcrumbs','models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client_id = Helper::my_domain();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('receipts.index'),'name' => "Receipts List"], ['name' => "Upload Receipt"]];
        $categories=ClientCategories::where('deleted_at', '=', null)->where('client_id', '=', $client_id)->get();
        $members=MemberProfiles::where('client_id', '=', $client_id)->get();
        //$warranties = PurchaseDetails::where('user_id',Auth::user()->id)->latest()->paginate(5);
        //return view('home', ['breadcrumbs' => $breadcrumbs], compact('warranties'));
        return view('receipts.create', compact('breadcrumbs','categories','members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        $now = Carbon::now();
        $ref = $user_id.'_'.$now->format('YmdHis');
        
        $request->validate([
            'member_category' => ['required'],
            'company_name' => ['required'],
            'receipt_no' => ['required'],
            'receipt_date' => ['required'],
            'receipt_amount' => ['required'],
        ]);

        if (!empty($request->hasFile('files'))) {            
            $portfolio_path = storage_path('app/public/receipts/'.$client_id.'/'.$ref);
            if (! File::exists($portfolio_path)) {
                mkdir($portfolio_path, 0775, TRUE);
            }
            
            $portfolios = $request->file('files');
            foreach($portfolios as $portfolio)
            {
                $destinationPath = $portfolio_path;
                $name = $portfolio->getClientOriginalName();
                $file_name = time().'_'.str_random(10). "." . $portfolio->getClientOriginalExtension();
                $portfolio->move($destinationPath,$file_name);  
                $dataorg[] = $name;
                $datanew[] = $file_name; 
            }
            
            $document_info = new Documents();
            $document_info->ref  = $ref;           
            $document_info->new_name=json_encode($datanew);
            $document_info->org_name=json_encode($dataorg);
            $document_info->save();
        }

        //dd($request->all());
        $receipts = new Receipts();
        $receipts->client_id = Helper::my_domain();
        $receipts->member_id = $request->company_name;
        $receipts->user_id = Auth::id();
        //$receipts->reasons_id = 'superadmin';
        $receipts->receipt_no = $request->receipt_no;
        $receipts->member_category = $request->member_category;
        $receipts->member_company_name = $request->company_name;
        $receipts->date_of_receipt = date('Y-m-d', strtotime($request->receipt_date));
        $receipts->receipt_total_amount = $request->receipt_amount;
        $receipts->visitor = $request->ip();
        $receipts->reasons_id = 99;
        $receipts->ref  = $ref;
        if($receipts->save()){
            //request()->session()->flash('success','Post Successfully added');
            $msg = 'Receipt Successfully added';
        }
        else{
            //request()->session()->flash('error','Please try again!!');
            $msg = 'Please try again!!';
        }
        return redirect()->route('receipts.index')->with('success', $msg);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Receipts  $receipts
     * @return \Illuminate\Http\Response
     */
    public function show(Receipts $receipts, $id)
    {
        $model=Receipts::find($id);
        if(!empty($model->ref)) {
            $photos = Documents::where('ref', '=', $model->ref)->first();
            $portfolios1 = json_decode($photos->new_name, true);
            $portfolios2 = json_decode($photos->org_name, true);
            $portfolios = array_combine($portfolios1,$portfolios2);
            //$resJson = json_encode($portfolios);
            //$portfolios = json_decode($c, true);
        }else {
            $portfolios = '';
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('receipts.index'),'name' => "Receipts List"], ['name' => 'View']];
        if($model){
            return view('receipts.show', compact('breadcrumbs','model', 'portfolios'));
        }
        else{
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Receipts  $receipts
     * @return \Illuminate\Http\Response
     */
    public function edit(Receipts $receipts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Receipts  $receipts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Receipts $receipts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Receipts  $receipts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Receipts $receipts)
    {
        //
    }
}
