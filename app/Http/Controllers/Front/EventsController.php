<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Helper;

use App\Models\Events;
use App\Models\Venues;

class EventsController extends Controller
{
    private $clientID;
    public function __construct()
    {
        $this->clientID = Helper::my_domain();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "All Venues"]];
        $models = Events::where([['client_id', '=', $this->clientID]])->latest()->get();
        return view('events.index', ['breadcrumbs' => $breadcrumbs], compact('models'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    
    public function pastEvents()
    {
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "All Venues"]];
        $models = Events::where([['client_id', '=', $this->clientID]])->latest()->get();
        return view('events.index', ['breadcrumbs' => $breadcrumbs], compact('models'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $venueslist=Venues::where([['client_id', '=', $this->clientID],['venue_status', '=', 'Visible']])->get();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('events.index'), 'name' => "Events List"], ['name' => "Create"]];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('events.create',compact('breadcrumbs','venueslist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[
            'audiences' => ['required'],
            'name' => ['required'],
            'description' => ['required'],
            'starts_on' => ['required'],
            'ends_on' => ['required'],
            'event_online' => ['required'],
            'ticket' => ['required'],
        ]);

        $data=$request->all();

        $data['client_id']=$this->clientID;
        $data['starts_on']=date('Y-m-d H:i:s', strtotime($request->starts_on));
        $data['ends_on']=date('Y-m-d H:i:s', strtotime($request->ends_on));
        // return $data;

        $status=Events::create($data);


        if($status){
            request()->session()->flash('success','Event Successfully added');
            //$msg = ['success', 'Venue Successfully added'];
        }
        else{
            request()->session()->flash('error','Please try again!!');
            //$msg = ['error', 'Please try again!!'];
        }
        return redirect()->route('events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function show(Events $events)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function edit(Events $events, $id)
    {
        $model= Events::findOrFail($id);
        $venueslist=Venues::where([['client_id', '=', $this->clientID],['venue_status', '=', 'Visible']])->get();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('events.index'), 'name' => "Events List"], ['name' => "Edit"]];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('events.edit',compact('breadcrumbs','venueslist','model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Events $events)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function destroy(Events $events)
    {
        //
    }
}
