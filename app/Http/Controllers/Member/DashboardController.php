<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
//use Helper;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use File;
use DB;
//use Auth;

use App\Models\Admin;
use App\Models\Banks;
use App\Models\BankDetails;
use App\Models\Documents;
use App\Models\MemberProfiles;

use App\Helpers\Helper;
use App\Rules\MatchOldPassword;
use App\Rules\IsValidPassword;

class DashboardController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('member')->user();
            return $next($request);
        });
    }


    public function index()
    {
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/
        $model = \App\Models\MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->whereNull('description')->count();
        if($model == 0){
            return view('dashboard.index');
        }else {
            
            $banks=Banks::where('status',"yes")->orderBy('bank_name')->get();
            return view('dashboard.company_setup',compact('banks'));
        }
        
        
        /*$total_roles = count(Role::select('id')->get());
        $total_admins = count(Admin::select('id')->get());
        $total_permissions = count(Permission::select('id')->get());
        return view('admin.pages.dashboard.index', compact('total_admins', 'total_roles', 'total_permissions'));*/
    }
    
    public function store(Request $request)
    {
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        $ref = '';
        $checkprofiles = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        $id = $checkprofiles->id;
        $model = MemberProfiles::findOrFail($id);
        $request->validate([
            'company_description' => 'required',
            'points_receipt' => 'required',
            'points_referral' => 'required',
        ]);
        
        if (!empty($request->hasFile('files'))) {
            $now = Carbon::now();
            $ref = $now->format('YmdHis');
            $portfolio_path = storage_path('app/public/portfolio/'.$client_id.'/'.$ref);
            if (! File::exists($portfolio_path)) {
                mkdir($portfolio_path, 0775, TRUE);
            }
            
            $portfolios = $request->file('files');
            foreach($portfolios as $portfolio)
            {
                $destinationPath = $portfolio_path;
                $name = $portfolio->getClientOriginalName();
                $file_name = time().'_'.str_random(10). "." . $portfolio->getClientOriginalExtension();
                $portfolio->move($destinationPath,$file_name);  
                $dataorg[] = $name;
                $datanew[] = $file_name; 
            }
            
            $document_info = new Documents();
            $document_info->ref  = $ref;           
            $document_info->new_name=json_encode($datanew);
            $document_info->org_name=json_encode($dataorg);
            $document_info->save();
        }
        
        $member_logo = storage_path('app/public/member_logo/'.$client_id);
        if (! File::exists($member_logo)) {
            mkdir($member_logo, 0775, TRUE);
        }
        
        if (!empty($request->hasFile('file'))) {
            $image = $request->file('file');
            $name = date('YmdHis');
            $save_name = $name . '.' . $image->getClientOriginalExtension();
            $image->move($member_logo,$save_name);
            $model->photo = $save_name;
        }
        
        $model->description = $request->company_description;
        $model->ref = $ref;
        $model->point_receipt = $request->points_receipt;
        $model->point_referral = $request->points_referral;
        if ($model->save()) {
            if(!empty($request->bank_name) && !empty($request->account_number) && !empty($request->account_name)) {
                $bankinfo = new BankDetails();            
                $bankinfo->member_profiles_id = $id;
                $bankinfo->bank_id = $request->bank_name;
                $bankinfo->account_number = $request->account_number;
                $bankinfo->account_name = $request->account_name;
                $bankinfo->account_verify = 'no';
                $bankinfo->save();
            }
            session()->flash('success', 'Your Business Profile Updated !!');
        }else {
            session()->flash('error', 'Sorry we couldn\'t update your business profile!!');
        }
        return redirect()->route('member.dashboard');
    }
    
    public function companyProfile()
    {
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/
        $profiles = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        //$profiles = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        $array_product = array(); 
        if(!empty($profiles->ref)) {
            $photos = Documents::where('ref', '=', $profiles->ref)->first();
            //$portfolios = json_decode($photos->new_name, true);
            
            //$data = array_merge(array($photos->new_name), array($photos->org_name));
            //$array_product["id"]= $photos->new_name;
            //$array_product["name"]= $photos->org_name;
            $portfolios1 = json_decode($photos->new_name, true);
            $portfolios2 = json_decode($photos->org_name, true);
            $portfolios = array_combine($portfolios1,$portfolios2);
            //$resJson = json_encode($portfolios);
            //$portfolios = json_decode($c, true);
        }else {
            $portfolios = '';
        }
        //dd($portfolios);
        return view('dashboard.company_profile',compact('profiles','portfolios'));
        /*$total_roles = count(Role::select('id')->get());
        $total_admins = count(Admin::select('id')->get());
        $total_permissions = count(Permission::select('id')->get());
        return view('admin.pages.dashboard.index', compact('total_admins', 'total_roles', 'total_permissions'));*/
    }
    
    public function changePassword(){
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Change Password"]];
        return view('dashboard.changePassword', ['breadcrumbs' => $breadcrumbs]);
    }
    
    public function changPasswordStore(Request $request)
    {
        $user_id = Auth::id();
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            //'new_password' => ['required'],
            'new_password' => ['required', 'string', new isValidPassword()],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        Admin::find($user_id)->update(['password'=> Hash::make($request->new_password)]);
   
        return redirect()->route('member.dashboard')->with('success','Password successfully changed');
        //return redirect()->route('dashboard');
    }
    
    public function profile(){
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        
        $profile = Admin::where('id', '=', $user_id)->first();
        $company_info = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Profile"]];
        
        return view('dashboard.profile',compact('profile','company_info','breadcrumbs'));
    }
}
