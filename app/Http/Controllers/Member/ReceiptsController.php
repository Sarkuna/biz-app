<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
//use Helper;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use File;
use DB;
//use Auth;


use App\Models\Receipts;
use App\Models\Documents;
use App\Models\Reasons;
use App\Models\MemberPoints;
use App\Models\UserPoints;

use App\Helpers\Helper;

class ReceiptsController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('member')->user();
            return $next($request);
        });
    }


    public function index()
    {
        $client_id = Helper::my_domain();
        $member_id = Helper::member_profile();

        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/

        $receipts = Receipts::where(['client_id' => $client_id, 'member_id' => $member_id, 'status' => 'pending'])->get();
        //'link' => route('receipt.index'),
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Receipt List"], ['name' => 'Pending']];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('receipt.index',compact('breadcrumbs','receipts'));
    }

    public function approve()
    {
        $client_id = Helper::my_domain();
        $member_id = Helper::member_profile();

        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/

        $receipts = Receipts::where(['client_id' => $client_id, 'member_id' => $member_id, 'status' => 'approved'])->get();
        //'link' => route('receipt.index'),
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Receipt List"], ['name' => 'Approved']];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('receipt.approve',compact('breadcrumbs','receipts'));
    }
    
    public function decline()
    {
        $client_id = Helper::my_domain();
        $member_id = Helper::member_profile();

        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/

        $receipts = Receipts::where(['client_id' => $client_id, 'member_id' => $member_id, 'status' => 'declined'])->get();
        //'link' => route('receipt.index'),
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Receipt List"], ['name' => 'Declined']];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('receipt.decline',compact('breadcrumbs','receipts'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client_id = Helper::my_domain();
        $member_id = Helper::member_profile();

        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/

        $model = Receipts::where(['client_id' => $client_id, 'member_id' => $member_id, 'id' => $id])->first();
        $reasons = Reasons::where('id', '!=', 99)->get();
        if(!empty($model->ref)) {
            $photos = Documents::where('ref', '=', $model->ref)->first();
            $portfolios1 = json_decode($photos->new_name, true);
            $portfolios2 = json_decode($photos->org_name, true);
            $portfolios = array_combine($portfolios1,$portfolios2);
            //$resJson = json_encode($portfolios);
            //$portfolios = json_decode($c, true);
        }else {
            $portfolios = '';
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Receipt List"], ['name' => 'Edit']];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('receipt.edit',compact('breadcrumbs','model','portfolios','reasons'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function receiptStore(Request $request, $id)
    {
        $date = Carbon::now();
        $post = Receipts::findOrFail($id);
        $point = $post->receipt_total_amount * $post->member_info->point_receipt;
 
        // Validation Data
        $request->validate([
            'status' => 'required',
            //'reason' => 'required_if:status,==,2',
            'reason' => 'required_if:status,declined',
        ]);
        
        $data=$request->all();

        $status=$post->fill($data)->save();
           
        if($status){
            if($post->status == 'approved') {
                $member_points = new MemberPoints();
                $member_points->client_id = $post->client_id;
                $member_points->member_profiles_id = $post->member_id;
                $member_points->user_id = $post->user_id;
                $member_points->num_points = '-'.(int)$point;
                $member_points->date_added = $date->toDateString();
                $member_points->description = 'Point redeemed to'.$post->receipt_order;
                $member_points->status = $member_points::STATUS_REDEEMED;
                $member_points->save();
                

                $user_points = new UserPoints();
                $user_points->client_id = $post->client_id;
                $user_points->member_profiles_id = $post->member_id;
                $user_points->user_id = $post->user_id;
                $user_points->receipts_id = $post->id;
                $user_points->num_points = (int)$point;
                $user_points->date_added = $date->toDateString();
                $user_points->description = 'Point awarded to'.$post->receipt_order;
                $user_points->status = $user_points::STATUS_AWARDED;
                $user_points->save();
            }
            //request()->session()->flash('success','Post Successfully added');
            $msg = 'Receipts Successfully update';
        }
        else{
            //request()->session()->flash('error','Please try again!!');
            $msg = 'Please try again!!';
        }
        return redirect()->route('receipt.index')->with('success', $msg);

    }
}
