<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Helpers\Helper;

use App\Models\PointPricings;
use App\Models\PurchaseHistories;

class PurchasePointsController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('member')->user();
            return $next($request);
        });
    }
    
    public function index()
    {
        $models = PointPricings::where('status', '=', 'active')->get();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => 'Points'], ['name' => 'Purchase Points']];
        return view('purchase-points.index', compact('breadcrumbs','models'));
    }
    
    public function purchaseconfirmation($id)
    {
        //dd($id);
        //$models = PointPricings::where('status', '=', 'active')->get();
        $models = PointPricings::findOrFail($id);
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('member.purchase.points'), 'name' => "Points"], ['name' => 'Purchase Points']];
        return view('purchase-points.payment_confirmation', compact('breadcrumbs','models'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $startDate = Carbon::today();
        $endDate = Carbon::today()->addDays(7);

        $point_price = PointPricings::findOrFail($request->pricings_id);
        // Validation Data
        $request->validate([
            'payment_method' => 'required',
            'terms1' => 'required',
            'terms2' => 'required',
        ]);
        
        //dd($request->all());

        // Create New User
        $model = new PurchaseHistories();
        $model->client_id = Helper::my_domain();
        $model->member_id = Auth::id();
        $model->invoice_date = $startDate->toDateString();
        $model->due_date = $endDate->toDateString();
        $model->per_price = $point_price->per_price;
        $model->total_amount = $point_price->points * $point_price->per_price;
        $model->point = $point_price->points;
        $model->visitor = $request->ip();
        $model->payment_method = $request->payment_method;


        if ($model->save()) {
            session()->flash('success', 'Thank you !!');
        }else {
            session()->flash('error', 'Payment not go through !!');
        }
        return redirect()->route('member.purchase.history');
    }
    
    public function purchaseHistory()
    {
        //dd($id);
        $models = PurchaseHistories::where('client_id', '=', Helper::my_domain())->where('member_id', '=', Auth::id())->get();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('member.purchase.points'), 'name' => "Points"], ['name' => 'Purchase Points']];
        return view('purchase-points.payment_history', compact('breadcrumbs','models'));
    }
}
