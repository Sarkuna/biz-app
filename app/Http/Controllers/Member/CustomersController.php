<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Models\MemberProfiles;

use App\Helpers\Helper;


class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        $memberprofiles = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        $models = User::where('client_id', '=', Helper::my_domain())->where('member_id', '=', $memberprofiles->id)->get();
 
        return view('customer.index', compact('models'));
    }

    
}
