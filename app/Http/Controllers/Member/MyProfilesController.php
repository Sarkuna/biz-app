<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
//use Helper;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use File;
use DB;
//use Auth;

use App\Models\Banks;
use App\Models\BankDetails;
use App\Models\Documents;
use App\Models\MemberProfiles;

use App\Helpers\Helper;

class MyProfilesController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('member')->user();
            return $next($request);
        });
    }


    public function index()
    {
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/
        $membercategories = '';
        $memberprofiles = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('member.company.profile'), 'name' => "My Profile"], ['name' => $memberprofiles->name]];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('my-profiles.edit',compact('breadcrumbs','membercategories','memberprofiles'));
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'company_name' => 'required',
            'description' => 'required',
            'point_receipt' => 'required',
            'point_referral' => 'required',
            'address1' => 'required',
            'contact_num' => 'required',
            
        ]);

        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        $ref = '';
        $checkprofiles = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        $id = $checkprofiles->id;
        $model = MemberProfiles::findOrFail($id);
        
        $model->name = $request->company_name;
        $model->description = $request->description;
        $model->contact_num = $request->contact_num;
        $model->address1 = $request->address1;
        $model->address2 = $request->address2;
        $model->city = $request->city;
        $model->post_code = $request->post_code;
        $model->state = $request->state;
        $model->country = $request->country;
        $model->point_receipt = $request->point_receipt;
        $model->point_referral = $request->point_referral;
        
        if ($model->save()) {            
            session()->flash('success', 'Your Business Profile Updated !!');
        }else {
            session()->flash('error', 'Sorry we couldn\'t update your business profile!!');
        }
        return redirect()->route('member.company.profile');
    }
    
    public function portfolio()
    {
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/
        
        $filecount = 0;
        $memberprofiles = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        if(!empty($memberprofiles->ref)) {
            $photos = Documents::where('ref', '=', $memberprofiles->ref)->first();
            $portfolios = json_decode($photos->new_name, true);
            $filecount = count($portfolios);
            $filecount = 5 - count($portfolios);
        }else {
            $portfolios = '';
        }
        //dd($count);
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('member.company.profile'), 'name' => "My Profile"], ['name' => $memberprofiles->name]];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('my-profiles.portfolio',compact('breadcrumbs','memberprofiles','filecount'));
    }
    
    public function storeImage(Request $request)
    {
        dd('test');
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        $id = $request->id;

        $model = MemberProfiles::findOrFail($id);
        $result = Documents::where('ref', '=', $model->ref)->first();
        
        if(empty($result)) {
            $ref = $now->format('YmdHis');
            $datanew = array();
            $dataorg = array();
        }else {
            $ref = $model->ref;
            if(!empty($result->new_name)) {
               $datanew = json_decode($result->new_name); 
            }else {
               $datanew = array(); 
            }
            
            if(!empty($result->org_name)) {
               $dataorg = json_decode($result->org_name);
            }else {
               $dataorg = array(); 
            }
        }
        
        
        if (!empty($request->hasFile('files'))) {
            $now = Carbon::now();
            
            $portfolio_path = storage_path('app/public/portfolio/'.$client_id.'/'.$ref);
            if (! File::exists($portfolio_path)) {
                mkdir($portfolio_path, 0775, TRUE);
            }
            
            $portfolios = $request->file('files');
            foreach($portfolios as $portfolio)
            {
                $destinationPath = $portfolio_path;
                $name = $portfolio->getClientOriginalName();
                $file_name = time().'_'.str_random(10). "." . $portfolio->getClientOriginalExtension();
                $portfolio->move($destinationPath,$file_name);  
                $dataorg[] = $name;
                $datanew[] = $file_name; 
            }
            
            if(empty($result)) {                
                $model->ref = $ref;
                $model->save();
                
                $document_info = new Documents();
                $document_info->ref  = $ref;           
                $document_info->new_name=json_encode($datanew);
                $document_info->org_name=json_encode($dataorg);
                //$document_info->save();
                if($document_info->save()) {
                   session()->flash('success', 'Image successfuly added !!'); 
                }else {
                   session()->flash('error', 'Oops something went wrong'); 
                }
            }else {
                $updatenewname = json_encode($datanew);
                $update_name_org = json_encode($dataorg);
                
                $update = Documents::where('id',$result->id)->update(['new_name' => $updatenewname, 'org_name' => $update_name_org]); 
                if($update) {
                    session()->flash('success', 'Image successfuly added !!'); 
                }else {
                   session()->flash('error', 'Oops something went wrong'); 
                }
            }
            //session()->flash('success', 'Your Business Profile Updated !!');
        }else {
            session()->flash('warning', 'Sorry no image select');
        }
        
        return redirect()->route('member.company.profile');
    }
    
    public function deleteImage(Request $request)
    {
        $client_id = Helper::my_domain();
        $newname = $request->new_name;
        //$newname = '1631015685_kiwrpUHNBG.jpg';
        //$newname = '1631015685_VlwfspXzZU.png';
        $orgname = $request->org_name;
        
        $result = Documents::where('ref', '=', $request->ref)->first();
        
        //Image New Name
        $portfolios = json_decode($result->new_name, true);
        $msg = 0;
        foreach($portfolios as $key => $val){
            if($val == $newname) {
                unset($portfolios[$key]);
                $filename  = storage_path('app/public/portfolio/'.$client_id.'/'.$request->ref.'/'.$newname);
                if(File::exists($filename)) {
                    File::delete($filename);
                }
                $msg = 1;
            }else {
                $datanew[] = $val;
            }            
            //$datanew[] = $file_name; 
        }
        $resultnew =array_values( array_unique( $datanew, SORT_REGULAR ) );
        $updatenewname = json_encode($resultnew);
        
        //Image Orginal Name
        $portfolio_org = json_decode($result->org_name, true);

        foreach($portfolio_org as $key => $val){
            if($val == $orgname) {
                unset($portfolio_org[$key]);        
            }else {
                $data_name_org[] = $val;
            }
        }
        $result_name_org =array_values( array_unique( $data_name_org, SORT_REGULAR ) );
        $update_name_org = json_encode($result_name_org);        
        
        //dd($updatenewname.'-'.$update_name_org);        
        Documents::where('id',$result->id)->update(['new_name' => $updatenewname, 'org_name' => $update_name_org]); 
        
        if ($msg == 1) {            
            session()->flash('success', 'Image successfully deleted');
        }else {
            session()->flash('warning', 'Oops something went wrong');
        }
        return redirect()->route('member.company.profile');
    }
    
    public function bankNew()
    {
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/
        $banks=Banks::where('status',"yes")->orderBy('bank_name')->get();
        $membercategories = '';
        $memberprofiles = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        if(!empty($memberprofiles->bank_info)) {
            return redirect()->route('member.myprofile.bank.edit');
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('member.company.profile'), 'name' => "My Profile"], ['name' => $memberprofiles->name]];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('my-profiles.new_bank',compact('breadcrumbs','membercategories','memberprofiles','banks'));
    }
    
    public function bankEdit()
    {
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        //dd(config('auth.defaults.guard'));
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/
        $banks=Banks::where('status',"yes")->orderBy('bank_name')->get();
        $membercategories = '';
        $memberprofiles = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        if(empty($memberprofiles->bank_info)) {
            return redirect()->route('member.myprofile.bank.new');
        }

        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('member.company.profile'), 'name' => "My Profile"], ['name' => $memberprofiles->name]];
        //return view('client.create', ['breadcrumbs' => $breadcrumbs])->with('membercategories',$membercategories);
        return view('my-profiles.edit_bank',compact('breadcrumbs','membercategories','memberprofiles','banks'));
    }
    
    public function bankStore(Request $request)
    {
        $client_id = Helper::my_domain();
        $user_id = Auth::id();
        $verify = 'no';
        $memberprofiles = MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
        
        $chkbank = BankDetails::where('member_profiles_id', '=', $memberprofiles->id)->first();
        if(empty($chkbank)) {
            $bankinfo = new BankDetails();            
            $bankinfo->member_profiles_id = $memberprofiles->id;
            
        }else {
            $id = $chkbank->id;
            if($chkbank->account_verify == 'yes') {
                if($chkbank->account_number == $request->account_number) {
                    $verify = 'yes';
                }else {
                    $verify = 'no';
                }
            }else {
                $verify = 'no';
            }

            $bankinfo = BankDetails::findOrFail($id);
        }
        
        $bankinfo->bank_id = $request->bank_name;
        $bankinfo->account_number = $request->account_number;
        $bankinfo->account_name = $request->account_name;
        $bankinfo->account_verify = $verify;
        if ($bankinfo->save()) {
            session()->flash('success', 'Bank account info successfuly save !!');
        } else {
            session()->flash('error', 'Oops something went wrong');
        }


        return redirect()->route('member.company.profile');              
    }
}
