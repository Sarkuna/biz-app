<?php

namespace App\Http\Controllers\Member\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
//use GuzzleHttp\Client;
use Carbon\Carbon;
use App\Admin;
use Auth;
use Hash;
use Helper;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/';
    protected $redirectTo = RouteServiceProvider::CLIENT_DASHBOARD;
    protected $guard = 'member';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        //$this->middleware('admin')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('member');
    }
    

    // Login
    public function showLoginForm(){
        return view('auth.login');
    }
    
    public function postLogin(Request $request)
    {
        $client_id = Helper::my_domain();
        //dd($request->all());die;
        // Validate Login Data
        $request->validate([
            'email' => 'required|max:50',
            'password' => 'required',
        ]);
	$credentials = $request->only('email', 'password');
        //dd($this->guard());
        if (Auth::guard('member')->attempt(['email' => $request->email, 'password' => $request->password, 'user_type' => 'members', 'client_id' => $client_id], $request->remember)) {
            $user = Auth()->guard('member')->user();
            $user->date_last_login = Carbon::now()->format('Y-m-d H:i:s');
            $user->save();
            //\AdminLogActivity::addToLog('Successully Logged in!');
            session()->flash('success', 'Successully Logged in !');
            //return redirect()->route('dashboard');
            return redirect()->intended('/member/dashboard');
        }
        // error
        session()->flash('error', 'Invalid email and password');
        return back();
       
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/member');
    }
}
