<?php 
  
namespace App\Http\Controllers\Member\Auth; 
  
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\User;
use App\Rules\IsValidPassword;

use DB;
use Mail; 
use Hash;
use Helper;
  
class ForgotPasswordController extends Controller
{
      /**
       * Write code on Method
       *
       * @return response()
       */
      public function showForgetPasswordForm()
      {
        return view('auth.passwords.forgetPassword');
      }
  
      /**
       * Write code on Method
       *
       * @return response()
       */
      public function submitForgetPasswordForm(Request $request)
      {
          
          $type = 'members';
          $request->validate([
              'email' => 'required|email|exists:admins,email,user_type,'.$type,
          ]);
          
          $token = Str::random(64);

          $reset_token = $request->getSchemeAndHttpHost().'/member/reset-password/'.$token;
   
          DB::table('password_resets')->insert([
              'email' => $request->email, 
              'token' => $token, 
              'created_at' => Carbon::now()
            ]);
  
          /*Mail::send('email.forgetPassword', ['token' => $token], function($message) use($request){
              $message->to($request->email);
              $message->subject('Reset Password');
          });*/
          
          $data = ['email' => $request->email, 'reset_token' => $reset_token];    
          $subject = '';
          $return = Helper::sendEmailUser($request->email, $subject, 'forgot_password', $data);
          if($return > 0) {
              return back()->with('success', 'We have e-mailed your password reset link!');
          }else {
              return back()->with('error', 'Please try again!!');
          }

      }
      /**
       * Write code on Method
       *
       * @return response()
       */
      public function showResetPasswordForm($token) {
         return view('auth.passwords.forgetPasswordLink', ['token' => $token]);
      }
  
      /**
       * Write code on Method
       *
       * @return response()
       */
      public function submitResetPasswordForm(Request $request)
      {
          $request->validate([
                'password' => ['required', 'string', new isValidPassword()],
                'password_confirmation' => ['same:password'],
          ]);
  
          $updatePassword = DB::table('password_resets')
                              ->where([
                                //'email' => $request->email, 
                                'token' => $request->token
                              ])
                              ->first();
  
          if(!$updatePassword){
              return back()->withInput()->with('error', 'Invalid token!');
          }
  
          $user = \App\Models\Admin::where('email', $updatePassword->email)
                      ->update(['password' => Hash::make($request->password)]);
 
          DB::table('password_resets')->where(['email'=> $updatePassword->email])->delete();
  
          return redirect('/member')->with('success', 'Your password has been changed!');
      }
}