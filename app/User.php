<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;
use App\Helpers\Helper;
use Wuwx\LaravelAutoNumber\AutoNumberTrait;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, AutoNumberTrait;
    
    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $model->customer_code = $model->customer_code;
        });       
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'email', 'password', 'client_id', 'member_id', 'gender', 'dob'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'date_last_login' => 'datetime',
        'dob' => 'date',
    ];
    
    public function getAutoNumberOptions()
    {
        return [
            'customer_code' => [
                'format' => function () {
                    return 'C'.Helper::my_domain().date('my') . '?'; // autonumber format. '?' will be replaced with the generated number.
                },
                'length' => 5, // The number of digits in the autonumber
            ],
        ];
    }
    
    public static function getpermissionGroups()
    {
        $permission_groups = DB::table('permissions')
            ->select('group_name as name')
            ->groupBy('group_name')
            ->get();
        return $permission_groups;
    }

    public static function getpermissionsByGroupName($group_name)
    {
        $permissions = DB::table('permissions')
            ->select('name', 'id')
            ->where('group_name', $group_name)
            ->get();
        return $permissions;
    }

    public static function roleHasPermissions($role, $permissions)
    {
        $hasPermission = true;
        foreach ($permissions as $permission) {
            if (!$role->hasPermissionTo($permission->name)) {
                $hasPermission = false;
                return $hasPermission;
            }
        }
        return $hasPermission;
    }
    
    public function member_info(){
        return $this->hasOne('App\Models\MemberProfiles','id','member_id');
    }
}
