<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\SiteOptions;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        /*$path_array = $request->segments();
        print_r($path_array);
        //die;
        //
        if($subDomain == 'rayadmin') {
            $path = resource_path('admin/views');
        }else {
            $path = resource_path('front/views');
        }
        view()->addLocation($path);*/

        $siteoptions = SiteOptions::pluck('option_value', 'option_name');
        config()->set(['app.siteoptions' => $siteoptions->toArray()]);    
        /*config([
            'global' => Setting:all([
                'name','value'
            ])
            ->keyBy('name') // key every setting by its name
            ->transform(function ($setting) {
                 return $setting->value // return only the value
            })
            ->toArray() // make it an array
        ]);*/

        
        $path_array = $request->segments();
        $admin_route = config('app.admin_route');
        $client_route = config('app.client_route');
        $member_route = config('app.member_route');
        //print_r($path_array);
    
        //If URL path is having "admin" then mark the current scope as Admin
        if (in_array($admin_route, $path_array)) {
            config(['app.app_scope' => 'admin']); 
        }else if (in_array($client_route, $path_array)) {
            config(['app.app_scope' => 'client']); 
        }else if (in_array($member_route, $path_array)) {
            config(['app.app_scope' => 'member']); 
        }


        $app_scope = config('app.app_scope');
        
        //if App scope is admin then define View/Theme folder path
        if ($app_scope == 'admin') {
            $path = resource_path('admin/views');
        }else if ($app_scope == 'member') {
            $path = resource_path('member/views');
        } else {
            $path = resource_path('front/views');
        }

        view()->addLocation($path);
        
        \Blade::directive('convert', function ($value) {
            return "<?php echo 'RM ' . number_format($value, 2); ?>";
        });
        
        \Blade::directive('convertpoint', function ($point) {
            return "<?php echo number_format($point); ?>";
        });
    }
    
    
}
