<?php

namespace App\Models;

Use App\Models\Admin;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //use AutoNumberTrait;
    //public static $_table = 'clients';
    public static function boot()
     {
        parent::boot();
        /*static::creating(function($model)
        {
            $user = Auth::user();           
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
            $model->subscription_code = $model->subscription_code;
        });*/
        static::updating(function($model)
        {
            $user = Auth::user();
            $model->updated_by = $user->id;
        });       
    }
    
    protected $fillable = [
        'name',
        'office_phone',
        'address',
        'city',
        'state',
        'post_code',
        'country',
    ];
    
    public function admins() {
        return $this->belongsTo(Admin::class);
    }


}
