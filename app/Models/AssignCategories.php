<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignCategories extends Model
{
    protected $fillable = [
        'client_categories_id','member_profiles_id'
    ];
    
}
