<?php

namespace App\Models;

use Auth;

use Illuminate\Database\Eloquent\Model;

class MemberProfiles extends Model
{
    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            //$user = Auth::user();
            //$id = Auth::guard(config('auth.defaults.guard'))->user()->client_id;
            $user = Auth()->guard(config('auth.defaults.guard'))->user();
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
        });
        static::updating(function($model)
        {
            $user = Auth()->guard(config('auth.defaults.guard'))->user();
            $model->updated_by = $user->id;
        });       
    }
    
    //protected $dates = ['date_of_purchase', 'expiry_date'];
    
    protected $fillable = [
        'name', 'membership_no', 'contact_num', 'address1', 'address2','city','post_code','state','country','summary','status','client_id', 'user_id', 'photo', 'description', 'ref', 'point_receipt', 'point_referral'
    ];
    
    public function member_info(){
        return $this->hasOne('App\Models\Admin','id','user_id');
    }
    
    public function bank_info(){
        return $this->hasOne('App\Models\BankDetails','member_profiles_id','id');
    }
}
