<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Wuwx\LaravelAutoNumber\AutoNumberTrait;

class UserPoints extends Model
{
    use AutoNumberTrait;
    const STATUS_AWARDED = 'awarded';
    const STATUS_EXPIRED = 'expired';
    const STATUS_REDEEMED = 'redeemed';
    const STATUS_REFUNDED = 'REFUNDED';
    
    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $model->ref_number = $model->ref_number;
        });       
    }
    
    protected $fillable = [
        'ref_number', 'client_id', 'member_profiles_id', 'user_id', 'receipts_id', 'num_points','date_added','description','status'
    ];
    
    public function getAutoNumberOptions()
    {
        return [
            'ref_number' => [
                'format' => function () {
                    return 'P' . '/?'; // autonumber format. '?' will be replaced with the generated number.
                },
                'length' => 10, // The number of digits in the autonumber
            ],
        ];
    }
}
