<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cart;
class Product extends Model
{
    public $file_photo;
    
    protected $fillable=['title','sku','slug','product_code','summary','description','cat_id','child_cat_id','price','sale_price','brand_id','discount','status','photo','stock','is_featured','condition','links','ref','shipping_price_em','shipping_price_wm'];

    public function cat_info(){
        return $this->hasOne('App\Models\Category','id','cat_id');
    }
    public function sub_cat_info(){
        return $this->hasOne('App\Models\Category','id','child_cat_id');
    }
    public function brand_info(){
        return $this->hasOne('App\Models\Brand','id','brand_id');
    }
    public static function getAllProduct(){
        return Product::with(['cat_info','sub_cat_info','brand_info'])->orderBy('id','desc')->paginate(10);
    }
    public function rel_prods(){
        return $this->hasMany('App\Models\Product','cat_id','cat_id')->where('status','active')->orderBy('id','DESC')->limit(8);
    }
    public function getReview(){
        return $this->hasMany('App\Models\ProductReview','product_id','id')->with('user_info')->where('status','active')->orderBy('id','DESC');
    }
    public static function getProductBySlug($slug){
        return Product::with(['cat_info','rel_prods'])->where('slug',$slug)->first();
    }
    public static function countActiveProduct(){
        $data=Product::where('status','active')->count();
        if($data){
            return $data;
        }
        return 0;
    }

    public function carts(){
        return $this->hasMany(Cart::class)->whereNotNull('order_id');
    }

    public function wishlists(){
        return $this->hasMany(Wishlist::class)->whereNotNull('cart_id');
    }
    
    public function gallery_image(){
        return $this->hasOne('App\Models\Documents','ref','ref');
    }

}
