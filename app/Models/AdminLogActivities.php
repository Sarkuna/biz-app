<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminLogActivities extends Model
{
    protected $fillable = [
        'subject', 'url', 'method', 'ip', 'user_type', 'agent', 'user_id'
    ];
}
