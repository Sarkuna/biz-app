<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientCategories extends Model
{
    protected $fillable=['client_id','member_cat_id'];
    
    public function Category(){
        return $this->hasOne('App\Models\MemberCategories','id','member_cat_id');
    }
    
    public function assign_info(){
        return $this->hasMany('App\Models\AssignCategories','client_categories_id','id');
    }

}
