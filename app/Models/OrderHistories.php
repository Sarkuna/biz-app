<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistories extends Model
{
    protected $fillable=['order_id','notify','comment','order_status'];
}
