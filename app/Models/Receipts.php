<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Wuwx\LaravelAutoNumber\AutoNumberTrait;

use Auth;
use App\Models\MemberProfiles;

class Receipts extends Model
{
    use AutoNumberTrait;
    //public static $_table = 'clients';
    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $user = Auth::user();           
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
            $model->receipt_order = $model->receipt_order;
        });
        static::updating(function($model)
        {
            $user = Auth::user();
            $model->updated_by = $user->id;
        });       
    }
    
    protected $fillable = [
        'client_id', 'member_id', 'user_id', 'reasons_id', 'receipt_no','member_category','member_company_name','date_of_receipt','receipt_total_amount','ref','status'
    ];
    
    protected $casts = [
        'date_of_receipt' => 'date',
    ];
    
    public function getAutoNumberOptions()
    {
        return [
            'receipt_order' => [
                'format' => function () {
                    return 'R' . '/?'; // autonumber format. '?' will be replaced with the generated number.
                },
                'length' => 6, // The number of digits in the autonumber
            ],
        ];
    }
    
    public function member_info(){
        return $this->hasOne('App\Models\MemberProfiles','id','member_id');
    }
    
    public function user_info(){
        return $this->hasOne('App\User','id','user_id');
    }
    
    public function client_category(){
        return $this->hasOne('App\Models\ClientCategories','id','member_category');
    }
}
