<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model
{
    protected $fillable = [
        'member_profiles_id','bank_id','account_number','account_name','account_verify'
    ];
    
    public function bank(){
        return $this->hasOne('App\Models\Banks','id','bank_id');
    }
}
