<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Auth;

class Venues extends Model
{
    protected $fillable = [
        'client_id', 'venue_name', 'address1', 'address2', 'city', 'state', 'post_code', 'country', 'venue_status'
    ];
}
