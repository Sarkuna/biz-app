<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Wuwx\LaravelAutoNumber\AutoNumberTrait;

class PurchaseHistories extends Model
{
    use AutoNumberTrait;
    //public static $_table = 'clients';
    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $model->invoice_no = $model->invoice_no;
        });     
    }
    
    protected $fillable = [
        'client_id',
        'member_id',
        'invoice_date',
        'due_date',
        'total_amount',
        'point',
        'summary',
        'visitor',
        'status',
        'payment_method',
        'per_price',
        'terms'
    ];
    
    protected $casts = [
        'invoice_date' => 'date',
        'due_date' => 'date',
    ];
    
    public function getAutoNumberOptions()
    {
        return [
            'invoice_no' => [
                'format' => function () {
                    return date('y') . '?'; // autonumber format. '?' will be replaced with the generated number.
                },
                'length' => 4, // The number of digits in the autonumber
            ],
        ];
    }
}
