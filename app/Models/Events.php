<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $fillable = [
        'client_id', 'audiences', 'name', 'description', 'starts_on', 'ends_on', 'event_online', 'venue_id', 'online_link', 'ticket', 'price', 'promotional_price', 'quantity', 'per_attendee', 'voting_duration', 'points', 'youtube_video_url', 'external_link', 'contact_phone_number', 'contact_email_address', 'instagram', 'facebook', 'linkedin', 'artists', 'main_image', 'notification', 'images_gallery', 'reviews'
    ];
}