<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointPricings extends Model
{
    protected $fillable = [
        'packages',
        'points',
        'per_price',
        'status',
    ];
}
