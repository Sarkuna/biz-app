<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $fillable=['order_id','user_id','product_id','title','sku','product_code','photo','quantity','sale_point','shipping_point_em','shipping_point_wm','total_point','status','tracking','tracking_no','tracking_link','remark'];
}
