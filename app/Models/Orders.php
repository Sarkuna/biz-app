<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Wuwx\LaravelAutoNumber\AutoNumberTrait;

use Auth;
class Orders extends Model
{
    use AutoNumberTrait;
    
    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $model->order_number = $model->order_number;
        });      
    }
    
    protected $fillable=['order_number','user_id','status','company','first_name','last_name','email','phone','tel','address1','address2','city','state','post_code','country','visitor','user_agent','client_id'];
    
    public function cart_info(){
        return $this->hasMany('App\Models\Cart','order_id','id');
    }
    public static function getAllOrder($id){
        return Order::with('cart_info')->find($id);
    }
    public static function countActiveOrder(){
        $data=Order::count();
        if($data){
            return $data;
        }
        return 0;
    }
    public function cart(){
        return $this->hasMany(Cart::class);
    }

    public function shipping(){
        return $this->belongsTo(Shipping::class,'shipping_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function getAutoNumberOptions()
    {
        return [
            'order_number' => [
                'format' => function () {
                    return 'ORD' . '/?'; // autonumber format. '?' will be replaced with the generated number.
                },
                'length' => 6, // The number of digits in the autonumber
            ],
        ];
    }
}
