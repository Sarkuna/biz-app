<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingAddresses extends Model
{
    protected $fillable = [
        'user_id',
        'full_name',
        'phone',
        'address',
        'state',
        'area',
        'post_code',
        'country',
        'label_as',
        'default_as'
    ];
}
