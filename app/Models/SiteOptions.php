<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteOptions extends Model
{
    protected $fillable=['option_name','option_value','autoload'];

}
