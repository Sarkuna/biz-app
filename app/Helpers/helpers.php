<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use Config;
use Illuminate\Support\Str;
use Mail;
use YoHang88\LetterAvatar\LetterAvatar;
use Auth;
use Illuminate\Http\Request;

use App\Models\Message;
use App\Models\Category;
use App\Models\PostTag;
use App\Models\PostCategory;
use App\Models\Order;
use App\Models\Wishlist;
use App\Models\Shipping;
use App\Models\Cart;

class Helper
{
    public static function shout(string $string)
    {
        return strtoupper($string);
    }
    
    public static function basic_email() {
      $data = array('name'=>"Virat Gandhi");
        $html = "test email";
        $tomail = 'shihanagni@gmail.com';
        $subject = 'hi';
        /*Mail::send(array(), array(), function ($message) use ($html) {
            $message->to('shihanagni@gmail.com')
              ->from('noreplay@rewardssolution.com')->subject('hi')->setBody($html, 'text/html');
          });*/

        /*Mail::send(
                'admin.email.forgot',
                ['admin' => $admin, 'token' => $token],
                function($message) use ($admin){
                    $message->from('noreplay@rewardssolution.com');
                    $message->to($admin->email);
                    $message->subject("$admin->email, rest your password.");
                }
        );*/
        //$data= '';
       Mail::send('mail.mail-welcome', $data, function($message) use($data) {
            $message->from('noreplay@rewardssolution.com');
            $message->to('shihanagni@gmail.com');
            $message->subject('New email!!!');
        });   
       if (Mail::failures()) {
            return "False Basic Email Sent. Check your inbox."; 
        }else  {
            return "True Basic Email Sent. Check your inbox.";
        }   
      //return "Basic Email Sent. Check your inbox.";
   }
   
   public static function sendEmailAdmin($userId, $emailTemplateCode, $data = null)
   {
        if ($data != null) {
            $data2 = json_decode($data, true);
        }
        
        $name = isset($data2['name']) ? $data2['name'] : null;
        $password = isset($data2['password']) ? $data2['password'] : null;

        $html = '<div style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; text-align: center; padding: 30px 25px 25px; border-top-left-radius: 5px; border-top-right-radius: 5px;"><a href="{store_url}" title="{store_name}"><img alt="{store_name}" class="logo" src="http://liveadmin.businessboosters.com.my/upload/files/14/Kansai_Paint-primary_grad_rgb.png" style="height:62px; margin-bottom:0px; width:200px" /></a></div>

<div style="padding: 20px 20px 15px;">
<p>Dear {name} ({password}),</p>

<p>&nbsp;</p>

<p>Welcome to the latest reward programme, Kansai Incentive Scheme (KIS), specially design to reward all our loyal dealers &amp; distributors.</p>

<p>KIS is how we from Kansai would like to say THANK YOU to all our supportive dealers and distributors, and we hope that we can grow together to greater heights in the years ahead.</p>

<p>Our variety of products for redemption will ensure KIS will always remain exciting &amp; current.</p>

<p>Wait no further, go to {site_url} &amp; insert your:</p>

<p>Username: {name}<br />
Password: {password}</p>

<p>Have fun with this rewarding journey. We look forward to your active support for many more years to come.</p>

<p>Best regards,<br />
KANSAI PAINT ASIA PACIFIC SDN. BHD.</p>
</div>

<div style="background: rgb(48, 50, 50); color: rgb(255, 255, 255); text-align: center; padding: 30px 25px 25px; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;"><span style="background-color:rgb(19, 19, 19); color:rgb(187, 187, 187); font-family:raleway,sans-serif">Copyright (C) 2019&nbsp;</span><a href="http://kis.my/#" style="color: rgb(214, 100, 74); box-sizing: border-box; background-color: rgb(19, 19, 19); text-decoration-line: none; outline: 0px; transition: all 0.4s ease 0s; font-family: Raleway, sans-serif;">Rewards Solution.</a><span style="background-color:rgb(19, 19, 19); color:rgb(187, 187, 187); font-family:raleway,sans-serif">&nbsp;All Rights Reserved.</span></div>
';

$emailBody = $html;

$emailBody = str_replace('{name}', $name, $emailBody);
$emailBody = str_replace('{password}', $password, $emailBody); 

        $tomail = 'shihanagni@gmail.com';
        $subject = 'Welcome Email - '.$emailTemplateCode;
        Mail::send(array(), array(), function ($message) use ($tomail,$subject,$emailBody) {
            $message->to($tomail)
            ->from('noreplay@rewardssolution.com')
            ->subject($subject)
            ->setBody($emailBody, 'text/html');
        });
        
        if (Mail::failures()) {
            return "False Basic Email Sent. Check your inbox."; 
        }else  {
            return "True Basic Email Sent. Check your inbox.";
        } 
       
   }
   
   public static function sendEmailUser($tomail, $subject = null, $emailTemplateCode, $data = null, $cc = null, $bcc = null)
    {
         //$tomail = 'shihanagni@gmail.com';
         $emailTemplate = \App\Models\EmailTemplate::where('code', '=', $emailTemplateCode)->first();
         $emailBody = $emailTemplate->parse($data);
         $subject = $emailTemplate->parsesubject($subject);
         Mail::send(array(), array(), function ($message) use ($tomail, $cc, $bcc, $subject,$emailBody) {             
             /*$message->to($tomail)
             ->from('noreplay@rewardssolution.com')
             ->subject($subject)
             //->setBody($emailBody, 'text/html');
             ->setBody($emailBody, 'text/html');;*/
             
             if(!empty($cc) && !empty($bcc)) {
                $message->to($tomail)
                    ->cc($cc)
                    ->bcc($bcc)    
                    ->from('noreplay@rewardssolution.com')
                    ->subject($subject)
                    ->setBody($emailBody, 'text/html');
            }elseif(!empty($cc)) {
                $message->to($tomail)
                    ->cc($cc)    
                    ->from('noreplay@rewardssolution.com')
                    ->subject($subject)
                    ->setBody($emailBody, 'text/html');
            }elseif(!empty($bcc)) {
                $message->to($tomail)
                    ->bcc($bcc)    
                    ->from('noreplay@rewardssolution.com')
                    ->subject($subject)
                    ->setBody($emailBody, 'text/html');
            }else {
                $message->to($tomail)   
                    ->from('noreplay@rewardssolution.com')
                    ->subject($subject)
                    ->setBody($emailBody, 'text/html');
            }
         });

         if (Mail::failures()) {
             //return "False Basic Email Sent. Check your inbox.";
             return false;
         }else  {
             //return "True Basic Email Sent. Check your inbox.";
             return true;
         } 
    }
   
   public static function sendEmailAdmin2($userId, $emailTemplateCode, $data = null)
   {
        $tomail = 'shihanagni@gmail.com';
        $subject = 'Welcome Email - '.$emailTemplateCode;
        Mail::send(array(), array(), function ($message) use ($tomail,$subject,$emailBody) {
            $emailTemplate = EmailTemplate::where('name', '=', 'yourtemplate')->first();
            $message->to($tomail)
            ->from('noreplay@rewardssolution.com')
            ->subject($subject)
            //->setBody($emailBody, 'text/html');
            ->setBody($emailTemplate->parse($emailBody), 'text/html');;
        });
        
        if (Mail::failures()) {
            return "False Basic Email Sent. Check your inbox."; 
        }else  {
            return "True Basic Email Sent. Check your inbox.";
        } 
   }
   
   public static function my_avatar() {
       if(Auth::check()){
        $name = Auth::guard(self::my_prefix())->user()->full_name;
        $avatar = new LetterAvatar($name);
        return $avatar;
       }
   }
   
   public static function user_avatar() {
       if(Auth::check()){
        $name = Auth::user()->full_name;
        $avatar = new LetterAvatar($name);
        return $avatar;
       }
   }
   
   public static function my_prefix() {
       return request()->segment(1);
       
   }
   
   public static function my_domain() {
       //$host = request()->getHost();
       $subdomain = request()->getSchemeAndHttpHost();
       $client = \App\Models\Client::where( 'url', $subdomain )->where( 'status', 'active' )->first();
       if(!empty($client)) {
           return $client->id;
       }
       
       
   }
   
   public static function client_info() {
       //$host = request()->getHost();
       $subdomain = request()->getSchemeAndHttpHost();
       $client = \App\Models\Client::where( 'url', $subdomain )->where( 'status', 'active' )->first();
       if(!empty($client)) {
           return [
            //'code' => $code,
            'name' => $client->name,
            'image' => $client->image,
            //'user_points_balance' => $user_points_balance,

        ];
       }
       
       
   }
   
   public static function member_profile() {
       //dd('tets');
        if(Auth::guard('member')->check()) {
            $client_id = self::my_domain();
            $user_id = Auth::id();
            $checkprofiles = \App\Models\MemberProfiles::where('client_id', '=', $client_id)->where('user_id', '=', $user_id)->first();
            //dd($checkprofiles);
            return $checkprofiles->id;
        }
    }
    
    
    //--------------- Shopping -----------------------
    public static function messageList()
    {
        return Message::whereNull('read_at')->orderBy('created_at', 'desc')->get();
    } 
    public static function getAllCategory(){
        $category=new Category();
        $menu=$category->getAllParentWithChild();
        return $menu;
    } 
    
    public static function getHeaderCategory(){
        $category = new Category();
        // dd($category);
        $menu=$category->getAllParentWithChild();

        if($menu){
            ?>
            
            <li>
            <a href="javascript:void(0);">Category<i class="ti-angle-down"></i></a>
                <ul class="dropdown border-0 shadow">
                <?php
                    foreach($menu as $cat_info){
                        if($cat_info->child_cat->count()>0){
                            ?>
                            <li><a href="<?php echo route('product-cat',$cat_info->slug); ?>"><?php echo $cat_info->title; ?></a>
                                <ul class="dropdown sub-dropdown border-0 shadow">
                                    <?php
                                    foreach($cat_info->child_cat as $sub_menu){
                                        ?>
                                        <li><a href="<?php echo route('product-sub-cat',[$cat_info->slug,$sub_menu->slug]); ?>"><?php echo $sub_menu->title; ?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php
                        }
                        else{
                            ?>
                                <li><a href="<?php echo route('product-cat',$cat_info->slug);?>"><?php echo $cat_info->title; ?></a></li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </li>
        <?php
        }
    }

    public static function productCategoryList($option='all'){
        if($option='all'){
            return Category::orderBy('id','DESC')->get();
        }
        return Category::has('products')->orderBy('id','DESC')->get();
    }

    public static function postTagList($option='all'){
        if($option='all'){
            return PostTag::orderBy('id','desc')->get();
        }
        return PostTag::has('posts')->orderBy('id','desc')->get();
    }

    public static function postCategoryList($option="all"){
        if($option='all'){
            return PostCategory::orderBy('id','DESC')->get();
        }
        return PostCategory::has('posts')->orderBy('id','DESC')->get();
    }
    // Cart Count
    public static function cartCount($user_id=''){
       
        if(Auth::check()){
            if($user_id=="") $user_id=auth()->user()->id;
            return Cart::where('user_id',$user_id)->where('order_id',null)->sum('quantity');
        }
        else{
            return 0;
        }
    }
    // relationship cart with product
    public function product(){
        return $this->hasOne('App\Models\Product','id','product_id');
    }

    public static function getAllProductFromCart($user_id=''){
        if(Auth::check()){
            if($user_id=="") $user_id=auth()->user()->id;
            return Cart::with('product')->where('user_id',$user_id)->where('order_id',null)->get();
        }
        else{
            return 0;
        }
    }
    // Total amount cart
    public static function totalCartPrice($user_id=''){
        if(Auth::check()){
            if($user_id=="") $user_id=auth()->user()->id;
            return Cart::where('user_id',$user_id)->where('order_id',null)->sum('total_point');
        }
        else{
            return 0;
        }
    }
    // Wishlist Count
    public static function wishlistCount($user_id=''){
       
        if(Auth::check()){
            if($user_id=="") $user_id=auth()->user()->id;
            return Wishlist::where('user_id',$user_id)->where('cart_id',null)->sum('quantity');
        }
        else{
            return 0;
        }
    }
    public static function getAllProductFromWishlist($user_id=''){
        if(Auth::check()){
            if($user_id=="") $user_id=auth()->user()->id;
            return Wishlist::with('product')->where('user_id',$user_id)->where('cart_id',null)->get();
        }
        else{
            return 0;
        }
    }
    public static function totalWishlistPrice($user_id=''){
        if(Auth::check()){
            if($user_id=="") $user_id=auth()->user()->id;
            return Wishlist::where('user_id',$user_id)->where('cart_id',null)->sum('amount');
        }
        else{
            return 0;
        }
    }

    // Total price with shipping and coupon
    public static function grandPrice($id,$user_id){
        $order=Order::find($id);
        dd($id);
        if($order){
            $shipping_price=(float)$order->shipping->price;
            $order_price=self::orderPrice($id,$user_id);
            return number_format((float)($order_price+$shipping_price),2,'.','');
        }else{
            return 0;
        }
    }


    // Admin home
    public static function earningPerMonth(){
        $month_data=Order::where('status','delivered')->get();
        // return $month_data;
        $price=0;
        foreach($month_data as $data){
            $price = $data->cart_info->sum('price');
        }
        return number_format((float)($price),2,'.','');
    }

    public static function shipping(){
        return Shipping::orderBy('id','DESC')->get();
    }
}
