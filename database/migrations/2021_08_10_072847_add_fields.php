<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('customer_code')->unique();
            $table->unsignedBigInteger('client_id')->default(0);
            $table->unsignedBigInteger('member_id')->default(0);
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('member_id')->references('id')->on('member_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
