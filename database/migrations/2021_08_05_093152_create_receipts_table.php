<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id')->default(0);
            $table->unsignedBigInteger('member_id')->default(0);
            $table->unsignedBigInteger('user_id')->default(0);
            $table->unsignedBigInteger('reasons_id')->default(0);
            $table->string('receipt_no',50)->nullable();
            $table->string('member_category',150)->nullable();
            $table->string('member_company_name',150)->nullable();
            $table->date('date_of_receipt');
            $table->float('receipt_total_amount');
            $table->integer('receipt_points')->default(0);
            $table->enum('status',['pending','approved','declined','deleted'])->default('pending');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->ipAddress('visitor');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('member_id')->references('id')->on('admins');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('reasons_id')->references('id')->on('reasons');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
