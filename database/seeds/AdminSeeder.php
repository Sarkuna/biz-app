<?php

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = Admin::where('email', 'shihanagni@gmail.com')->first();
        if (is_null($user)) {
            $user = new Admin();
            $user->full_name = "Business Booster";
            $user->email = "shihanagni@gmail.com";
            $user->password = Hash::make('12345678');
            $user->user_type = "superadmin";
            $user->save();
        }
    }
}
