<?php

use Illuminate\Database\Seeder;

class ReasonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=array(
            array(
                'name'=>'Product(s) not listed'
            ),
            array(
                'name'=>'Invoice Date is out of range'
            ),
            array(
                'name'=>'Poor image quality'
            ),
            array(
                'name'=>'Admin decision'
            ),
        );

        DB::table('reasons')->insert($data);
    }
}
