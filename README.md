# Rewards And You - Laravel

Rewards And You (a brand by Masterz Myind Sdn Bhd) was formed to serve the growing need for a happier, healthier and more balanced lifestyle in our current fast paced world.

Our reward packages were specifically selected to meet the distinctive needs of today's hard and fast modern day living   where everyone is constantly rushing around with no time to spare a thought for themselves.

So with you in mind we hand-picked an exclusive selection of unique incentives that offer a range of enchanting and beneficial rewards.  These experiences will be able to enhance your physical health and emotional well-being as well as inspire spiritual renewal, and will ultimately help you achieve a greater sense of personal fulfillment and happiness.

We believe that life is abundant and that it should be celebrated in all its natural splendour.  So the 'Rewards and You team' invites you to come join us in enjoying the many pleasures of life.

## Version
1.0.0

## PHP Version
7.2 Above

## Laravel Version
7.0

## Requirements
- Basic knowledge of the [Laravel framework](https://laravel.com).
- Laravel CLI installed on your machine.
- [Valet](https://laravel.com/docs/5.5/valet#installation) installed on your machine.
- A code editor like [Visual Studio Code](https://code.visualstudio.com).
- [SQLite installed](http://www.sqlitetutorial.net/download-install-sqlite/) on your machine.

> Valet is only officially available to Mac users. However, there are ports for both [Linux](https://github.com/cpriego/valet-linux) and [Windows](https://github.com/cretueusebiu/valet-windows) available.

## Getting Started
To get up and running, simply run the following commands, first clone the repository and run the following commands
- `$ composer install`
- `$ valet link user.rewardsandyou.com`
- `$ valet link api.acme`
- `$ valet link www.rayadmin.rewardssolution.com`
 

Next, copy the `.env.example` file to `.env` and then add your database keys, and the `APP_BASE_DOMAIN`. Run the command below:
- `$ php artisan migrate --seed`

## Folders

- `app` - Contains all the Eloquent models
- `app/Http/Controllers/Admin` - Contains all the admin controllers
- `app/Http/Controllers/Front` - Contains all the end user controllers
- `public/themes/adminlte3` - Contains the admin CSS & JS files
- `public/themes/travelfresh` - Contains all the end user CSS & JS files
- `resources/admin` - Contains the admin view files
- `resources/front` - Contains the end user view files
- `routes/admin` - Contains the all admin routes
- `routes/frontend` - Contains the all end user routes