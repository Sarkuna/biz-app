<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Auth::routes();
//Route::view('/', 'welcome');
//Route::get('/', 'HomeController@index')->name('index');
//Route::get('/home', 'HomeController@index')->name('home');
Auth::routes(); 
Route::get('/', 'Front\Auth\LoginController@showLoginForm')->name('login');
Route::post('post-login', 'Front\Auth\LoginController@postLogin')->name('user.login.post'); 
Route::get('/login-id/{token}/{url}', 'Front\Auth\LoginController@loginUserbyid')->name('guest.login.id.get');


Route::get('/registration', 'Front\Auth\RegisterController@showRegistrationForm')->name('user.registration');
Route::post('post-registration', 'Front\Auth\RegisterController@postRegistration')->name('user.registration.post'); 

Route::get('forget-password', 'Front\Auth\ForgotPasswordController@showForgetPasswordForm')->name('user.forget.password.get');
Route::post('forget-password', 'Front\Auth\ForgotPasswordController@submitForgetPasswordForm')->name('user.forget.password.post');
Route::get('reset-password/{token}', 'Front\Auth\ForgotPasswordController@showResetPasswordForm')->name('user.reset.password.get');
Route::post('reset-password', 'Front\Auth\ForgotPasswordController@submitResetPasswordForm')->name('user.reset.password.post');

Route::group(['prefix' => '/user', 'middleware'=>['auth']], function () {
    Route::get('/', 'Front\HomeController@index')->name('user.home');
    Route::post('/logout/submit', 'Front\Auth\LoginController@logout')->name('user.logout.submit');
    //Route::get('/logout-id/{token}/{url}', 'Front\HomeController@logoutapi')->name('user.logoutapi.id.get');
    Route::get('/login-id/{token}/{url}', 'Front\HomeController@loginUserbyid')->name('user.login.id.get');
    
    Route::get('/my-profile', 'Front\HomeController@viewProfile')->name('user.my.profile');
    Route::get('/edit-profile', 'Front\HomeController@editProfile')->name('user.my.profile.edit');
    Route::post('/edit-profile', 'Front\HomeController@editProfileStore')->name('user.my.profile.store');
    
    Route::get('/change-password', 'Front\HomeController@changePassword')->name('user.change.password');
    Route::post('/change-password', 'Front\HomeController@changPasswordStore')->name('user.change.password.store');
    
    // Member Lists
    Route::get('/member-list', 'Front\MemberListsController@index')->name('user.member.list');
    Route::match(['get','post'],'/filtermember', 'Front\MemberListsController@memberFilter')->name('user.member.list.filter');
    Route::match(['get','post'],'/filterreceipt', 'Front\MemberListsController@receiptFilter')->name('user.receipt.list.filter');
    
    Route::get('/member-list/{id}/view/','Front\MemberListsController@memberDetail')->name('user.member.list.view');
    
    // Receipts
    Route::resource('/receipts','Front\ReceiptsController');
    Route::get('/receipts/{id}/view/','Front\ReceiptsController@show')->name('user.member.receipt.view');
    
    // Rewards
    Route::get('product-detail/{slug}','Front\RewardsController@productDetail')->name('product-detail');
    Route::post('/product/search','Front\RewardsController@productSearch')->name('product.search');
    Route::get('/product-cat/{slug}','Front\RewardsController@productCat')->name('product-cat');
    Route::get('/product-sub-cat/{slug}/{sub_slug}','Front\RewardsController@productSubCat')->name('product-sub-cat');
    Route::get('/product-brand/{slug}','Front\RewardsController@productBrand')->name('product-brand');
    Route::get('/rewards','Front\RewardsController@productGrids')->name('product-grids');
    Route::get('/product-lists','Front\RewardsController@productLists')->name('product-lists');
    Route::match(['get','post'],'/filter','Front\RewardsController@productFilter')->name('shop.filter');
    
    Route::get('/cart','Front\CartController@indexCart')->name('cart');
    /*Route::get('/cart',function(){
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('product-grids'),'name' => "Rewards Gallery"], ['name' => 'Cart']];
        return view('rewards.cart', compact('breadcrumbs'));
    })->name('cart');*/
    Route::get('/checkout','Front\CartController@checkout')->name('checkout');
    //Route::get('/wishlist/{slug}','WishlistController@wishlist')->name('add-to-wishlist');
    
    // Cart section
    Route::get('/add-to-cart/{slug}','Front\CartController@addToCart')->name('add-to-cart');
    Route::post('/add-to-cart','Front\CartController@singleAddToCart')->name('single-add-to-cart');
    Route::get('cart-delete/{id}','Front\CartController@cartDelete')->name('cart-delete');
    Route::post('cart-update','Front\CartController@cartUpdate')->name('cart.update');
    
    //Route::post('cart/order','OrderController@store')->name('cart.order');
    
    // Shipping Address
    Route::resource('/shipping','Front\ShippingController');
    Route::get('/shipping/{id}/delete/', 'Front\ShippingController@destroy')->name('shipping.delete');
    Route::get('/shipping/{id}/default/', 'Front\ShippingController@addressDefault')->name('shipping.default');
    
    // Events
    Route::get('/events/upcoming-events', 'Front\EventsController@index')->name('upcoming.events.list');
    Route::get('/events/past-events', 'Front\EventsController@pastEvents')->name('past.events.list');
});

Route::group(['prefix' => '/admin'], function () {
    Auth::routes(['register'=>false]);
    // Login
    Route::get('/', 'Admin\Auth\LoginController@index');
    Route::get('/login', 'Admin\Auth\LoginController@index')->name('admin.login');
    Route::post('post-login', 'Admin\Auth\LoginController@postLogin')->name('admin.login.post'); 
    Route::post('/logout/submit', 'Admin\Auth\LoginController@logout')->name('admin.logout.submit');

    // Reset password
    Route::get('forget-password', 'Admin\Auth\ForgotPasswordController@showForgetPasswordForm')->name('admin.forget.password.get');
    Route::post('forget-password', 'Admin\Auth\ForgotPasswordController@submitForgetPasswordForm')->name('admin.forget.password.post');
    Route::get('reset-password/{token}', 'Admin\Auth\ForgotPasswordController@showResetPasswordForm')->name('admin.reset.password.get');
    Route::post('reset-password', 'Admin\Auth\ForgotPasswordController@submitResetPasswordForm')->name('admin.reset.password.post');
    
    //Loged in Admin User
    Route::middleware('auth:admin')->group(function () {
        
        Route::get('/dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');
        Route::get('/profile', 'Admin\DashboardController@profile')->name('admin.profile');
        Route::get('/change-password', 'Admin\DashboardController@changePassword')->name('admin.change.password');
        //Route::get('/company123', 'Admin\DashboardController@companyprofile')->name('admin.company.profile');
        
        //Company Info
        Route::get('/company', 'Admin\ClientsController@index')->name('admin.company.index');
        Route::get('/company/{id}/edit/','Admin\ClientsController@edit')->name('admin.company.edit');
        Route::patch('/company/{id}/update/','Admin\ClientsController@update')->name('admin.company.update');
        Route::get('/company', 'Admin\ClientsController@index')->name('admin.company.index');
        Route::get('/company/logo', 'Admin\ClientsController@companylogo')->name('admin.company.logo');
        Route::post('/company/logo/store','Admin\ClientsController@fileStore')->name('admin.company.logo.store');
        Route::post('/company/images-delete', 'Admin\ClientsController@destroy')->name('admin.company.logo.delete');
        Route::get('/company/images-view', 'Admin\ClientsController@viewimage')->name('admin.company.logo.view');
        
        //Email Template
        Route::resource('/templates/email','Admin\EmailTemplatesController');
        Route::get('templates/email/{id}/edit/','Admin\EmailTemplatesController@edit');
        Route::get('templates/email/{id}/delete/', 'Admin\EmailTemplatesController@destroy')->name('email.delete'); 
        
        //Venues
        Route::resource('/events/venues','Admin\VenuesController');
        Route::get('events/venues/{id}/edit/','Admin\VenuesController@edit');
        Route::get('events/venues/{id}/status/','Admin\VenuesController@status')->name('venues.status'); 
        Route::get('events/venues/{id}/delete/', 'Admin\VenuesController@destroy')->name('venues.delete'); 
        Route::post('events/venues/{id}/change/', 'Admin\VenuesController@destroystatus')->name('venues.status.change'); 
        
        //Venues
        Route::resource('/events','Admin\EventsController');
        Route::get('events/{id}/edit/','Admin\VenuesController@edit');
        
        //Member Profile
        //Route::get('/members', 'Admin\MemberProfilesController@index')->name('admin.members.index');
        Route::resource('/members','Admin\MemberProfilesController');
        Route::get('/members/{id}/edit/','Admin\MemberProfilesController@edit')->name('admin.members.edit');
        Route::patch('/members/{id}/update/','Admin\MemberProfilesController@update')->name('admin.members.update');
    });
});


Route::group(['prefix' => '/member'], function () {
    Route::get('/login', 'Member\Auth\LoginController@showLoginForm')->name('member.login');
    Route::get('/', 'Member\Auth\LoginController@showLoginForm');
    Route::post('post-login', 'Member\Auth\LoginController@postLogin')->name('login.post'); 
    Route::post('/logout/submit', 'Member\Auth\LoginController@logout')->name('member.logout.submit');
    
    // Reset password
    Route::get('forget-password', 'Member\Auth\ForgotPasswordController@showForgetPasswordForm')->name('member.forget.password.get');
    Route::post('forget-password', 'Member\Auth\ForgotPasswordController@submitForgetPasswordForm')->name('member.forget.password.post');
    Route::get('reset-password/{token}', 'Member\Auth\ForgotPasswordController@showResetPasswordForm')->name('member.reset.password.get');
    Route::post('reset-password', 'Member\Auth\ForgotPasswordController@submitResetPasswordForm')->name('reset.password.post');
    
    Route::middleware('auth:member')->group(function () {
        Route::get('/dashboard', 'Member\DashboardController@index')->name('member.dashboard');
        Route::get('/home', 'Member\AdminsController@index')->name('home');
        Route::get('/company/profile', 'Member\DashboardController@companyProfile')->name('member.company.profile');
        Route::post('/company-setup', 'Member\DashboardController@store')->name('member.company.setup');
        
        Route::get('/change-password', 'Member\DashboardController@changePassword')->name('member.change.password');
        Route::post('/change-password', 'Member\DashboardController@changPasswordStore')->name('member.change.password.store');
        Route::get('/profile', 'Member\DashboardController@profile')->name('member.profile');

        Route::get('/profile/edit', 'Member\MyProfilesController@index')->name('member.myprofile.edit');
        Route::get('/profile/gallery', 'Member\MyProfilesController@portfolio')->name('member.myprofile.gallery');
        Route::post('/profile/company-post', 'Member\MyProfilesController@store')->name('member.myprofile.post');
        Route::post('/profile/gallery-post', 'Member\MyProfilesController@storeImage')->name('member.myprofile.post.new.image');
        Route::post('/profile/remove-image', 'Member\MyProfilesController@deleteImage')->name('member.myprofile.remove.image');
        
        Route::get('/profile/bank/edit', 'Member\MyProfilesController@bankEdit')->name('member.myprofile.bank.edit');
        Route::get('/profile/bank/new', 'Member\MyProfilesController@bankNew')->name('member.myprofile.bank.new');
        Route::post('/profile/bank/edit-post', 'Member\MyProfilesController@bankStore')->name('member.myprofile.bank.store');
        
        Route::get('/customer', 'Member\CustomersController@index')->name('customer.index');
        
        Route::get('/receipt/pending', 'Member\ReceiptsController@index')->name('receipt.index');
        Route::get('/receipt/approve', 'Member\ReceiptsController@approve')->name('receipt.approve');
        Route::get('/receipt/decline', 'Member\ReceiptsController@decline')->name('receipt.decline');
        Route::get('/receipt/decline', 'Member\ReceiptsController@decline')->name('receipt.decline');
        Route::get('/receipt/{id}/edit/', 'Member\ReceiptsController@edit')->name('receipt.edit');
        Route::post('/receipt/{id}/update', 'Member\ReceiptsController@receiptStore')->name('receipt.store');
        
        Route::get('/points/purchase-points', 'Member\PurchasePointsController@index')->name('member.purchase.points');
        Route::get('/points/{id}/purchase-confirmation/', 'Member\PurchasePointsController@purchaseconfirmation')->name('member.purchase.confirmation');
        Route::post('/points/purchase-post', 'Member\PurchasePointsController@store')->name('member.purchase.post');
        Route::get('/points/purchase-history', 'Member\PurchasePointsController@purchaseHistory')->name('member.purchase.history');
        
    });
   
});
/*Route::group(['prefix' => 'client', 'as' => 'client.'], function () {
    Route::get('/login', 'Client\Auth\LoginController@index');
    Route::get('/', 'Client\Auth\LoginController@index');
    Route::post('post-login', 'Client\Auth\LoginController@postLogin')->name('login.post'); 
    Route::get('/logout', 'Client\Auth\LoginController@logout')->name('client.logout');
});*/