<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*Route::post('articles', function() {
    // If the Content-Type and Accept headers are set to 'application/json', 
    // this will return a JSON structure. This will be cleaned up later.
    $name = "MOhamed Shihna Test API";
    return $name;
});*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'API\v1\AuthController@login');
    Route::post('signup', 'API\v1\AuthController@signup');
    Route::get('test', 'API\v1\TestController@index');
    Route::post('forgot-password', 'API\v1\AuthController@ForgotPassword');
    
    Route::middleware('auth:api')->group( function () {
    //Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'API\v1\AuthController@logout');
        //Route::get('user', 'API\v1\AuthController@user');
        Route::get('dashboard', 'API\v1\AuthController@Dashboard');
        Route::post('chang-password', 'API\v1\AuthController@changPassword');
        Route::get('external-login', 'API\v1\AuthController@checkExternallogin');
    });
});